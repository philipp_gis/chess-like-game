﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SP_Enum.unitTypes;
using static SP_Enum.gameState;
using UnityEngine.UI;

public class SP_Actions : MonoBehaviour
{
    #region Header

    private SP_Selection selection;
    private SP_Sound sound;
    private SP_Lists lists;
    private SP_UI ui;
    private SP_Controller controller;

    // Positionen von der aus das Objekt zum Ziel bewegt werden soll
    private Vector3 startPosition;
    private Vector3 endPosition;

    // Zeitpunkt von dem an die Zeit gemessen wird und wann aufgehört werden soll
    private float startTime;
    private float endTime;

    private void Awake()
    {
        selection = GetComponent<SP_Selection>();
        sound = GetComponent<SP_Sound>();
        lists = GetComponent<SP_Lists>();
        ui = GetComponent<SP_UI>();
        controller = GetComponent<SP_Controller>();
    }

    #endregion



    #region Methoden

    #region Move

    public void Move()
    {
        // Einheit wird auf freies angeklickte Feld bewegt
        if (selection.HitInfo.transform.GetComponent<SP_Tile>().Type == SP_Enum.tileReachability.MOVEABLE || selection.HitInfo.transform.GetComponent<SP_Tile>().Type == SP_Enum.tileReachability.BOTHABLE)
        {
            // Ermittelt die Position der ausgewählten Einheit
            float playerHorizontalPosition = selection.SelectedUnit.transform.parent.position.x;
            float playerVerticalPosition = selection.SelectedUnit.transform.parent.position.z;
            Vector3 playerTransform = new Vector3(playerHorizontalPosition, 1, playerVerticalPosition);

            // Ermittelt die angeklickte Zielposition
            float targetHorizontalPosition = selection.HitInfo.transform.position.x;
            float targetVerticalPosition = selection.HitInfo.transform.position.z;
            Vector3 targetTransform = new Vector3(targetHorizontalPosition, 1, targetVerticalPosition);

            // Lege Start und Endpunkt für LERP fest
            startPosition = playerTransform;
            endPosition = targetTransform;

            // Lege LERP Dauer fest
            startTime = Time.time;
            endTime = 1f;

            selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", true);
            sound.playSound(sound.SoundSource, sound.MoveSound);

            // Während der Animation darf nicht geskipt werden
            ui.SkipButtonsInteractableToggle();

            controller.CurrentGameState = SP_Enum.gameState.UNITY_STATE;
            controller.CurrentAction = SP_Enum.action.MOVE;
            controller.Animations = SP_Enum.animations.IN_ANIMATION;
        }
    }

    public void PostMoveWork()
    {
        // Beende Animationsloop
        selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", false);

        // Altes Feld verliert seine Referenz auf die Einheit
        selection.SelectedTile.GetComponent<SP_Tile>().CurrentUnit = null;

        // Einheit und Zielfeld setzen sich gegenseitig als Referenz
        selection.SelectedUnit.GetComponent<SP_Unit>().CurrentTile = selection.HitInfo.transform.gameObject;
        selection.SelectedUnit.GetComponent<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().CurrentUnit = selection.SelectedUnit;

        // Einheit abwählen und passiv setzen
        selection.SelectedUnit.GetComponent<SP_Unit>().IsActiv = false;
        selection.DeselectSelectedUnit();

        ui.SkipButtonsInteractableToggle();

        // Zustände zurücksetzen
        controller.CurrentAction = SP_Enum.action.NOT_IN_ACTION;
        controller.CurrentSelection = SP_Enum.selection.NOTHING_SELECTED;
        controller.CurrentGameState = SP_Enum.gameState.SELECTION_STATE;
        controller.Animations = SP_Enum.animations.OUT_OF_ANIMATION;

        ui.TeamPassivCheck();
    }

    #endregion

    #region Attack

    // Töten einer feindlichen Einheit
    public void Attack()
    {
        if (selection.HitInfo.collider is CapsuleCollider &&
           (selection.HitInfo.transform.GetComponentInChildren<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().Type ==
            SP_Enum.tileReachability.ATTACKABLE ||
            selection.HitInfo.transform.GetComponentInChildren<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().Type ==
            SP_Enum.tileReachability.BOTHABLE) ||
            selection.HitInfo.collider is BoxCollider &&
           (selection.HitInfo.transform.GetComponent<SP_Tile>().Type == SP_Enum.tileReachability.ATTACKABLE ||
            selection.HitInfo.transform.GetComponent<SP_Tile>().Type == SP_Enum.tileReachability.BOTHABLE))
        {
            // Ermittelt die Position der ausgewählten Einheit
            float playerHorizontalPosition = selection.SelectedUnit.transform.parent.position.x;
            float playerVerticalPosition = selection.SelectedUnit.transform.parent.position.z;
            Vector3 playerTransform = new Vector3(playerHorizontalPosition, 1, playerVerticalPosition);

            // Ermittelt die angeklickte Zielposition
            float targetHorizontalPosition = selection.HitInfo.transform.position.x;
            float targetVerticalPosition = selection.HitInfo.transform.position.z;
            Vector3 targetTransform = new Vector3(targetHorizontalPosition, 1, targetVerticalPosition);

            // Lege Start und Endpunkt für LERP fest
            startPosition = playerTransform;
            endPosition = targetTransform;

            // Lege LERP Dauer fest
            startTime = Time.time;
            endTime = 1f;

            selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", true);

            // Während der Animation darf nicht geskipt werden
            ui.SkipButtonsInteractableToggle();

            controller.CurrentGameState = SP_Enum.gameState.UNITY_STATE;
            controller.CurrentAction = SP_Enum.action.ATTACK;
            controller.Animations = SP_Enum.animations.IN_ANIMATION;
        }
    }

    public void PostAttackWork()
    {
        // Beende Animationsloop
        selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", false);

        // Wenn eine Unit getroffen wird
        if (selection.HitInfo.collider is CapsuleCollider)
        {
            // Einheit und Zielfeld setzen sich gegenseitig als Referenz
            selection.SelectedUnit.GetComponent<SP_Unit>().CurrentTile = selection.HitInfo.transform.GetComponent<SP_Unit>().CurrentTile;
            selection.SelectedUnit.GetComponent<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().CurrentUnit = selection.SelectedUnit;

            // Alte Einheit verliert seine Referenz
            selection.SelectedTile.GetComponent<SP_Tile>().CurrentUnit = null;

            // Angegriffene Unit wird aus der Unit-Liste entfernt
            lists.UnitList.Remove(selection.HitInfo.transform.parent.gameObject);

            // Angegriffene Unit wird aus seiner Team-Liste entfernt
            if (selection.HitInfo.transform.GetComponent<SP_Unit>().Team == true)
            {
                lists.TeamTrueList.Remove(selection.HitInfo.transform.parent.gameObject);
            }

            else if (selection.HitInfo.transform.GetComponent<SP_Unit>().Team == false)
            {
                lists.TeamFalseList.Remove(selection.HitInfo.transform.parent.gameObject);
            }

            Destroy(selection.HitInfo.transform.parent.gameObject);
        }

        // Wenn ein Tile getroffen wird
        else if (selection.HitInfo.collider is BoxCollider)
        {
            // Zwischenspeichern der zu zerstörenden Unit
            GameObject unitToDestroy = selection.HitInfo.transform.GetComponent<SP_Tile>().CurrentUnit;

            // Alte Einheit verliert seine Referenz
            selection.SelectedTile.GetComponent<SP_Tile>().CurrentUnit = null;

            // Einheit und Zielfeld setzen sich gegenseitig als Referenz
            selection.SelectedUnit.GetComponent<SP_Unit>().CurrentTile = selection.HitInfo.transform.gameObject;
            selection.SelectedUnit.GetComponent<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().CurrentUnit = selection.SelectedUnit;

            // Angegriffene Unit wird aus der Unit-Liste entfernt
            lists.UnitList.Remove(unitToDestroy.transform.parent.gameObject);

            // Angegriffene Unit wird aus seiner Team-Liste entfernt
            if (unitToDestroy.GetComponent<SP_Unit>().Team == true)
            {
                lists.TeamTrueList.Remove(unitToDestroy.transform.parent.gameObject);
            }

            else if (unitToDestroy.GetComponent<SP_Unit>().Team == false)
            {
                lists.TeamFalseList.Remove(unitToDestroy.transform.parent.gameObject);
            }

            Destroy(unitToDestroy.transform.parent.gameObject);
        }

        // Einheit abwählen und passiv setzen
        selection.SelectedUnit.GetComponent<SP_Unit>().IsActiv = false;
        selection.DeselectSelectedUnit();

        ui.SkipButtonsInteractableToggle();

        // Zustände zurücksetzen
        controller.Animations = SP_Enum.animations.OUT_OF_ANIMATION;
        controller.CurrentAction = SP_Enum.action.NOT_IN_ACTION;
        controller.CurrentSelection = SP_Enum.selection.NOTHING_SELECTED;
        controller.CurrentGameState = SP_Enum.gameState.SELECTION_STATE;
        
        // Abfrage ob ein Team gewonnen hat
        ui.WinCheck();
        ui.TeamPassivCheck();
    }

    #endregion

    #region Fusion

    // Vereinigung zweier verbündeter Einheiten
    public void Fusion()
    {
        if ((selection.HitInfo.collider is CapsuleCollider
            && (selection.HitInfo.transform.GetComponentInChildren<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().Type == SP_Enum.tileReachability.MOVEABLE ||
            selection.HitInfo.transform.GetComponentInChildren<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().Type == SP_Enum.tileReachability.BOTHABLE)) 
            || 
            selection.HitInfo.collider is BoxCollider &&
            selection.HitInfo.transform.GetComponent<SP_Tile>().Type == SP_Enum.tileReachability.MOVEABLE ||
            selection.HitInfo.transform.GetComponent<SP_Tile>().Type == SP_Enum.tileReachability.BOTHABLE)
        {
            // Ermittelt die Position der ausgewählten Einheit
            float playerHorizontalPosition = selection.SelectedUnit.transform.parent.position.x;
            float playerVerticalPosition = selection.SelectedUnit.transform.parent.position.z;
            Vector3 playerTransform = new Vector3(playerHorizontalPosition, 1, playerVerticalPosition);

            // Ermittelt die angeklickte Zielposition
            float targetHorizontalPosition = selection.HitInfo.transform.position.x;
            float targetVerticalPosition = selection.HitInfo.transform.position.z;
            Vector3 targetTransform = new Vector3(targetHorizontalPosition, 1, targetVerticalPosition);

            // Lege Start und Endpunkt für LERP fest
            startPosition = playerTransform;
            endPosition = targetTransform;

            // Lege LERP Dauer fest
            startTime = Time.time;
            endTime = 1f;

            selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", true);

            // Während der Animation darf nicht geskipt werden
            ui.SkipButtonsInteractableToggle();

            controller.CurrentGameState = SP_Enum.gameState.UNITY_STATE;
            controller.CurrentAction = SP_Enum.action.FUSION;
            controller.Animations = SP_Enum.animations.IN_ANIMATION;
        }
    }

    public void PostFusionWork()
    {
        // Beende Animationsloop
        selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", false);
        selection.SelectedUnit.GetComponent<SP_Unit>().IsActiv = false;

        controller.CurrentGameState = SP_Enum.gameState.UNITY_STATE;

        ui.RememberPatternForPatternPanel();
        ui.SetPatternButtonName();
        
        ui.PatternPanel.SetActive(true);
        
        ui.PatternPanelEqualityCheck();


        // Zuständ zurücksetzen
        controller.Animations = SP_Enum.animations.OUT_OF_ANIMATION;
        controller.CurrentAction = SP_Enum.action.NOT_IN_ACTION;
    }

    #endregion

    #region Lerp

    private void FixedUpdate()
    {
        if (controller.Animations == SP_Enum.animations.IN_ANIMATION)
        {
            float actualTime;
            float progress;

            // Bestimmt den Moment der Zeitmessung und wandelt es in den prozentualen Anteil des gesamten LERP
            actualTime = Time.time - startTime;
            progress = actualTime / endTime;

            // Positioniere Einheit in die Position des Lerp anhand des Fortschritts der Gesamtstrecke
            selection.SelectedUnit.transform.parent.position = Vector3.Lerp(startPosition, endPosition, progress);

            // Wenn 100%  des LERP erreicht wurde
            if (progress >= 1.0f)
            {
                if (controller.CurrentAction == SP_Enum.action.MOVE)
                {
                    controller.CurrentAction = SP_Enum.action.POST_MOVE_WORK;
                }

                else if (controller.CurrentAction == SP_Enum.action.ATTACK)
                {
                    sound.playSound(sound.SoundSource, sound.AttackSound);
                    controller.CurrentAction = SP_Enum.action.POST_ATTACK_WORK;
                }

                else if (controller.CurrentAction == SP_Enum.action.FUSION)
                {
                    sound.playSound(sound.SoundSource, sound.FusionSound);
                    controller.CurrentAction = SP_Enum.action.POST_FUSION_WORK;
                }
            }
        }
    }

    #endregion

    #endregion
}