﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.PlayerLoop;
using static SP_Enum.unitTypes;


public class SP_Selection : MonoBehaviour
{
    // Verwaltung der Auswahl von Objekten

    #region Header

    private SP_SelectionActions selectionActions;
    private SP_Material material;
    private SP_Sound sound;
    private SP_Lists lists;
    private SP_Controller controller;

    // Merkt sich welche Einheit, Feld ausgewählt ist

    [Header("READ ONLY")]
    [SerializeField] private GameObject selectedUnit;
    [SerializeField] private GameObject selectedTile;

    public RaycastHit hitInfo;
    [SerializeField] private bool hit;

    private void Awake()
    {
        selectionActions = GetComponent<SP_SelectionActions>();
        material = GetComponent<SP_Material>();
        sound = GetComponent<SP_Sound>();
        lists = GetComponent<SP_Lists>();
        controller = GetComponent<SP_Controller>();
    }

    #endregion



    #region Methoden

    public void ChoseUnit()
    {
        sound.playSound(sound.SoundSource, sound.ClickSound);

        // Raycast um festzustellen was der Spieler anklickt
        hitInfo = new RaycastHit();
        hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);

        // Spieler klickt auf feindliche Einheit oder auf Ihr Feld
        if ((hitInfo.collider is BoxCollider && hitInfo.transform.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().Team != controller.GetComponent<SP_Controller>().TeamTurn) ||
            (hitInfo.collider is CapsuleCollider && hitInfo.transform.GetComponentInChildren<SP_Unit>().Team != controller.GetComponent<SP_Controller>().TeamTurn))
        {
            // Setzt Einheit und dessen Feld als ausgewählt
            SelectObjects();

            // Färbt Einheit und dessen Feld als Feind
            material.SetMaterial(selectedUnit, material.EnemySelectedMaterial);
            material.SetMaterial(selectedTile, material.EnemySelectedMaterial);

            // Färbe Felder der Einheit in der Sie eine Aktion ausführen kann
            ColorPattern();

            // Wechsel in den nächsten Zustand
            controller.CurrentGameState = SP_Enum.gameState.ENEMY_SELECTED_STATE;
        }

        // Spieler klickt auf freundliche Einheit oder auf Ihr Feld
        else if ((hitInfo.collider is BoxCollider && hitInfo.transform.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().Team == controller.GetComponent<SP_Controller>().TeamTurn) ||
                 (hitInfo.collider is CapsuleCollider && hitInfo.transform.GetComponent<SP_Unit>().Team == controller.GetComponent<SP_Controller>().TeamTurn))
        {
            // Setzt Einheit und dessen Feld als ausgewählt
            SelectObjects();

            // Färbt Einheit und dessen Feld als Freund
            material.SetMaterial(selectedUnit, material.AllySelectedMaterial);
            material.SetMaterial(selectedTile, material.AllySelectedMaterial);

            // Färbe Felder der Einheit in der Sie eine Aktion ausführen kann
            ColorPattern();

            // Wechsel in den nächsten Zustand
            controller.CurrentGameState = SP_Enum.gameState.ALLY_SELECTED_STATE;
        }
    }

    public void DeselectEnemy()
    {
        sound.playSound(sound.SoundSource, sound.ClickSound);
        DeselectAll();

        // Wechsel in den nächsten Zustand
        controller.CurrentGameState = SP_Enum.gameState.SELECTION_STATE;
    }

    public void ChoseAction()
    {
        // Bewege Einheit auf ein Verbündeten oder dessen Feld
        if ((hitInfo.collider is BoxCollider && hitInfo.transform.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().Team == controller.GetComponent<SP_Controller>().TeamTurn) ||
            (hitInfo.collider is CapsuleCollider && hitInfo.transform.GetComponent<SP_Unit>().Team == controller.GetComponent<SP_Controller>().TeamTurn))
        {
            selectionActions.fusion();
            // [ -> FUSION ]
            // [ -> FUSIONFENSTER AUFRUFEN ]
        }

        // Bewege Einheit auf ein Feind oder dessen Feld
        else if ((hitInfo.collider is BoxCollider && hitInfo.transform.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().Team != controller.GetComponent<SP_Controller>().TeamTurn) ||
            (hitInfo.collider is CapsuleCollider && hitInfo.transform.GetComponentInChildren<SP_Unit>().Team != controller.GetComponent<SP_Controller>().TeamTurn))
        {
            selectionActions.attack();
            // [ -> CHECK OB TEAM KEINE EINHEITEN MEHR HAT ]
            // [ NEIN -> WEITER MACHEN ]
            // [ JA -> GEWONNENFENSTER AUFRUFEN ]
        }

        // Bewege Einheit auf ein leeres Feld
        else if (hitInfo.collider is BoxCollider && !hitInfo.transform.GetComponent<SP_Tile>().CurrentUnit)
        {
            if (SelectedUnit.GetComponent<SP_Unit>().IsActiv == true)
            {
                selectionActions.Move();
            }
        }

        // [ -> EINHEIT PASSIV SETZEN ]
        // [ -> ABWAHL EINHEIT UND FELD ]

        // [ -> CHECK OB ALLE EINHEITEN PASSIV SIND ]
        // [ NEIN -> WEITER MACHEN ]
        // [ JA -> FELD LÖSCHEN, TEAM WECHSEL, DANN WEITER MACHEN ]

        // [ ZUSTAND WECHSELN ]
    }

    public void DeselectAction()
    {

        sound.playSound(sound.SoundSource, sound.ClickSound);
        DeselectAll();
        // [ ANPASSEN, DAS WENN MAN NICHT AUF PATTERN DER EINHEIT KLICKT ]
        // [ ALLES ABGEWÄHLT WIRD ]

        // Wechsel in den nächsten Zustand
        controller.CurrentGameState = SP_Enum.gameState.SELECTION_STATE;
    }

    public void DeselectAll()
    {
        if (selectedUnit)
        {
            // entfäbre Felder
            DecolorPattern();
            foreach (GameObject g in lists.TileList)
            {
                g.GetComponent<SP_Tile>().Type = SP_Enum.tileReachability.DEFAULT;
            }

            // Wähle Einheit und Feld ab
            selectedTile = null;
            selectedUnit = null;

            sound.playSound(sound.SoundSource, sound.ClickSound);
        }
    }

    #endregion



    #region Hilfsmethoden

    private void SelectObjects()
    {
        // Wählt je nachdem was angeklickt wird ein Feld oder eine Einheit aus
        if (hitInfo.transform.GetComponent<SP_Unit>())
        {
            selectedUnit = hitInfo.transform.gameObject;
            selectedTile = hitInfo.transform.GetComponent<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().gameObject;
        }

        else if (hitInfo.transform.GetComponent<SP_Tile>())
        {
            selectedTile = hitInfo.transform.gameObject;
            selectedUnit = hitInfo.transform.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().gameObject;
        }
    }

    private void ColorPattern()
    {
        // Färbt die Felder anhand der übergebenen Muster der Einheiten

        List<int[]> attackPattern;
        List<int[]> movePattern;

        int localX;
        int localZ;

        attackPattern = selectedUnit.GetComponent<SP_Unit>().AttackPattern;
        movePattern = selectedUnit.GetComponent<SP_Unit>().MovePattern;
        localX = (int)selectedUnit.transform.position.x;
        localZ = (int)selectedUnit.transform.position.z;

        foreach (int[] i in attackPattern)
        {
            // Koordinaten des Pattern in Weltkoordinaten umwandeln
            int worldX = i[0] + localX;
            int worldZ = i[1] + localZ;

            // Vergleiche Koordinaten von Muster und Felder, wenn diese übereinstimmen 
            foreach (GameObject g in lists.TileList)
            {
                if (worldX == g.transform.position.x && worldZ == g.transform.position.z)
                {
                    material.SetMaterial(g, material.TileAttackMaterial);
                    g.GetComponent<SP_Tile>().Type = SP_Enum.tileReachability.ATTACKABLE;
                    break;
                }
            }
        }

        foreach (int[] i in movePattern)
        {
            // Koordinaten des Pattern in Weltkoordinaten umwandeln
            int worldX = i[0] + localX;
            int worldZ = i[1] + localZ;

            // Vergleiche Koordinaten von Muster und Felder, wenn diese übereinstimmen 
            foreach (GameObject g in lists.TileList)
            {
                if (worldX == g.transform.position.x && worldZ == g.transform.position.z)
                {
                    if (g.GetComponent<SP_Tile>().Type == SP_Enum.tileReachability.ATTACKABLE)
                    {
                        material.SetMaterial(g, material.TileBothableMaterial);
                        g.GetComponent<SP_Tile>().Type = SP_Enum.tileReachability.BOTHABLE;
                    }
                    else
                    {
                        material.SetMaterial(g, material.TileMoveMaterial);
                        g.GetComponent<SP_Tile>().Type = SP_Enum.tileReachability.MOVEABLE;
                    }
                    break;
                }
            }
        }
    }

    public void DecolorPattern()
    {
        if (selectedUnit.GetComponent<SP_Unit>().Team == false)
        {
            if (selectedUnit.GetComponent<SP_Unit>().Team == controller.TeamTurn)
            {
                if (selectedUnit.GetComponent<SP_Unit>().IsActiv)
                {
                    material.SetMaterial(selectedUnit, material.TeamFalseActiveMaterial);
                }
                else
                {
                    material.SetMaterial(selectedUnit, material.TeamFalsePassivMaterial);
                }
            }
            else
            {
                material.SetMaterial(selectedUnit, material.TeamFalsePassivMaterial);
            }
        }

        else if (selectedUnit.GetComponent<SP_Unit>().Team == true)
        {
            if (selectedUnit.GetComponent<SP_Unit>().Team == controller.TeamTurn)
            {
                if (selectedUnit.GetComponent<SP_Unit>().IsActiv)
                {
                    material.SetMaterial(selectedUnit, material.TeamTrueActiveMaterial);
                }
                else
                {
                    material.SetMaterial(selectedUnit, material.TeamTruePassivMaterial);
                }
            }
            else
            {
                material.SetMaterial(selectedUnit, material.TeamTruePassivMaterial);
            }
        }

        foreach (GameObject g in lists.TileList)
        {
            if (g.GetComponent<Renderer>().material != material.TileDefaultMaterial)
            {
                material.SetMaterial(g, material.TileDefaultMaterial);
            }
        }
    }

    #endregion



    #region Getter Setter

    public GameObject SelectedUnit { get => selectedUnit; set => selectedUnit = value; }
    public GameObject SelectedTile { get => selectedTile; set => selectedTile = value; }
    public RaycastHit HitInfo { get => hitInfo; }
    public bool Hit { get => hit; }

    #endregion
}




//public void SelectObject(RaycastHit hittedObject)
//{
//    //hittedObject.transform.gameObject = selectedUnit
//}
/*
[Header("Gameobjects")]
[SerializeField] private SP_Model model;
[SerializeField] private SP_Access a;

public void deselect()
{
    model.raycast();

    foreach (GameObject t in a.tileList)
    {
        t.GetComponent<Renderer>().material = a.tileMaterial;
    }

    if (a.selectedUnit_Team == 2)
    {
        a.selectedUnit_Material = a.unitTwoActivMaterial;
    }

    else if (a.selectedUnit_Team == 1)
    {
        a.selectedUnit_Material = a.unitOneActivMaterial;
    }

    a.selectedUnit = null;
    a.selectedTile = null;
}

public void select()
{
    model.raycast();

    if (a.hitUnit)
    {
        a.selectedUnit = a.hittedObject;
        a.selectedTile = a.selectedUnit_CurrentTile;
    }
    else if (a.hitTile)
    {
        a.selectedTile = a.hittedObject;
        a.selectedUnit = a.selectedTile_CurrentUnit;
    }

    a.selectedUnit_Material = a.selectedMaterial;
    a.selectedTile_Material = a.selectedMaterial;

    for (int i = 0; i < a.selectedUnit_Position.GetLength(0); i++)
    {
        for (int j = 0; j < a.selectedUnit_Position.GetLength(1); j++)
        {
            if (a.selectedUnit_MovePattern[i, j])
            {
                foreach (GameObject t in a.tileList)
                {
                    /*
                    if (a.selectedUnit_Position[i, j] == t.GetComponent<SP_Tile>().Index)
                    {
                        t.GetComponent<Renderer>().material = a.moveableMaterial;
                    }

                }
            }

            if (a.selectedUnit_AttackPattern[i, j])
            {
                foreach (GameObject t in a.tileList)
                {
                    /*
                    if (a.selectedUnit_Position[i, j] == t.GetComponent<SP_Tile>().Index)
                    {
                        t.GetComponent<Renderer>().material = a.attackableMaterial;
                    }

                }
            }

            if (a.selectedUnit_MovePattern[i, j] && a.selectedUnit_AttackPattern[i, j])
            {
                foreach (GameObject t in a.tileList)
                {
                    /*
                    if (a.selectedUnit_Position[i, j] == t.GetComponent<SP_Tile>().Index)
                    {
                        t.GetComponent<Renderer>().material = a.bothableMaterial;
                    }

                }
            }
        }
    }
}
*/

//private delegate void SetMaterial(GameObject g, Material m);
//private SetMaterial setMaterial;

//private void Start()
//{
//    // setMaterial = material.SetMaterial;
//    // LocalToWorldCoordinate(setMaterial);
//}

//private void LocalToWorldCoordinate(SetMaterial methodToCall)
//{
//    {
//        // Färbt die Felder anhand der übergebenen Muster der Einheiten

//        List<int[]> pattern;
//        int localX;
//        int localZ;

//        pattern = selectedUnit.GetComponent<SP_Unit>().AttackPattern;
//        localX = (int)selectedUnit.transform.position.x;
//        localZ = (int)selectedUnit.transform.position.z;

//        foreach (int[] i in pattern)
//        {
//            // Koordinaten des Pattern in Weltkoordinaten umwandeln
//            int worldX = i[0] + localX;
//            int worldZ = i[1] + localZ;

//            // Vergleiche Koordinaten von Muster und Felder, wenn diese übereinstimmen 
//            foreach (GameObject g in lists.TileList)
//            {
//                if (worldX == g.transform.position.x && worldZ == g.transform.position.z)
//                {
//                    methodToCall(g,material.TileAttackMaterial);
//                    break;
//                }
//            }
//        }
//    }
//}

/*
// Beschreibt was passieren soll wenn etwas angeklickt wird
public void SelectionActions()
{
    if (Input.GetMouseButtonDown(0))
    {
        // Erstelle ein Raycast

        hitInfo = new RaycastHit();
        hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);

        // Ins Leere geklickt
        //
        // [Einheit nicht ausgewählt]
        // Sound abspielen
        //
        // [Einheit ausgewählt]
        // Entfärben, abwählen, Sound abspielen, Switch betätigen

        if (!hit && !EventSystem.current.IsPointerOverGameObject())
        {
            if (!unitSelected)
            {
                sound.playSound(sound.SoundSource, sound.ClickSound);
            }

            else if (unitSelected)
            {
                DeselectAll();
            }
        }

        // Leeres Feld angeklickt
        //
        // [Einheit nicht ausgewählt]
        // Sound abspielen
        //
        // [Einheit ausgewählt]
        // Einheit auf leeres Feld bewegen

        else if (hitInfo.collider is BoxCollider && !hitInfo.transform.GetComponent<SP_Tile>().CurrentUnit)
        {
            if (!unitSelected)
            {
                sound.playSound(sound.SoundSource, sound.ClickSound);
            }

            else if (unitSelected)
            {
                //playerActions.move(HitInfo.transform.position);

                if (SelectedUnit.GetComponent<SP_Unit>().IsActiv == true)
                {
                    selectionActions.Move();
                }
            }
        }

        // Feld mit Feind angeklickt oder feindliche Einheit direkt getroffen
        //
        // [Einheit nicht ausgewählt]
        // Einheit und Feld auswählen und färben
        // Klicksound abspielen
        // Switch auf Einheit ist ausgewählt wechseln
        //
        // [Einheit ausgewählt]
        // Angeklicktes Feld mit Einheit bzw Einheit selber wird angegriffen

        else if ((hitInfo.collider is BoxCollider && hitInfo.transform.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().Team != controller.GetComponent<SP_Controller>().PlayerTeam) || (hitInfo.collider is CapsuleCollider && hitInfo.transform.GetComponentInChildren<SP_Unit>().Team != controller.GetComponent<SP_Controller>().PlayerTeam))
        {
            if (!unitSelected)
            {
                SelectObjects();
                material.SetMaterial(selectedUnit, material.EnemySelectedMaterial);
                material.SetMaterial(selectedTile, material.EnemySelectedMaterial);

                sound.playSound(sound.SoundSource, sound.ButtonSound);

                unitSelected = !unitSelected;
            }

            else if (unitSelected)
            {
                selectionActions.attack();

                sound.playSound(sound.SoundSource, sound.ButtonSound);
            }
        }

        // Feld mit Verbündeten oder verbündete Einheit direkt angeklickt
        //
        // [Einheit nicht ausgewählt]
        // Einheit und Feld auswählen und färben
        // Klicksound abspielen
        // Switch auf Einheit ist ausgewählt wechseln
        //
        // [Einheit ausgewählt]
        // Einheit des Spielers verschmilzt mit verbündeter neuer Einheit

        else if ((hitInfo.collider is BoxCollider && hitInfo.transform.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().Team == controller.GetComponent<SP_Controller>().PlayerTeam) || (hitInfo.collider is CapsuleCollider && hitInfo.transform.GetComponent<SP_Unit>().Team == controller.GetComponent<SP_Controller>().PlayerTeam))
        {
            if (!unitSelected)
            {
                SelectObjects();
                material.SetMaterial(selectedUnit, material.AllySelectedMaterial);
                material.SetMaterial(selectedTile, material.AllySelectedMaterial);

                ColorPattern();

                sound.playSound(sound.SoundSource, sound.ButtonSound);

                unitSelected = !unitSelected;
            }

            else if (unitSelected)
            {
                selectionActions.fusion();

                sound.playSound(sound.SoundSource, sound.ButtonSound);
            }
        }
    }

    // Irgendwo Rechtsklick gemacht
    //
    // [Einheit nicht ausgewählt]
    // Klicksound abspielen
    //
    // [Einheit ausgewählt]
    // Klicksound abspielen
    // Wähle Einheit und Feld ab

    if (Input.GetMouseButtonDown(1))
    {
        if (!unitSelected)
        {
            sound.playSound(sound.SoundSource, sound.ClickSound);
        }

        else if (unitSelected)
        {
            DeselectAll();
        }
    }
}
*/
