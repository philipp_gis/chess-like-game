﻿using System.Collections.Generic;
using UnityEngine;
using static SP_Enum.team;
using static SP_Enum.tileReachability;
using static SP_Enum.gameState;
using static SP_Enum.enemyState;
using static SP_Enum.animationEnemy;

public class SP_EnemyBot : MonoBehaviour
{
    // Verwaltung des Bots

    #region Header

    //private bool enemyTeam;
    private bool isAnimating;
    private bool newRound;

    public List<GameObject> activeUnits;
    public List<string> activActions;
    public List<GameObject> possibleMoveables;
    public List<GameObject> possibleAttackables;
    public List<GameObject> possibleFusionables;
    public GameObject target;
    public Vector3 startPosition;
    public Vector3 endPosition;
    public float startTime;
    public float endTime;

    private SP_Controller controller;
    private SP_Lists lists;
    private SP_Selection selection;
    private SP_Material material;
    private SP_Sound sound;
    private SP_UI ui;

    public void Awake()
    {
        controller = GetComponent<SP_Controller>();
        lists = GetComponent<SP_Lists>();
        selection = GetComponent<SP_Selection>();
        material = GetComponent<SP_Material>();
        sound = GetComponent<SP_Sound>();
        ui = GetComponent<SP_UI>();

        activActions = new List<string> { "Move", "Attack", "Fusion" };
        isAnimating = false;
        newRound = true;
    }

    #endregion

    #region Methoden

    public void ActivUnitCheck()
    {
        // Prüft ob es noch Einheiten gibt die nicht passiv sind

        // Der Skipbutton kann in der Gegnerrunde nicht benutzt werden
        ui.SkipRoundButton.interactable = false;

        if (newRound)
        {
            // Liste soll neu angelget werden wenn eine neue Runde für den Bot anfängt
            activeUnits = new List<GameObject> ();

            // Je nach dem welches Team der Bot hat, schaut er welche Einheiten noch eine Aktion ausführen können und packt diese in eine Liste
            if (controller.EnemyTeam == TEAM_RED)
            {
                foreach (GameObject g in lists.TeamRedList)
                {
                    if (g.GetComponentInChildren<SP_Unit>().IsActiv)
                    {
                        activeUnits.Add(g);
                    }
                }
            }

            else if (controller.EnemyTeam == TEAM_BLUE)
            {
                foreach (GameObject g in lists.TeamBlueList)
                {
                    if (g.GetComponentInChildren<SP_Unit>().IsActiv)
                    {
                        activeUnits.Add(g);
                    }
                }
            }

            newRound = false;

            SelectRandomUnit(); 
        }
    }
    void SelectRandomUnit()
    {
        // Wähle zufällig eine Einheit aus
        
        int random = Random.Range(0, activeUnits.Count);
        
        selection.SelectedUnit = activeUnits[random];
        selection.SelectedTile = activeUnits[random].GetComponentInChildren<SP_Unit>().CurrentTile;

        // Merke dir Lokal die Pattern und die Position der Einheit
        List<int[]> attackPattern;
        List<int[]> movePattern;

        int localX;
        int localZ;

        attackPattern = selection.SelectedUnit.GetComponentInChildren<SP_Unit>().AttackPattern;
        movePattern = selection.SelectedUnit.GetComponentInChildren<SP_Unit>().MovePattern;
        localX = (int)selection.SelectedUnit.transform.position.x;
        localZ = (int)selection.SelectedUnit.transform.position.z;


        foreach (int[] i in attackPattern)
        {
            // Koordinaten des Pattern in Weltkoordinaten umwandeln
            int worldX = i[0] + localX;
            int worldZ = i[1] + localZ;

            // Vergleiche Koordinaten von Muster und Felder, wenn diese übereinstimmen 
            foreach (GameObject g in lists.TileList)
            {
                if (worldX == g.transform.position.x && worldZ == g.transform.position.z)
                {
                    g.GetComponent<SP_Tile>().Type = ATTACKABLE;
                    break;
                }
            }
        }

        foreach (int[] i in movePattern)
        {
            // Koordinaten des Pattern in Weltkoordinaten umwandeln
            int worldX = i[0] + localX;
            int worldZ = i[1] + localZ;

            // Vergleiche Koordinaten von Muster und Felder, wenn diese übereinstimmen 
            foreach (GameObject g in lists.TileList)
            {
                if (worldX == g.transform.position.x && worldZ == g.transform.position.z)
                {
                    if (g.GetComponent<SP_Tile>().Type == ATTACKABLE)
                    {
                        g.GetComponent<SP_Tile>().Type = BOTHABLE;
                    }
                    else
                    {
                        g.GetComponent<SP_Tile>().Type = MOVEABLE;
                    }
                    break;
                }
            }
        }
        PossibilityCheck();
    }

    void PossibilityCheck()
    {
        // Welche Aktion ist dem Bot möglich

        // Listen sollen neu angelget werden wenn eine neue Einheit vom Bot ausgewählt wurde
        activActions = new List<string> { "Move", "Attack", "Fusion" };
        possibleMoveables = new List<GameObject> ();
        possibleAttackables = new List<GameObject> ();
        possibleFusionables = new List<GameObject> ();

        // Packt jedes Feld das über das Bewegungsmuster erreicht werden kann und keine Einheit besitzt in die Bewegungsliste
        foreach (GameObject g in lists.TileList)
        {
            if (g.GetComponent<SP_Tile>().Type == MOVEABLE || g.GetComponent<SP_Tile>().Type == BOTHABLE)
            {
                if (!g.GetComponent<SP_Tile>().CurrentUnit)
                {
                    possibleMoveables.Add(g);
                }
            }
        }

        // Wenn die Bewegungsliste leer ist wird dem Bot die Auswahl der Bewegung genommen
        if (possibleMoveables.Count == 0)
        {
            activActions.Remove("Move");
        }

        // Packt jedes Feld das über das Angriffsmuster erreicht werden kann und eine dem Bot feindliche Einheit besitzt in die Angriffsliste
        foreach (GameObject g in lists.TileList)
        {
            if (g.GetComponent<SP_Tile>().Type == ATTACKABLE || g.GetComponent<SP_Tile>().Type == BOTHABLE)
            {
                if (g.GetComponent<SP_Tile>().CurrentUnit && g.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().Team != controller.EnemyTeam)
                {
                    possibleAttackables.Add(g);
                }
            }
        }

        // Wenn die Angriffsliste leer ist wird dem Bot die Auswahl der Bewegung genommen
        if (possibleAttackables.Count == 0)
        {
            activActions.Remove("Attack");
        }

        // Packt jedes Feld das über das Bewegungsmuster erreicht werden kann und eine dem Bot freundliche Einheit besitzt in die Fusionsliste
        foreach (GameObject g in lists.TileList)
        {
            if (g.GetComponent<SP_Tile>().Type == MOVEABLE || g.GetComponent<SP_Tile>().Type == BOTHABLE)
            {
                if (g.GetComponent<SP_Tile>().CurrentUnit && g.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().Team == controller.EnemyTeam)
                {
                    possibleFusionables.Add(g);
                }
            }
        }

        // Wenn die Angriffsliste leer ist wird dem Bot die Auswahl der Bewegung genommen
        if (possibleFusionables.Count == 0)
        {
            activActions.Remove("Fusion");
        }

        RandomIntention();
    }


    void RandomIntention()
    {
        // Bot wählt zufällig eine Aktion aus die er mit der ausgewählten Einheit ausführen möchte

        // Wenn die Aktionsliste des Bots leer ist, setzt er die ausgewählte Einheit passiv und vergisst diese
        if (activActions.Count == 0)
        {
            // Einheit wird aus den vom Bot noch auswählbaren Einheiten entfernt
            activeUnits.Remove(selection.SelectedUnit);

            selection.SelectedUnit.GetComponentInChildren<SP_Unit>().IsActiv = false;
            
            if(controller.EnemyTeam == TEAM_RED)
            {
                material.SetMaterial(selection.SelectedUnit, material.TeamRedPassivMaterial);
            }
            else if (controller.EnemyTeam == TEAM_BLUE)
            {
                material.SetMaterial(selection.SelectedUnit, material.TeamBluePassivMaterial);
            }

            selection.SelectedTile = null;
            selection.SelectedUnit = null;

            SelectRandomUnit();
        }

        // Der Bot wählt zufällig eine Aktion aus den ihm verbleibenden Aktionen aus und setzt sein Ziel zufällig.
        else if (activActions.Count > 0)
        {
            int random = Random.Range(0, activActions.Count);

            if (activActions[random] == "Move")
            {
                controller.CurrentEnemyState = MOVE;

                // Bot wählt zufällig wohin er die Aktion ausführen möchte
                target = possibleMoveables[Random.Range(0, possibleMoveables.Count)];
            }

            else if (activActions[random] == "Attack")
            {
                controller.CurrentEnemyState = ATTACK;

                // Bot wählt zufällig wohin er die Aktion ausführen möchte
                target = possibleAttackables[Random.Range(0, possibleAttackables.Count)];
            }

            else if (activActions[random] == "Fusion")
            {
                controller.CurrentEnemyState = FUSION;

                // Bot wählt zufällig wohin er die Aktion ausführen möchte
                target = possibleFusionables[Random.Range(0, possibleFusionables.Count)];
            }

            LerpAnimation();
        }
    }

    void LerpAnimation()
    {
        // Führt ein Lerp mit Animation zum vom Bot ausgewählten Ziel

        // Ermittelt die Position der ausgewählten Einheit
        float playerHorizontalPosition = selection.SelectedUnit.transform.position.x;
        float playerVerticalPosition = selection.SelectedUnit.transform.position.z;
        Vector3 playerTransform = new Vector3(playerHorizontalPosition, 1, playerVerticalPosition);

        // Ermittelt die angeklickte Zielposition
        float targetHorizontalPosition = target.transform.position.x;
        float targetVerticalPosition = target.transform.position.z;
        Vector3 targetTransform = new Vector3(targetHorizontalPosition, 1, targetVerticalPosition);

        // Lege Start und Endpunkt für LERP fest
        startPosition = playerTransform;
        endPosition = targetTransform;

        // Lege LERP Dauer fest
        startTime = Time.time;
        endTime = 1f;

        selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", true);
        sound.playSound(sound.SoundSource, sound.MoveSound);

        controller.AnimationsEnemy = IN_ANIMATION;
    }

    private void FixedUpdate()
    {
        // LERP

        if (controller.AnimationsEnemy == IN_ANIMATION && controller.CurrentTeam == controller.EnemyTeam)
        {
            float actualTime;
            float progress;

            // Bestimmt den Moment der Zeitmessung und wandelt es in den prozentualen Anteil des gesamten LERP
            actualTime = Time.time - startTime;
            progress = actualTime / endTime;

            // Positioniere Einheit in die Position des Lerp anhand des Fortschritts der Gesamtstrecke
            selection.SelectedUnit.transform.position = Vector3.Lerp(startPosition, endPosition, progress);

            // Wenn 100%  des LERP erreicht wurde

            if (progress >= 1.0f)
            {
                if (controller.CurrentEnemyState == MOVE)
                {
                    controller.CurrentEnemyState = POST_MOVE_WORK;
                }

                else if (controller.CurrentEnemyState == ATTACK)
                {
                    controller.CurrentEnemyState = POST_ATTACK_WORK;
                }

                else if (controller.CurrentEnemyState == FUSION)
                {
                    controller.CurrentEnemyState = POST_FUSION_WORK;
                }

                controller.AnimationsEnemy = OUT_OF_ANIMATION;

                PostActionWork();
            }
        }
    }

    void PostActionWork()
    {
        if (controller.CurrentEnemyState == POST_MOVE_WORK)
        {
            // Beende Animationsloop
            selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", false);

            // Altes Feld verliert seine Referenz auf die Einheit
            selection.SelectedTile.GetComponent<SP_Tile>().CurrentUnit = null;

            // Einheit und Zielfeld setzen sich gegenseitig als Referenz
            selection.SelectedUnit.GetComponentInChildren<SP_Unit>().CurrentTile = target.transform.gameObject;
            selection.SelectedUnit.GetComponentInChildren<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().CurrentUnit = selection.SelectedUnit.transform.GetChild(0).transform.gameObject;

            // Einheit abwählen und passiv setzen
            selection.SelectedUnit.GetComponentInChildren<SP_Unit>().IsActiv = false;
            selection.SelectedTile = null;
            selection.SelectedUnit = null;

            foreach (GameObject g in lists.TileList)
            {
                if (g.GetComponent<SP_Tile>().Type != DEFAULT)
                {
                    g.GetComponent<SP_Tile>().Type = DEFAULT;
                }
            }

            // Abfrage ob alle Einheiten passiv sind
            ui.TeamPassivCheck();

            controller.CurrentEnemyState = NOT_IN_ACTION;

            newRound = true;
        }

        else if (controller.CurrentEnemyState == POST_ATTACK_WORK)
        {
            // Beende Animationsloop
            selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", false);

            // Zwischenspeichern der zu zerstörenden Unit
            GameObject unitToDestroy = target.transform.GetComponent<SP_Tile>().CurrentUnit;

            // Alte Einheit verliert seine Referenz
            selection.SelectedTile.GetComponent<SP_Tile>().CurrentUnit = null;

            // Einheit und Zielfeld setzen sich gegenseitig als Referenz
            selection.SelectedUnit.GetComponentInChildren<SP_Unit>().CurrentTile = target.transform.gameObject;
            selection.SelectedUnit.GetComponentInChildren<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().CurrentUnit = selection.SelectedUnit.transform.GetChild(0).transform.gameObject; ;

            // Angegriffene Unit wird aus der Unit-Liste entfernt
            lists.UnitList.Remove(unitToDestroy.transform.parent.gameObject);

            // Angegriffene Unit wird aus seiner Team-Liste entfernt
            if (unitToDestroy.GetComponentInChildren<SP_Unit>().Team == TEAM_BLUE)
            {
                lists.TeamBlueList.Remove(unitToDestroy.transform.parent.gameObject);
            }

            else if (unitToDestroy.GetComponentInChildren<SP_Unit>().Team == TEAM_RED)
            {
                lists.TeamRedList.Remove(unitToDestroy.transform.parent.gameObject);
            }

            Destroy(unitToDestroy.transform.parent.gameObject);

            // Einheit abwählen und passiv setzen
            selection.SelectedUnit.GetComponentInChildren<SP_Unit>().IsActiv = false;
            selection.SelectedTile = null;
            selection.SelectedUnit = null;

            foreach (GameObject g in lists.TileList)
            {
                if (g.GetComponent<SP_Tile>().Type != DEFAULT)
                {
                    g.GetComponent<SP_Tile>().Type = DEFAULT;
                }
            }

            // Abfrage ob ein Team gewonnen hat
            ui.WinCheck();

            // Abfrage ob alle Einheiten passiv sind
            ui.TeamPassivCheck();

            controller.CurrentEnemyState = NOT_IN_ACTION;

            newRound = true;
        }

        else if (controller.CurrentEnemyState == POST_FUSION_WORK)
        {
            selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", false);

            // Speichern der eigenen Move- und Attack-Pattern
            List<int[]> myMovePattern = selection.SelectedUnit.GetComponentInChildren<SP_Unit>().MovePattern;
            string myMovePatternName = selection.SelectedUnit.GetComponentInChildren<SP_Unit>().MovePatternName;
            List<int[]> myAttackPattern = selection.SelectedUnit.GetComponentInChildren<SP_Unit>().AttackPattern;
            string myAttackPatternName = selection.SelectedUnit.GetComponentInChildren<SP_Unit>().AttackPatternName;

            // Zwischenspeichern der zu zerstörenden Unit
            GameObject unitToDestroy = target.transform.GetComponent<SP_Tile>().CurrentUnit;

            // Speichern der Move- und Attack-Pattern der getroffenen Unit
            List<int[]> otherMovePattern = unitToDestroy.GetComponentInChildren<SP_Unit>().MovePattern;
            string otherMovePatternName = unitToDestroy.GetComponentInChildren<SP_Unit>().MovePatternName;
            List<int[]> otherAttackPattern = unitToDestroy.GetComponentInChildren<SP_Unit>().AttackPattern;
            string otherAttackPatternName = unitToDestroy.GetComponentInChildren<SP_Unit>().AttackPatternName;

            // Aufaddieren der Level der beiden Units
            selection.SelectedUnit.GetComponentInChildren<SP_Unit>().CurrentLevel += unitToDestroy.GetComponentInChildren<SP_Unit>().CurrentLevel;

            // Referenzen der Felder und Einheiten werden angepasst
            selection.SelectedUnit.GetComponentInChildren<SP_Unit>().CurrentTile = target.transform.gameObject;
            selection.SelectedUnit.GetComponentInChildren<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().CurrentUnit = selection.SelectedUnit.transform.GetChild(0).transform.gameObject; ;
            selection.SelectedTile.GetComponent<SP_Tile>().CurrentUnit = null;

            // Getroffene Unit wird aus der Unit-Liste entfernt
            lists.UnitList.Remove(unitToDestroy.transform.parent.gameObject);
            activeUnits.Remove(unitToDestroy.transform.parent.gameObject);

            // Getroffene Unit wird aus ihrer Team-Liste entfernt
            if (unitToDestroy.GetComponentInChildren<SP_Unit>().Team == TEAM_BLUE)
            {
                lists.TeamBlueList.Remove(unitToDestroy.transform.parent.gameObject);
            }
            else if (unitToDestroy.GetComponentInChildren<SP_Unit>().Team == TEAM_RED)
            {
                lists.TeamRedList.Remove(unitToDestroy.transform.parent.gameObject);
            }
            
            Destroy(unitToDestroy.transform.parent.gameObject);

            // Bot wählt zufällig zwischen den Mustern aus 
            bool rnd = Random.value < 0.5f;

            if (rnd)
            {
                selection.SelectedUnit.GetComponentInChildren<SP_Unit>().MovePattern = myMovePattern;
                selection.SelectedUnit.GetComponentInChildren<SP_Unit>().MovePatternName = myMovePatternName;
            }

            else if (!rnd)
            {
                selection.SelectedUnit.GetComponentInChildren<SP_Unit>().MovePattern = otherMovePattern;
                selection.SelectedUnit.GetComponentInChildren<SP_Unit>().MovePatternName = otherMovePatternName;
            }

            rnd = Random.value < 0.5f;

            if (rnd)
            {
                selection.SelectedUnit.GetComponentInChildren<SP_Unit>().AttackPattern = myAttackPattern;
                selection.SelectedUnit.GetComponentInChildren<SP_Unit>().AttackPatternName = myAttackPatternName;
            }

            else if (!rnd)
            {
                selection.SelectedUnit.GetComponentInChildren<SP_Unit>().AttackPattern = otherAttackPattern;
                selection.SelectedUnit.GetComponentInChildren<SP_Unit>().AttackPatternName = otherAttackPatternName;
            }

            // Zwischenspeichern einer temporären Liste, die aus der gesamten Pattern-Liste passend zum Level der fusionierten Unit genommen wird
            List<(List<int[]>, string)> tmp;

            if (selection.SelectedUnit.GetComponentInChildren<SP_Unit>().CurrentLevel <= 5)
            {
                tmp = new List<(List<int[]>, string)>(lists.PatternList[selection.SelectedUnit.GetComponentInChildren<SP_Unit>().CurrentLevel - 1]);
            }
            else
            {
                tmp = new List<(List<int[]>, string)>(lists.PatternList[4]);
            }

            // Generierung einer Zufallszahl im Bereich 0 - Länge von tmp
            int random = Random.Range(0, tmp.Count);

            // Bot wählt zufällig zwischen Fusionsmuster aus
            if (rnd)
            {
                selection.SelectedUnit.GetComponentInChildren<SP_Unit>().MovePattern = tmp[random].Item1;
                selection.SelectedUnit.GetComponentInChildren<SP_Unit>().MovePatternName = tmp[random].Item2;
            }

            else if (!rnd)
            {
                selection.SelectedUnit.GetComponentInChildren<SP_Unit>().AttackPattern = tmp[random].Item1;
                selection.SelectedUnit.GetComponentInChildren<SP_Unit>().AttackPatternName = tmp[random].Item2;
            }

            selection.SelectedUnit.GetComponentInChildren<SP_Unit>().IsActiv = false;
            selection.SelectedTile = null;
            selection.SelectedUnit = null;

            foreach (GameObject g in lists.TileList)
            {
                if (g.GetComponent<SP_Tile>().Type != DEFAULT)
                {
                    g.GetComponent<SP_Tile>().Type = DEFAULT;
                }
            }

            // Abfrage ob alle Einheiten passiv sind
            ui.TeamPassivCheck();

            controller.CurrentEnemyState = NOT_IN_ACTION;

            newRound = true;
        }
    }

    #endregion
}