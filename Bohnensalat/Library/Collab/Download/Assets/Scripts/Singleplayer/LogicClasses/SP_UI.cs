﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using static SP_Enum.team;
using static SP_Enum.gameState;
using static SP_Enum.action;
using static SP_Enum.isAnimation;

public class SP_UI : MonoBehaviour
{
    // Verwaltung der graphischen Elemente

    #region Header

    [Header("UI References")]
    [SerializeField] private GameObject teamPanel;
    [SerializeField] private GameObject coinPanel;
    [SerializeField] private GameObject fusionPanel;
    [SerializeField] private GameObject winPanel;
    [SerializeField] private GameObject hudPanel;
    [SerializeField] private GameObject patternPanel;

    [SerializeField] private Button fusionChoice1;
    [SerializeField] private Button fusionChoice2;
    [SerializeField] private Button fusionChoice3;

    [SerializeField] private Button fusionOwnMove;
    [SerializeField] private Button fusionOwnAttack;
    [SerializeField] private Button fusionOtherMove;
    [SerializeField] private Button fusionOtherAttack;

    [SerializeField] private Button skipRoundButton;

    // Text des Rundenanzeigers im Spiel
    [Header("Text References")]
    [SerializeField] private Text roundText;
    [SerializeField] private Text winText;

    private SP_Controller controller;
    private SP_Lists lists;
    private SP_Material material;
    private SP_Sound sound;
    private SP_Animation animation;
    private SP_Selection selection;
    private SP_Board board;

    // Rundenzähler
    private int roundCount = 1;

    // Zwischenspeicher der Pattern für das PatternPanel
    List<int[]> myMovePattern;
    List<int[]> myAttackPattern;
    List<int[]> otherMovePattern;
    List<int[]> otherAttackPattern;

    string myMovePatternName;
    string myAttackPatternName;
    string otherMovePatternName;
    string otherAttackPatternName;

    // Speicher für Buttons, welches Pattern zugewiesen wird und ob Move- oder Attack-Pattern
    private (List<int[]>, string, bool) choice1;
    private (List<int[]>, string, bool) choice2;
    private (List<int[]>, string, bool) choice3;

    public void Awake()
    {
        controller = GetComponent<SP_Controller>();
        lists = GetComponent<SP_Lists>();
        material = GetComponent<SP_Material>();
        sound = GetComponent<SP_Sound>();
        animation = GetComponent<SP_Animation>();
        selection = GetComponent<SP_Selection>();
        board = GetComponent<SP_Board>();
    }

    #endregion



    #region Methoden

    #region Teamfenster
    public void ClickedOnTeamBlueButton()
    {
        // Spieler wählt ein Team aus

        teamPanel.SetActive(false);
        
        controller.PlayerTeam = TEAM_BLUE;
        controller.EnemyTeam = TEAM_RED;

        // Starte Kameraschwenk auf Team Rot und weist Spieler Team Rot zu
        animation.playAnimation(animation.CameraAnimator, "CameraSetTeamBlue");

        // Blende Teamauswahl aus und blende nach 1.1 Sekunden Münzwurf ein damit der nächste Zustand erreicht wird
        Invoke("ActiveCoinPanel", 1.1f);

        // Spiele Klicksound ab
        sound.playSound(sound.SoundSource, sound.ButtonSound);
    }

    public void ClickedOnTeamRedButton()
    {
        // Spieler wählt ein Team aus

        teamPanel.SetActive(false);

        controller.PlayerTeam = TEAM_RED;
        controller.EnemyTeam = TEAM_BLUE;

        // Starte Kameraschwenk auf Team Blau und weist Spieler Team Rot zu
        animation.playAnimation(animation.CameraAnimator, "CameraSetTeamRed");

        // Blende Teamauswahl aus und blende nach 1.1 Sekunden Münzwurf ein damit der nächste Zustand erreicht wird
        Invoke("ActiveCoinPanel", 1.1f);

        // Spiele Klicksound ab
        sound.playSound(sound.SoundSource, sound.ButtonSound);
    }

    #endregion

    #region Münzwurffenster

    public void ClickedOnCoinTossButton()
    {
        // Spieler entscheidet über Münzwurf wer anfängt

        // Blende Münzwurffenster aus
        coinPanel.SetActive(false);

        // Färbe zufällig ein Team passiv, das andere Team fängt das Spiel an
        if (Random.value < 0.5)
        {
            foreach (GameObject g in lists.TeamRedList)
            {
                material.SetMaterial(g, material.TeamRedPassivMaterial);
                g.GetComponentInChildren<SP_Unit>().IsActiv = false;
            }

            controller.CurrentTeam = TEAM_BLUE;

            // Blende HUD ein
            hudPanel.SetActive(true);
        }

        else
        {
            foreach (GameObject g in lists.TeamBlueList)
            {
                material.SetMaterial(g, material.TeamBluePassivMaterial);
                g.GetComponentInChildren<SP_Unit>().IsActiv = false;
            }

            controller.CurrentTeam = TEAM_RED;
            hudPanel.SetActive(true);
        }

        // Schalte Spieleraktionen frei und somit kommt der Spieler in den nächsten Zustand
        if (controller.CurrentTeam == controller.PlayerTeam)
        {
            controller.CurrentGameState = SELECTION_STATE;
        }
        //else
        //{
        //    controller.CurrentGameState = ENEMY_TURN_STATE;
        //}

        // Spiele KnopfSound ab
        sound.playSound(sound.SoundSource, sound.ButtonSound);
    }

    #endregion

    #region HUDFenster

    public void ClickedOnSkipButton()
    {
        // Spieler überspringt eine Runde

        // Wechselt welches Team am Zug ist
        if (controller.CurrentTeam == TEAM_RED)
        {
            // Färbt das Team des Spielers passiv und das andere Team aktiv
            foreach (GameObject g in lists.TeamRedList)
            {
                material.SetMaterial(g, material.TeamRedPassivMaterial);
                g.GetComponentInChildren<SP_Unit>().IsActiv = false;
            }

            foreach (GameObject g in lists.TeamBlueList)
            {
                material.SetMaterial(g, material.TeamBlueActiveMaterial);
                g.GetComponentInChildren<SP_Unit>().IsActiv = true;
            }
        }

        else if (controller.CurrentTeam == TEAM_BLUE)
        {
            foreach (GameObject g in lists.TeamBlueList)
            {
                material.SetMaterial(g, material.TeamBluePassivMaterial);
                g.GetComponentInChildren<SP_Unit>().IsActiv = false;
            }

            foreach (GameObject g in lists.TeamRedList)
            {
                material.SetMaterial(g, material.TeamRedActiveMaterial);
                g.GetComponentInChildren<SP_Unit>().IsActiv = true;
            }
        }

        // Wähle alle ausgewählten Einheiten und Felder ab
        selection.DeselectSelectedUnit();

        if (controller.CurrentTeam == TEAM_BLUE)
        {
            controller.CurrentTeam = TEAM_RED;
        }
        else
        {
            controller.CurrentTeam = TEAM_BLUE;
        }

        if (controller.CurrentTeam == controller.PlayerTeam)
        {
            controller.CurrentGameState = SELECTION_STATE;
        }
        //else
        //{
        //    controller.CurrentGameState = ENEMY_TURN_STATE;
        //}

        // Erhöhe Rundenindex und überschreibe Rundentext
        roundCount++;
        roundText.text = "Round: " + roundCount;

        board.DestroyTile();
        WinCheck();

        sound.playSound(sound.SoundSource, sound.ButtonSound);
    }

    #endregion

    #region Patternfenster

    // PatternPanel
    public void RememberPatternForPatternPanel()
    {
        // Speichern der eigenen Move- und Attack-Pattern
        myMovePattern = selection.SelectedUnit.GetComponent<SP_Unit>().MovePattern;
        myMovePatternName = selection.SelectedUnit.GetComponent<SP_Unit>().MovePatternName;
        myAttackPattern = selection.SelectedUnit.GetComponent<SP_Unit>().AttackPattern;
        myAttackPatternName = selection.SelectedUnit.GetComponent<SP_Unit>().AttackPatternName;

        // Wenn eine Unit getroffen wird
        if (selection.HitInfo.collider is CapsuleCollider)
        {
            // Speichern der Move- und Attack-Pattern der getroffenen Unit
            otherMovePattern = selection.HitInfo.transform.GetComponent<SP_Unit>().MovePattern;
            otherMovePatternName = selection.HitInfo.transform.GetComponent<SP_Unit>().MovePatternName;
            otherAttackPattern = selection.HitInfo.transform.GetComponent<SP_Unit>().AttackPattern;
            otherAttackPatternName = selection.HitInfo.transform.GetComponent<SP_Unit>().AttackPatternName;

            // Aufaddieren der Level der beiden Units
            selection.SelectedUnit.GetComponent<SP_Unit>().CurrentLevel += selection.HitInfo.transform.GetComponent<SP_Unit>().CurrentLevel;

            // Referenzen der Felder und Einheiten werden angepasst
            selection.SelectedUnit.GetComponent<SP_Unit>().CurrentTile = selection.HitInfo.transform.GetComponent<SP_Unit>().CurrentTile;
            selection.SelectedUnit.GetComponent<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().CurrentUnit = selection.SelectedUnit;
            selection.SelectedTile.GetComponent<SP_Tile>().CurrentUnit = null;

            // Getroffene Unit wird aus der Unit-Liste entfernt
            lists.UnitList.Remove(selection.HitInfo.transform.parent.gameObject);

            // Getroffene Unit wird aus ihrer Team-Liste entfernt
            if (selection.HitInfo.transform.GetComponent<SP_Unit>().Team == TEAM_BLUE)
            {
                lists.TeamBlueList.Remove(selection.HitInfo.transform.parent.gameObject);
            }

            else if (selection.HitInfo.transform.GetComponent<SP_Unit>().Team == TEAM_RED)
            {
                lists.TeamRedList.Remove(selection.HitInfo.transform.parent.gameObject);
            }

            Destroy(selection.HitInfo.transform.parent.gameObject);
        }

        // Wenn ein Tile getroffen wird
        else if (selection.HitInfo.collider is BoxCollider)
        {
            // Zwischenspeichern der zu zerstörenden Unit
            GameObject unitToDestroy = selection.HitInfo.transform.GetComponent<SP_Tile>().CurrentUnit;

            // Speichern der Move- und Attack-Pattern der getroffenen Unit
            otherMovePattern = unitToDestroy.GetComponent<SP_Unit>().MovePattern;
            otherMovePatternName = unitToDestroy.GetComponent<SP_Unit>().MovePatternName;
            otherAttackPattern = unitToDestroy.GetComponent<SP_Unit>().AttackPattern;
            otherAttackPatternName = unitToDestroy.GetComponent<SP_Unit>().AttackPatternName;

            // Aufaddieren der Level der beiden Units
            selection.SelectedUnit.GetComponent<SP_Unit>().CurrentLevel += unitToDestroy.GetComponent<SP_Unit>().CurrentLevel;

            // Referenzen der Felder und Einheiten werden angepasst
            selection.SelectedUnit.GetComponent<SP_Unit>().CurrentTile = selection.HitInfo.transform.gameObject;
            selection.SelectedUnit.GetComponent<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().CurrentUnit = selection.SelectedUnit;
            selection.SelectedTile.GetComponent<SP_Tile>().CurrentUnit = null;

            // Getroffene Unit wird aus der Unit-Liste entfernt
            lists.UnitList.Remove(unitToDestroy.transform.parent.gameObject);

            // Getroffene Unit wird aus ihrer Team-Liste entfernt
            if (unitToDestroy.GetComponent<SP_Unit>().Team == TEAM_BLUE)
            {
                lists.TeamBlueList.Remove(unitToDestroy.transform.parent.gameObject);
            }
            else if (unitToDestroy.GetComponent<SP_Unit>().Team == TEAM_RED)
            {
                lists.TeamRedList.Remove(unitToDestroy.transform.parent.gameObject);
            }

            Destroy(unitToDestroy.transform.parent.gameObject);
        }
    }

    public void SetPatternButtonName()
    {
        fusionOwnMove.GetComponentInChildren<Text>().text = myMovePatternName;
        fusionOwnAttack.GetComponentInChildren<Text>().text = myAttackPatternName;
        fusionOtherMove.GetComponentInChildren<Text>().text = otherMovePatternName;
        fusionOtherAttack.GetComponentInChildren<Text>().text = otherAttackPatternName;
    }

    public void PatternPanelEqualityCheck()
    {
        if (myMovePatternName == otherMovePatternName)
        {
            fusionOwnMove.interactable = false;
            fusionOtherMove.interactable = false;
        }

        if (myAttackPatternName == otherAttackPatternName)
        {
            fusionOwnAttack.interactable = false;
            fusionOtherAttack.interactable = false;
        }
        PatternPanelCheck();
    }

    public void ClickedOnMyMovePatternButton()
    {
        sound.playSound(sound.SoundSource, sound.LevelUpSound);

        selection.SelectedUnit.GetComponent<SP_Unit>().MovePattern = myMovePattern;
        selection.SelectedUnit.GetComponent<SP_Unit>().MovePatternName = myMovePatternName;

        fusionOwnMove.interactable = false;
        fusionOtherMove.interactable = false;

        PatternPanelCheck();
    }

    public void ClickedOnMyAttackPatternButton()
    {
        sound.playSound(sound.SoundSource, sound.LevelUpSound);

        selection.SelectedUnit.GetComponent<SP_Unit>().AttackPattern = myAttackPattern;
        selection.SelectedUnit.GetComponent<SP_Unit>().AttackPatternName = myAttackPatternName;

        fusionOwnAttack.interactable = false;
        fusionOtherAttack.interactable = false;

        PatternPanelCheck();
    }

    public void ClickedOnOtherMovePatternButton()
    {
        sound.playSound(sound.SoundSource, sound.LevelUpSound);

        selection.SelectedUnit.GetComponent<SP_Unit>().MovePattern = otherMovePattern;
        selection.SelectedUnit.GetComponent<SP_Unit>().MovePatternName = otherMovePatternName;

        fusionOtherMove.interactable = false;
        fusionOwnMove.interactable = false;

        PatternPanelCheck();
    }

    public void ClickedOnOtherAttackPatternButton()
    {
        sound.playSound(sound.SoundSource, sound.LevelUpSound);

        selection.SelectedUnit.GetComponent<SP_Unit>().AttackPattern = otherAttackPattern;
        selection.SelectedUnit.GetComponent<SP_Unit>().AttackPatternName = otherAttackPatternName;

        fusionOwnAttack.interactable = false;
        fusionOtherAttack.interactable = false;

        PatternPanelCheck();
    }

    private void PatternPanelCheck()
    {
        // Prüft ob eine Bewegungs und Angriffsmuster ausgewählt wurden und blendet Fusionsfenster ein
        
        if (fusionOtherAttack.interactable == false && fusionOwnAttack.interactable == false &&
            fusionOtherMove.interactable == false && fusionOwnMove.interactable == false)
        {
            PatternPanel.SetActive(false);
            SetPatternButtonsInteractable();
            fusionChoice3.interactable = true;
            FusionPanel.SetActive(true);
            SetFusionChoice();
        }
    }

    #endregion

    #region Fusionsfenster

    public void SetFusionChoice()
    {
        List<(List<int[]>, string)> tmp;

        // Entscheidung per Zufall, ob Move- oder Attack-Pattern (true = Move, false = AP)
        choice1.Item3 = Random.value > 0.5;
        choice2.Item3 = Random.value > 0.5;

        // Zwischenspeichern einer temporären Liste, die aus der gesamten Pattern-Liste passend zum Level der fusionierten Unit genommen wird
        if (selection.SelectedUnit.GetComponent<SP_Unit>().CurrentLevel == 2)
        {
            tmp = new List<(List<int[]>, string)>(lists.PatternList[1]);
            choice1.Item3 = false;
            choice2.Item3 = false;
        }
        else if (selection.SelectedUnit.GetComponent<SP_Unit>().CurrentLevel <= 5)
        {
            tmp = new List<(List<int[]>, string)>(lists.PatternList[selection.SelectedUnit.GetComponent<SP_Unit>().CurrentLevel - 1]);
        }
        else
        {
            tmp = new List<(List<int[]>, string)>(lists.PatternList[4]);
        }

        // Generierung einer Zufallszahl im Bereich 0 - Länge von tmp
        int random = Random.Range(0, tmp.Count);

        // Tupel für Button 1 bekommt ein Pattern aus tmp zugewiesen
        choice1.Item1 = tmp[random].Item1;
        choice1.Item2 = tmp[random].Item2;

        // Text für Button mit Beschreibung, ob es Move- oder Attack-Pattern ist
        if (choice1.Item3)
        {
            FusionChoice1.GetComponentInChildren<Text>().text = tmp[random].Item2 + " MP";
        }
        else
        {
            FusionChoice1.GetComponentInChildren<Text>().text = tmp[random].Item2 + " AP";
        }
        // Entfernen des Patterns aus der Liste, um Doppelung zu vermeiden
        tmp.RemoveAt(random);

        random = Random.Range(0, tmp.Count);
        choice2.Item1 = tmp[random].Item1;
        choice2.Item2 = tmp[random].Item2;
        if (choice2.Item3)
        {
            FusionChoice2.GetComponentInChildren<Text>().text = tmp[random].Item2 + " MP";
        }
        else
        {
            FusionChoice2.GetComponentInChildren<Text>().text = tmp[random].Item2 + " AP";
        }
        tmp.RemoveAt(random);

        if (tmp.Count != 0)
        {
            random = Random.Range(0, tmp.Count);
            choice3.Item1 = tmp[random].Item1;
            choice3.Item2 = tmp[random].Item2;
            choice3.Item3 = Random.value > 0.5;
            if (choice3.Item3)
            {
                FusionChoice3.GetComponentInChildren<Text>().text = tmp[random].Item2 + " MP";
            }
            else
            {
                FusionChoice3.GetComponentInChildren<Text>().text = tmp[random].Item2 + " AP";
            }
            tmp.RemoveAt(random);
        }
        else
        {
            fusionChoice3.interactable = false;
            fusionChoice3.GetComponentInChildren<Text>().text = "";
        }
    }

    public void ClickedOnChoice1()
    {
        sound.playSound(sound.SoundSource, sound.LevelUpSound);

        // Setzen der Variable, dass Button 1 gewählt wurde
        if (choice1.Item3)
        {
            selection.SelectedUnit.GetComponent<SP_Unit>().MovePattern = choice1.Item1;
            selection.SelectedUnit.GetComponent<SP_Unit>().MovePatternName = choice1.Item2;
        }
        else
        {
            selection.SelectedUnit.GetComponent<SP_Unit>().AttackPattern = choice1.Item1;
            selection.SelectedUnit.GetComponent<SP_Unit>().AttackPatternName = choice1.Item2;
        }
        controller.CurrentAction = NOT_IN_ACTION;
        controller.Animations = OUT_OF_ANIMATION;
        selection.DeselectSelectedUnit();
        skipRoundButton.interactable = !skipRoundButton.interactable;

        FusionPanel.SetActive(false);
        TeamPassivCheck();

    }

    public void ClickedOnChoice2()
    {
        sound.playSound(sound.SoundSource, sound.LevelUpSound);

        if (choice2.Item3)
        {
            selection.SelectedUnit.GetComponent<SP_Unit>().MovePattern = choice2.Item1;
            selection.SelectedUnit.GetComponent<SP_Unit>().MovePatternName = choice2.Item2;
        }
        else
        {
            selection.SelectedUnit.GetComponent<SP_Unit>().AttackPattern = choice2.Item1;
            selection.SelectedUnit.GetComponent<SP_Unit>().AttackPatternName = choice2.Item2;
        }
        controller.CurrentAction = NOT_IN_ACTION;
        controller.Animations = OUT_OF_ANIMATION;
        selection.DeselectSelectedUnit();
        skipRoundButton.interactable = !skipRoundButton.interactable;

        FusionPanel.SetActive(false);
        TeamPassivCheck();

    }

    public void ClickedOnChoice3()
    {
        sound.playSound(sound.SoundSource, sound.LevelUpSound);

        if (choice3.Item3)
        {
            selection.SelectedUnit.GetComponent<SP_Unit>().MovePattern = choice3.Item1;
            selection.SelectedUnit.GetComponent<SP_Unit>().MovePatternName = choice3.Item2;
        }
        else
        {
            selection.SelectedUnit.GetComponent<SP_Unit>().AttackPattern = choice3.Item1;
            selection.SelectedUnit.GetComponent<SP_Unit>().AttackPatternName = choice3.Item2;
        }
        controller.CurrentAction = NOT_IN_ACTION;
        controller.Animations = OUT_OF_ANIMATION;
        selection.DeselectSelectedUnit();
        skipRoundButton.interactable = !skipRoundButton.interactable;

        FusionPanel.SetActive(false);
        TeamPassivCheck();
    }

    #endregion

    #region Gewonnenfenster

    public void ClickedOnRematchButton()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Singleplayer");
    }

    public void ClickedOnMainmenuButton()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Mainmenu");
    }

    #endregion

    #endregion

    #region Hilfsmethoden

    public void DisableUI()
    {
        // Blendet von jeder Einheit die UI aus
        foreach (GameObject g in lists.UnitList)
        {
            g.GetComponentInChildren<Canvas>().gameObject.SetActive(false);
        }

        // Blendet jedes UI Element im Spiel aus
        coinPanel.SetActive(false);
        fusionPanel.SetActive(false);
        winPanel.SetActive(false);
        teamPanel.SetActive(false);
        hudPanel.SetActive(false);
        patternPanel.SetActive(false);
    }

    private void ActiveCoinPanel()
    {
        coinPanel.SetActive(true);
    }

    public void SetPatternButtonsInteractable()
    {
        // Methode zum interactable machen der Pattern-Buttons
        
        fusionOwnMove.GetComponent<Button>().interactable = true;
        fusionOtherMove.GetComponent<Button>().interactable = true;
        fusionOwnAttack.GetComponent<Button>().interactable = true;
        fusionOtherAttack.GetComponent<Button>().interactable = true;
    }

    public void SkipButtonsInteractableToggle()
    {
        skipRoundButton.interactable = !skipRoundButton.interactable;
    }

    public void WinCheck()
    {
        // Prüft ob ein Team keine Einheiten hat und gibt zurück welches Team gewonnen hat
        if (lists.TeamRedList.Count == 0)
        {
            controller.CurrentGameState = UNITY_STATE;
            SkipButtonsInteractableToggle();

            winText.text = "Team Blau hat gewonnen!";
            winPanel.SetActive(true);
        }

        else if (lists.TeamBlueList.Count == 0)
        {
            controller.CurrentGameState = UNITY_STATE;
            SkipButtonsInteractableToggle();

            winText.text = "Team Rot hat gewonnen!";
            winPanel.SetActive(true);
        }
    }

    public void TeamPassivCheck()
    {
        // Wenn alle Einheiten passiv sind wird die nächste Runde gestartet
        
        int indexFalse = 0;
        int indexTrue = 0;

        if (controller.CurrentTeam == TEAM_RED)
        {
            foreach (GameObject g in lists.TeamRedList)
            {
                if (g.GetComponentInChildren<SP_Unit>().IsActiv == false)
                {
                    indexFalse++;
                }
            }
        }

        if (controller.CurrentTeam == TEAM_BLUE)
        {
            foreach (GameObject g in lists.TeamBlueList)
            {
                if (g.GetComponentInChildren<SP_Unit>().IsActiv == false)
                {
                    indexTrue++;
                }
            }
        }

        if (indexFalse == lists.TeamRedList.Count || indexTrue == lists.TeamBlueList.Count)
        {
            ClickedOnSkipButton();
        }

        skipRoundButton.interactable = true;
    }

    #endregion

    #region Getter Setter

    public GameObject TeamPanel { get => teamPanel; }
    public GameObject CoinPanel { get => coinPanel; }
    public GameObject FusionPanel { get => fusionPanel; }
    public GameObject WinPanel { get => winPanel; }
    public GameObject HudPanel { get => hudPanel; }
    public GameObject PatternPanel { get => patternPanel; }
    public Button FusionOwnMove { get => fusionOwnMove; set => fusionOwnMove = value; }
    public Button FusionOwnAttack { get => fusionOwnAttack; set => fusionOwnAttack = value; }
    public Button FusionOtherMove { get => fusionOtherMove; set => fusionOtherMove = value; }
    public Button FusionOtherAttack { get => fusionOtherAttack; set => fusionOtherAttack = value; }
    public Button FusionChoice1 { get => fusionChoice1; set => fusionChoice1 = value; }
    public Button FusionChoice2 { get => fusionChoice2; set => fusionChoice2 = value; }
    public Button FusionChoice3 { get => fusionChoice3; set => fusionChoice3 = value; }
    public Button SkipRoundButton { get => skipRoundButton; set => skipRoundButton = value; }

    #endregion
}