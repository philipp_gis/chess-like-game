﻿using UnityEngine;
using static SP_Enum.gameState;
using static SP_Enum.action;
using static SP_Enum.isAnimation;
using static SP_Enum.selection;

public class SP_Controller : MonoBehaviour
{
    // Verwaltet den Spielablauf

    #region Header

    [Header("Class References")]
    private SP_Board board;
    private SP_UI ui;
    private SP_Selection selection;
    private SP_Actions actions;
    private SP_EnemyBot enemy;

    [Header("Global States")]
    [SerializeField] private SP_Enum.gameState currentGameState;            // Aktueller Spielzustand
    [SerializeField] private SP_Enum.team playerTeam;                       // Team des Spielers
    [SerializeField] private SP_Enum.team enemyTeam;                        // Team des Gegners
    [SerializeField] private SP_Enum.team currentTeam;                      // Team das aktuell dran ist
    [SerializeField] private SP_Enum.action currentAction;                  // Aktion die aktuell ausgeführt wird
    [SerializeField] private SP_Enum.selection currentSelection;            // Auswahl die aktuell ausgeführt wird
    [SerializeField] private SP_Enum.isAnimation isAnimation;                 // Ist eine Animation am laufen 
    [SerializeField] private SP_Enum.enemyState currentEnemyState;          // Aktueller Zustand des Feindes
    [SerializeField] private SP_Enum.animationEnemy isAnimationEnemy;       // Ist eine Animation beim Gegner am laufen

    private void Awake()
    {
        board = GetComponent<SP_Board>();
        ui = GetComponent<SP_UI>();
        selection = GetComponent<SP_Selection>();
        actions = GetComponent<SP_Actions>();
        enemy = GetComponent<SP_EnemyBot>();

        // Default Zustände
        currentGameState = UNITY_STATE;
        currentSelection = NOTHING_SELECTED;
        currentAction = NOT_IN_ACTION;
        isAnimation = OUT_OF_ANIMATION;
        currentEnemyState = SP_Enum.enemyState.NOT_IN_ACTION;
        isAnimationEnemy = SP_Enum.animationEnemy.OUT_OF_ANIMATION;
    }

    #endregion

    #region Verwaltungsmethoden

    private void Start()
    {
        // Zum Spielstart wird das Spielfeld mit Feldern und Einheiten in Teams erstellt 
        board.SetupBoard();

        // UI Elemente werden ausgeblendet um einen optisch besseren Start zu gestalten
        ui.DisableUI();

        // Teamauswahlfenster werden eingeblendet damit der Spieler sein Team auswählen kann und der nächste Zustand erreicht wird
        ui.TeamPanel.SetActive(true);
    }

    private void Update()
    {
        // Zustandsverwaltung
        switch (currentGameState)
        {
            // Default Zustand des Spiels
            case UNITY_STATE:

                break;

            // Spieler klickt ein Objekt an und wählt dieses Objekt somit aus
            case SELECTION_STATE:

                if (currentTeam == playerTeam)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        selection.ChoseUnit();
                    }
                }
                else
                {
                    enemy.ActivUnitCheck();
                }

                break;

            // Je nach dem ob der Spieler eine Einheit des eigenen oder des feindlichen Team ausgewählt hatt, kann er Aktionen ausführen
            case ACTION_STATE:

                if (currentSelection == ENEMY)
                {
                    if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
                    {
                        // Einheit wird abgewählt
                        selection.DeselectSelectedUnit();
                    }
                }

                else if (currentSelection == ALLY)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        // Einheit kann sich Bewegen, Angreifen, Fusionieren, Abwählen
                        selection.ChoseAction();
                    }

                    if (Input.GetMouseButtonDown(1))
                    {
                        // Einheit wird abgewählt
                        selection.DeselectSelectedUnit();
                    }
                }

                break;
        }

        // Aktionsverwaltung
        switch (currentAction)
        {
            case POST_MOVE_WORK:

                actions.PostMoveWork();

                break;

            case POST_ATTACK_WORK:

                actions.PostAttackWork();

                break;

            case POST_FUSION_WORK:

                actions.PostFusionWork();

                break;
        }
    }

    #endregion

    #region Getter Setter

    public SP_Enum.gameState CurrentGameState { get => currentGameState; set => currentGameState = value; }
    public SP_Enum.action CurrentAction { get => currentAction; set => currentAction = value; }
    public SP_Enum.selection CurrentSelection { get => currentSelection; set => currentSelection = value; }
    public SP_Enum.isAnimation Animations { get => isAnimation; set => isAnimation = value; }
    public SP_Enum.enemyState CurrentEnemyState { get => currentEnemyState; set => currentEnemyState = value; }
    public SP_Enum.animationEnemy AnimationsEnemy { get => isAnimationEnemy; set => isAnimationEnemy = value; }
    public SP_Enum.team PlayerTeam { get => playerTeam; set => playerTeam = value; }
    public SP_Enum.team EnemyTeam { get => enemyTeam; set => enemyTeam = value; }
    public SP_Enum.team CurrentTeam { get => currentTeam; set => currentTeam = value; }

    #endregion
}