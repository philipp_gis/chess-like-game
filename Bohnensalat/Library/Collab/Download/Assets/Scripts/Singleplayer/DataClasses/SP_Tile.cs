﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using static SP_Enum.tileReachability;

public class SP_Tile : MonoBehaviour
{
    // Model der Felder

    #region Header

    [Header("Attributes")]
    
    // Referenz auf Einheit die auf dem Feld steht
    [SerializeField] private GameObject tileCurrentUnit;

    [SerializeField] private SP_Enum.tileReachability type;

    void Awake()
    {
        // Initialisierung des Typ der Einheit
        type = DEFAULT;
    }

    #endregion



    #region Getter Setter

    public GameObject CurrentUnit { get => tileCurrentUnit; set => tileCurrentUnit = value; }
    public SP_Enum.tileReachability Type { get => type; set => type = value; }

    #endregion
}