﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class OO_Material : NetworkBehaviour
{
    // Verwaltung von Materialien
    
    #region Header

    Material teamFalseActiveMaterial;
    Material teamTrueActiveMaterial;
    Material teamFalsePassivMaterial;
    Material teamTruePassivMaterial;
    Material allySelectedMaterial;
    Material enemySelectedMaterial;
    Material tileDefaultMaterial;
    Material tileAttackMaterial;
    Material tileMoveMaterial;
    Material tileBothableMaterial;

    private void Awake()
    {
        teamTrueActiveMaterial = (Material)Resources.Load("Material/Singleplayer/teamTrueActiveMaterial", typeof(Material));
        teamTruePassivMaterial = (Material)Resources.Load("Material/Singleplayer/teamTruePassivMaterial", typeof(Material));

        teamFalseActiveMaterial = (Material)Resources.Load("Material/Singleplayer/teamFalseActiveMaterial", typeof(Material));
        teamFalsePassivMaterial = (Material)Resources.Load("Material/Singleplayer/teamFalsePassivMaterial", typeof(Material));

        allySelectedMaterial = (Material)Resources.Load("Material/Singleplayer/allySelectedMaterial", typeof(Material));
        enemySelectedMaterial = (Material)Resources.Load("Material/Singleplayer/enemySelectedMaterial", typeof(Material));

        tileDefaultMaterial = (Material)Resources.Load("Material/Singleplayer/tileDefaultMaterial", typeof(Material));
        tileAttackMaterial = (Material)Resources.Load("Material/Singleplayer/tileAttackMaterial", typeof(Material));
        tileMoveMaterial = (Material)Resources.Load("Material/Singleplayer/tileMoveMaterial", typeof(Material));
        tileBothableMaterial = (Material)Resources.Load("Material/Singleplayer/tileBothableMaterial", typeof(Material));
    }

    #endregion



    #region Methoden

    public void SetMaterial(GameObject objectToColor, Material material)
    {
        // Färbt Objekt mit gewünschten Material
        //
        // GameObject das gefärbt werden soll als erster Parameter
        // Material das zum färben genutzt werden soll als zweiter Parameter

        objectToColor.GetComponentInChildren<Renderer>().material = material;
    }
   
    #endregion



    #region Getter Setter

    public Material TeamFalseActiveMaterial { get => teamFalseActiveMaterial; }
    public Material TeamTrueActiveMaterial { get => teamTrueActiveMaterial; }
    public Material TeamFalsePassivMaterial { get => teamFalsePassivMaterial; }
    public Material TeamTruePassivMaterial { get => teamTruePassivMaterial; }
    public Material AllySelectedMaterial { get => allySelectedMaterial; }
    public Material EnemySelectedMaterial { get => enemySelectedMaterial; }
    public Material TileDefaultMaterial { get => tileDefaultMaterial; }
    public Material TileAttackMaterial { get => tileAttackMaterial; }
    public Material TileMoveMaterial { get => tileMoveMaterial; set => tileMoveMaterial = value; }
    public Material TileBothableMaterial { get => tileBothableMaterial; set => tileBothableMaterial = value; }

    #endregion
}