﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.Events;

using static OO_Enum.tileReachability;

public class OO_Tile : NetworkBehaviour
{
    // Model der Felder

    #region Header

    [Header("Attributes")]

    // Referenz auf Einheit die auf dem Feld steht
    [SyncVar] [SerializeField] private GameObject tileCurrentUnit;

    [SyncVar] [SerializeField] private OO_Enum.tileReachability type;

    void Awake()
    {
        // Initialisierung des Typ der Einheit
        type = OO_Enum.tileReachability.DEFAULT;
    }

    #endregion



    #region Getter Setter

    public GameObject CurrentUnit { get => tileCurrentUnit; set => tileCurrentUnit = value; }
    public OO_Enum.tileReachability Type { get => type; set => type = value; }

    #endregion
}