﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class OO_Enum : NetworkBehaviour
{
    // Verwaltung aller Enumerations

    #region Header

    // Enumeration für die verschiedenen Unit-Typen
    public enum unitTypes { DEFAULT };

    // Feldabfrage für Muster
    public enum tileReachability { DEFAULT, ATTACKABLE, MOVEABLE, BOTHABLE };

    // Spielzustände
    public enum  gameState { UNITY_STATE, SELECTION_STATE, ACTION_STATE, TEAM_ONE_STATE, TEAM_TWO_STATE };

    // Aktionen der der Spieler ausführen kann
    public enum action { MOVE, POST_MOVE_WORK, ATTACK, POST_ATTACK_WORK, FUSION, POST_FUSION_WORK, NOT_IN_ACTION }

    // Objekte die der Spieler auswählen kann
    public enum selection { ENEMY, ALLY, NOTHING_SELECTED }

    // Objekte die der Spieler auswählen kann
    public enum animations { IN_ANIMATION, OUT_OF_ANIMATION }

    // Objekte die der Spieler auswählen kann
    public enum enemyState { MOVE, POST_MOVE_WORK, ATTACK, POST_ATTACK_WORK, FUSION, POST_FUSION_WORK, NOT_IN_ACTION }

    public enum animationsEnemy { IN_ANIMATION, OUT_OF_ANIMATION }

    #endregion
}