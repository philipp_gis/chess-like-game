﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.Events;

using static OO_Enum.unitTypes;

public class OO_Unit : NetworkBehaviour
{
    // Model der Einheiten

    #region Header

    [Header("Attributes")]

    // Teamzugehörigkeit und Feld Referenz
    [SerializeField] private bool team;
    // Level der Einheit
    [SerializeField] private int currentLevel;
    [SyncVar] [SerializeField] private GameObject unitCurrentTile;

    // ist die Einheit aktiv oder passiv
    [SyncVar] [SerializeField] private bool isActiv;

    // Angriffs und Bewegungsmuster
    [SerializeField] private List<int[]> movePattern;
    [SerializeField] private string movePatternName;

    [SerializeField] private List<int[]> attackPattern;
    [SerializeField] private string attackPatternName;

    private Animator unitAnimator;
    private NetworkAnimator unitNetworkAnimator;
    private OO_Enum.unitTypes type;

    void Awake()
    {
        // Initialisierung des Typ der Einheit
        type = OO_Enum.unitTypes.DEFAULT;
        currentLevel = 1;
    }

    #endregion



    #region Getter Setter

    public bool Team { get => team; set => team = value; }
    public int CurrentLevel { get => currentLevel; set => currentLevel = value; }
    public bool IsActiv { get => isActiv; set => isActiv = value; }
    public GameObject CurrentTile { get => unitCurrentTile; set => unitCurrentTile = value; }
    public List<int[]> MovePattern { get => movePattern; set => movePattern = value; }
    public List<int[]> AttackPattern { get => attackPattern; set => attackPattern = value; }
    public OO_Enum.unitTypes Type { get => type; set => type = value; }
    public Animator Animator { get => unitAnimator; set => unitAnimator = value; }
    public string MovePatternName { get => movePatternName; set => movePatternName = value; }
    public string AttackPatternName { get => attackPatternName; set => attackPatternName = value; }
    public NetworkAnimator UnitNetworkAnimator { get => unitNetworkAnimator; set => unitNetworkAnimator = value; }

    #endregion
}