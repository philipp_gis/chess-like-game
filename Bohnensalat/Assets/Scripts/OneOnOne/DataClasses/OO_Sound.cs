﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class OO_Sound : NetworkBehaviour
{
    // Verwaltung von Sounds und Musik

    #region Header

    [Header("AudioSorce References")]
    [SerializeField] private AudioSource soundSource;
    [SerializeField] private AudioSource musicSource;

    [Header("Sound References")]
    private AudioClip clickSound;
    private AudioClip buttonSound;
    private AudioClip moveSound;
    private AudioClip fusionSound;
    private AudioClip attackSound;
    private AudioClip levelUpSound;
    private AudioClip tileDestructionSound;

    private void Awake()
    {
        clickSound = (AudioClip)Resources.Load("Sounds/Klick_Sound", typeof(AudioClip));
        buttonSound = (AudioClip)Resources.Load("Sounds/Button_Sound", typeof(AudioClip));
        moveSound = (AudioClip)Resources.Load("Sounds/Move_Sound", typeof(AudioClip));
        fusionSound = (AudioClip)Resources.Load("Sounds/Fusion_Sound", typeof(AudioClip));
        attackSound = (AudioClip)Resources.Load("Sounds/Attack_Sound", typeof(AudioClip));
        levelUpSound = (AudioClip)Resources.Load("Sounds/LevelUp_Sound", typeof(AudioClip));
        tileDestructionSound = (AudioClip)Resources.Load("Sounds/TileDestruction_Sound", typeof(AudioClip));
    }

    #endregion



    #region Methoden

    public void playSound(AudioSource audioSource, AudioClip audioClip)
    {
        // Spielt gewünschte Sound oder Musik ab
        //
        // AudioSource je nachdem ob Musik oder Sound abgespielt wird, über Getter aus dieser Klasse als ersten Parameter übergeben
        // Musik oder Sound über Getter aus dieser Klasse als zweiter Parameter übergeben

        audioSource.PlayOneShot(audioClip);
    }

    #endregion



    #region Getter Setter

    public AudioSource SoundSource { get => soundSource; }
    public AudioSource MusicSource { get => musicSource; }
    public AudioClip ClickSound { get => clickSound; }
    public AudioClip ButtonSound { get => buttonSound; }
    public AudioClip MoveSound { get => moveSound; }
    public AudioClip FusionSound { get => fusionSound; set => fusionSound = value; }
    public AudioClip AttackSound { get => attackSound; set => attackSound = value; }
    public AudioClip LevelUpSound { get => levelUpSound; set => levelUpSound = value; }
    public AudioClip TileDestructionSound { get => tileDestructionSound; set => tileDestructionSound = value; }

    #endregion
}