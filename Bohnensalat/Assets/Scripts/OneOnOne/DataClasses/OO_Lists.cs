﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class OO_Lists : NetworkBehaviour
{
    // Verwaltung aller Listen

    #region Header

    // Header sind richtig, keine Ahnung warum Unity das so komisch macht
    [Header("GameObject Lists")]
    [Header("READ ONLY")]
    [SerializeField] private List<GameObject> tileList;
    [SerializeField] private List<GameObject> unitList;

    [Header("Team Lists")]
    [SerializeField] private List<GameObject> teamFalseList;
    [SerializeField] private List<GameObject> teamTrueList;

    [Header("Pattern List")]
    [SerializeField] private List<List<(List<int[]>, string)>> patternList = new List<List<(List<int[]>, string)>>();

    [SerializeField] private List<(List<int[]>, string)> patternBasic = new List<(List<int[]>, string)>();
    [SerializeField] private List<(List<int[]>, string)> patternLvl1 = new List<(List<int[]>, string)>();
    [SerializeField] private List<(List<int[]>, string)> patternLvl2 = new List<(List<int[]>, string)>();
    [SerializeField] private List<(List<int[]>, string)> patternLvl3 = new List<(List<int[]>, string)>();
    [SerializeField] private List<(List<int[]>, string)> patternLvl4 = new List<(List<int[]>, string)>();
    [SerializeField] private List<(List<int[]>, string)> patternLvl5 = new List<(List<int[]>, string)>();
    [SerializeField] private List<(List<int[]>, string)> patternLvl6 = new List<(List<int[]>, string)>();

    private void Awake()
    {
        patternList.Add(patternBasic);
        patternList.Add(patternLvl1);
        patternList.Add(patternLvl2);
        patternList.Add(patternLvl3);
        patternList.Add(patternLvl4);
        patternList.Add(patternLvl5);
        patternList.Add(patternLvl6);
    }

    #endregion



    #region Getter Setter

    public List<GameObject> TileList { get => tileList; }
    public List<GameObject> UnitList { get => unitList; }
    public List<GameObject> TeamFalseList { get => teamFalseList; }
    public List<GameObject> TeamTrueList { get => teamTrueList; }
    public List<List<(List<int[]>, string)>> PatternList { get => patternList; }
    public List<(List<int[]>, string)> PatternBasic { get => patternBasic; set => patternBasic = value; }
    public List<(List<int[]>, string)> PatternLvl1 { get => patternLvl1; set => patternLvl1 = value; }
    public List<(List<int[]>, string)> PatternLvl2 { get => patternLvl2; }
    public List<(List<int[]>, string)> PatternLvl3 { get => patternLvl3; }
    public List<(List<int[]>, string)> PatternLvl4 { get => patternLvl4; }
    public List<(List<int[]>, string)> PatternLvl5 { get => patternLvl5; }
    public List<(List<int[]>, string)> PatternLvl6 { get => patternLvl6; }

    #endregion
}