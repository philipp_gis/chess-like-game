﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OO_Animation : NetworkBehaviour
{
    // Verwaltung von Animationen und Animationsabläufen

    #region Header

    [Header("Animator References")]
    [SerializeField] private Animator cameraAnimator;

    #endregion



    #region Methoden

    public void playAnimation(Animator animator,string animation)
    {
        // Spielt gewünschte Animation ab
        //
        // Animator von gewünschten GameObject über Getter aus dieser Klasse als ersten Parameter übergeben
        // Animation in UnityProject, Namen der Animation als String als zweiter Parameter übergeben
        
        animator.Play(animation);
    }

    #endregion



    #region Getter Setter

    public Animator CameraAnimator { get => cameraAnimator; }

    #endregion
}