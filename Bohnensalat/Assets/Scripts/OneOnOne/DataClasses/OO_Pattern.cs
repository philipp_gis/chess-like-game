﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class OO_Pattern : NetworkBehaviour
{
    // Verwaltung der Angriffs und Bewegungsmuster

    #region Header

    private OO_Lists lists;

    static bool I = false;
    static bool O = true;

    public static bool[,] mpBasicPattern = new bool[,]
    {
        {I, I, I, I, I},
        {I, I, O, I, I},
        {I, O, I, O, I},
        {I, I, O, I, I},
        {I, I, I, I, I}
    };

    public static bool[,] apBasicPattern = new bool[,]
    {
        {I, I, I, I, I},
        {I, O, I, O, I},
        {I, I, I, I, I},
        {I, I, I, I, I},
        {I, I, I, I, I}
    };

    private List<int[]> mpBasic;
    private List<int[]> apBasic;

    private void Awake()
    {
        mpBasic = transformPattern(mpBasicPattern);
        apBasic = transformPattern(apBasicPattern);
        lists = GetComponent<OO_Lists>();
        initializePattern();
    }

    #endregion



    #region Methoden

    public static List<int[]> transformPattern(bool[,] pattern)
    {
        // Wandelt ein rohes Pattern so um das die Einheitwas damit anfangen kann

        // Gesamtanzahl an Feldern im Muster
        float totalLength = pattern.Length;

        // Länge einer Zeile des Musters
        float rowLength = pattern.GetLength(0);

        // Variable zur Errechnung der Reichweite des Musters (Beispiel: Einheit kann sich bis zu 3 Felder nach vorne Bewegnen)
        float mathForPatternRange = 0;
        for (int i = 0; i <= rowLength; i++)
        {
            mathForPatternRange += 0.5f;
        }

        // Maximale Reichweite in die sich eine Einheit bewegen kann
        float maxPatternRange = rowLength - mathForPatternRange;

        // Variable um Muster in Koordinaten umzurechnen
        int maxXCoordinateRange = (int)-maxPatternRange;
        int maxZCoordinateRange = (int)maxPatternRange;

        // Zähler um Variablen zurückzusetzen
        int count = 0;

        // Erstellt ein Array das alle Felder mit X und Z Koordinaten beinhaltet
        int[][] coordinateArray = new int[(int)totalLength][];

        for (int i = 0; i < totalLength; i++)
        {
            coordinateArray[i] = new int[2];

            // X Koordinate wird gesetzt
            coordinateArray[i][0] = maxXCoordinateRange++;

            // Z Koordinate wird gesetzt
            coordinateArray[i][1] = maxZCoordinateRange;

            count++;

            // Wenn eine Zeile des Musters durch ist wird der Zähler, 
            // die X Variable zurückgesetzt und Z Variable um 1 erhöht
            if (count == rowLength)
            {
                maxXCoordinateRange = (int)-maxPatternRange;
                maxZCoordinateRange--;
                count = 0;
            }
        }

        // Liste die der Einheit übergeben wird
        List<int[]> package = new List<int[]> { };

        // Geht das Pattern durch und überprüft welche Felder mit O markiert wurden
        for (int i = 0; i < rowLength; i++)
        {
            for (int j = 0; j < rowLength; j++)
            {
                if (pattern[i, j] == O)
                {
                    // Wenn ein Feld mit O markiert ist, rechne seine Position im KoordinatenArray aus
                    int x = coordinateArray[i * (int)rowLength + j][0];
                    int z = coordinateArray[i * (int)rowLength + j][1];
                    int[] field = { x, z };

                    // Übergebe Koordinaten an Liste
                    package.Add(field);
                }
            }
        }
        return package;
    }

    public List<int[]> rotatePattern(List<int[]> pattern, bool team)
    {
        // Dreht ein asymetrisches Muster je nach Team nach links oder rechts
        List<int[]> package = new List<int[]> { };

        foreach (int[] i in pattern)
        {
            int[] j = new int[] { i[0], i[1] };
            package.Add(j);
        }

        foreach (int[] i in package)
        {
            // Vorzeichen undrehen
            if (team == false)
            {
                i[0] = i[0] * -1;
            }

            else if (team == true)
            {
                i[1] = i[1] * -1;
            }

            // Taucht X und Z Koordinate miteinander aus
            int x = i[1];
            int z = i[0];

            i[0] = x;
            i[1] = z;
        }
        return package;
    }

    public void initializePattern()
    {
        // Basic Pattern
        List<(bool[,], string)> patternBasic = new List<(bool[,], string)>();
        
        bool[,] basicMP = new bool[,]
        {
            { I, O, I },
            { O, I, O },
            { I, O, I }
        };
        patternBasic.Add((basicMP, "Basic MP"));

        bool[,] basicAP = new bool[,]
        {
            { O, I, O },
            { I, I, I },
            { I, I, I }
        };
        patternBasic.Add((basicAP, "Basic AP"));

        foreach (var i in patternBasic)
        {
            lists.PatternBasic.Add((transformPattern(i.Item1), i.Item2));
        }

        // Level 1 Pattern
        List<(bool[,], string)> patternLvl1 = new List<(bool[,], string)>();

        bool[,] plus1 = new bool[,]
        {
            { I, O, I },
            { O, I, O },
            { I, O, I }
        };
        patternLvl1.Add((plus1, "Plus 1"));

        bool[,] kreuz1 = new bool[,]
        {
            { O, I, O },
            { I, I, I },
            { O, I, O }
        };
        patternLvl1.Add((kreuz1, "Kreuz 1"));

        foreach (var i in patternLvl1)
        {
            lists.PatternLvl1.Add((transformPattern(i.Item1), i.Item2));
        }

        // Level 2 Pattern
        List<(bool[,], string)> patternLvl2 = new List<(bool[,], string)>();

        bool[,] plus_2 = new bool[,]
        {
            {I, I, O, I, I},
            {I, I, O, I, I},
            {O, O, I, O, O},
            {I, I, O, I, I},
            {I, I, O, I, I}
        };
        patternLvl2.Add((plus_2, "Plus 2"));

        bool[,] kreuz_2 = new bool[,]
        {
            {O, I, I, I, O},
            {I, O, I, O, I},
            {I, I, I, I, I},
            {I, O, I, O, I},
            {O, I, I, I, O}
        };
        patternLvl2.Add((kreuz_2, "Kreuz 2"));

        bool[,] block_2 = new bool[,]
        {
            {I, I, I, I, I},
            {I, O, O, O, I},
            {I, O, I, O, I},
            {I, O, O, O, I},
            {I, I, I, I, I}
        };
        patternLvl2.Add((block_2, "Block 2"));


        foreach (var i in patternLvl2)
        {
            lists.PatternLvl2.Add((transformPattern(i.Item1), i.Item2));
        }

        // Level 3 Pattern
        List<(bool[,], string)> patternLvl3 = new List<(bool[,], string)>();

        bool[,] plus_3 = new bool[,]
        {
            {I, I, I, O, I, I, I},
            {I, I, I, O, I, I, I},
            {I, I, I, O, I, I, I},
            {O, O, O, I, O, O, O},
            {I, I, I, O, I, I, I},
            {I, I, I, O, I, I, I},
            {I, I, I, O, I, I, I}
        };
        patternLvl3.Add((plus_3, "Plus 3"));

        bool[,] kreuz_3 = new bool[,]
        {
            {O, I, I, I, I, I, O},
            {I, O, I, I, I, O, I},
            {I, I, O, I, O, I, I},
            {I, I, I, I, I, I, I},
            {I, I, O, I, O, I, I},
            {I, O, I, I, I, O, I},
            {O, I, I, I, I, I, O}

        };
        patternLvl3.Add((kreuz_3, "Kreuz 3"));

        bool[,] block_plus_3 = new bool[,]
        {
            {I, I, O, I, I},
            {I, O, O, O, I},
            {O, O, I, O, O},
            {I, O, O, O, I},
            {I, I, O, I, I}
        };
        patternLvl3.Add((block_plus_3, "Block-Plus 3"));

        bool[,] block_kreuz_3 = new bool[,]
        {
            {O, I, I, I, O},
            {I, O, O, O, I},
            {I, O, I, O, I},
            {I, O, O, O, I},
            {O, I, I, I, O}
        };
        patternLvl3.Add((block_kreuz_3, "Block-Kreuz 3"));

        foreach (var i in patternLvl3)
        {
            lists.PatternLvl3.Add((transformPattern(i.Item1), i.Item2));
        }

        // Level 4 Pattern
        List<(bool[,], string)> patternLvl4 = new List<(bool[,], string)>();

        bool[,] plus_4 = new bool[,]
        {
            {I, I, I, I, O, I, I, I, I},
            {I, I, I, I, O, I, I, I, I},
            {I, I, I, I, O, I, I, I, I},
            {I, I, I, I, O, I, I, I, I},
            {O, O, O, O, I, O, O, O, O},
            {I, I, I, I, O, I, I, I, I},
            {I, I, I, I, O, I, I, I, I},
            {I, I, I, I, O, I, I, I, I},
            {I, I, I, I, O, I, I, I, I},
            
        };
        patternLvl4.Add((plus_4, "Plus 4"));

        bool[,] kreuz_4 = new bool[,]
        {
            {O, I, I, I, I, I, I, I, O},
            {I, O, I, I, I, I, I, O, I},
            {I, I, O, I, I, I, O, I, I},
            {I, I, I, O, I, O, I, I, I},
            {I, I, I, I, I, I, I, I, I},
            {I, I, I, O, I, O, I, I, I},
            {I, I, O, I, I, I, O, I, I},
            {I, O, I, I, I, I, I, O, I},
            {O, I, I, I, I, I, I, I, O},

        };
        patternLvl4.Add((kreuz_4, "Kreuz 4"));

        bool[,] block_plus_4 = new bool[,]
        {
            {I, I, I, O, I, I, I},
            {I, I, I, O, I, I, I},
            {I, I, O, O, O, I, I},
            {O, O, O, I, O, O, O},
            {I, I, O, O, O, I, I},
            {I, I, I, O, I, I, I},
            {I, I, I, O, I, I, I}
        };
        patternLvl4.Add((block_plus_4, "Block-Plus 4"));

        bool[,] block_kreuz_4 = new bool[,]
        {
            {O, I, I, I, I, I, O},
            {I, O, I, I, I, O, I},
            {I, I, O, O, O, I, I},
            {I, I, O, I, O, I, I},
            {I, I, O, O, O, I, I},
            {I, O, I, I, I, O, I},
            {O, I, I, I, I, I, O}
        };
        patternLvl4.Add((block_kreuz_4, "Block-Kreuz 4"));

        foreach (var i in patternLvl4)
        {
            lists.PatternLvl4.Add((transformPattern(i.Item1), i.Item2));
        }
    }

    #endregion



    #region Getter Setter

    public List<int[]> ApBasic { get => apBasic; }
    public List<int[]> MpBasic { get => mpBasic; }

    #endregion

}