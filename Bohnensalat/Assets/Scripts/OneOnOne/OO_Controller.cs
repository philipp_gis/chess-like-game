﻿using UnityEngine;
using Mirror;
using UnityEngine.EventSystems;
using static OO_Enum.gameState;

public class OO_Controller : NetworkBehaviour
{
    // Verwaltet den Spielablauf

    #region Header

    [Header("Class References")]
    private OO_Board board;
    private OO_UI ui;
    private OO_Actions actions;
    private OO_Material material;
    private OO_Lists lists;
    private OO_Sound sound;
    [SerializeField] private OO_Selection hostSelection;
    [SerializeField] private OO_Selection clientSelection;
    [SerializeField] private OO_Actions hostActions;
    [SerializeField] private OO_Actions clientActions;


    [Header("Global Attributes")]
    // Dadurch kann man sagen welches Team der Spieler gehört
    [SyncVar] [SerializeField] private bool hostTeam;
    [SyncVar] [SerializeField] private bool clientTeam;

    // Damit weis man welches Team passiv gestellt werden muss
    [SyncVar] [SerializeField] private bool teamTurn;

    [Header("Global States")]
    [SyncVar] [SerializeField] private OO_Enum.gameState currentGameState;
    [SyncVar] [SerializeField] private OO_Enum.action currentAction;
    [SyncVar] [SerializeField] private OO_Enum.selection currentSelection;
    [SyncVar] [SerializeField] private OO_Enum.animations animations;
    [SyncVar] [SerializeField] private OO_Enum.enemyState currentEnemyState;
    [SyncVar] [SerializeField] private OO_Enum.animationsEnemy animationsEnemy;



    private void Awake()
    {
        board = GetComponent<OO_Board>();
        ui = GetComponent<OO_UI>();
        //actions = GetComponent<OO_Actions>();
        material = GetComponent<OO_Material>();
        lists = GetComponent<OO_Lists>();
        sound = GetComponent<OO_Sound>();

        currentGameState = OO_Enum.gameState.UNITY_STATE;
        currentSelection = OO_Enum.selection.NOTHING_SELECTED;
        currentAction = OO_Enum.action.NOT_IN_ACTION;
        animations = OO_Enum.animations.OUT_OF_ANIMATION;
        currentEnemyState = OO_Enum.enemyState.NOT_IN_ACTION;
        animationsEnemy = OO_Enum.animationsEnemy.OUT_OF_ANIMATION;

    }

    #endregion



    #region Verwaltungsmethoden
    private void Start()
    {
        // Zum Spielstart wird das Spielfeld mit Feldern und Einheiten in Teams erstellt 

        // UI Elemente werden ausgeblendet um einen optisch besseren Start zu gestalten
        ui.DisableUI();
        board.SetupBoard();
        teamTurn = hostTeam;
        // Teamauswahlfenster werden eingeblendet damit der Spieler sein Team auswählen kann und der nächste Zustand erreicht wird
    }

    private void Update()
    {
        // Zustandsverwaltung verschoben in seperate Methode, die von OO_Selection aufgerufen wird               

        // Aktionsverwaltung
        switch (currentAction)
        {
            case OO_Enum.action.POST_MOVE_WORK:

                actions.CmdPostMoveWork();

                break;

            case OO_Enum.action.POST_ATTACK_WORK:

                actions.PostAttackWork();

                break;

            case OO_Enum.action.POST_FUSION_WORK:

                actions.PostFusionWork();

                break;
        }
    }

    #endregion

    #region Methoden

    // Ausgelagerte Zustandsverwaltung, die den Input einer OO_Selection interpretiert
    public void InterpretInput(bool team, bool mouse1, bool mouse2, GameObject selection)
    {
        CheckSelection();
        switch (currentGameState)
        {
            // Default Zustand des Spiels
            case OO_Enum.gameState.UNITY_STATE:
                break;

            // Spieler klickt ein Objekt an und wählt dieses Objekt somit aus
            case OO_Enum.gameState.SELECTION_STATE:

                if (mouse1)
                {
                    if (team == hostTeam)
                    {
                        hostSelection.ChoseUnit();
                    }
                    else
                    {
                        clientSelection.ChoseUnit();
                    } 
                }
                break;

            // Je nach dem ob der Spieler eine Einheit des eigenen oder des feindlichen Team kann er Aktionen ausführen
            case OO_Enum.gameState.ACTION_STATE:

                if (currentSelection == OO_Enum.selection.ENEMY)
                {
                    if (mouse1 || mouse2)
                    {
                        // Einheit wird abgewählt
                        if (team == hostTeam)
                        {
                            hostSelection.DeselectSelectedUnit();
                        }
                        else
                        {
                            clientSelection.DeselectSelectedUnit();
                        }
                    }

                    break;
                }

                else if (currentSelection == OO_Enum.selection.ALLY)
                {
                    if (mouse1)
                    {
                        // Einheit kann sich Bewegen, Angreifen, Fusionieren, Abwählen
                        if (team == hostTeam)
                        {
                            hostSelection.ChoseAction();
                        }
                        else
                        {
                            clientSelection.ChoseAction();
                        }
                    }

                    if (mouse2)
                    {
                        // Einheit wird abgewählt
                        if (team == hostTeam)
                        {
                            hostSelection.DeselectSelectedUnit();
                        }
                        else
                        {
                            clientSelection.DeselectSelectedUnit();
                        }
                    }
                }
                break;
        }
    }
    
    // Die Selections bekommen die Scripte über den Controller zugewiesen, da nicht mehr Teil des Scripts-Objekts
    public void AssignSelection(bool team, GameObject selection)
    {
        if(team == hostTeam)
        {
            hostSelection = selection.GetComponent<OO_Selection>();
            hostSelection.FoundByController = true;
            //hostSelection.Actions = actions;
            hostSelection.Material = material;
            hostSelection.Sound = sound;
            hostSelection.Lists = lists;
        }
        else
        {
            clientSelection = selection.GetComponent<OO_Selection>();
            clientSelection.FoundByController = true;
            //clientSelection.Actions = actions;
            clientSelection.Material = material;
            clientSelection.Sound = sound;
            clientSelection.Lists = lists;
        }
    }

    public void CheckSelection()
    {
        if(hostTeam == TeamTurn)
        {
            ui.Selection = hostSelection;
            ui.Actions = hostSelection.Actions;
            actions = hostSelection.Actions;
            //actions.Selection = hostSelection;
        }
        else
        {
            ui.Selection = clientSelection;
            ui.Actions = clientSelection.Actions;
            actions = clientSelection.Actions;
            //actions.Selection = clientSelection;
        }
    }

    #endregion


    #region Getter Setter

    public bool HostTeam { get => hostTeam; set => hostTeam = value; }
    public bool TeamTurn { get => teamTurn; set => teamTurn = value; }
    public OO_Enum.gameState CurrentGameState { get => currentGameState; set => currentGameState = value; }
    public OO_Enum.action CurrentAction { get => currentAction; set => currentAction = value; }
    public OO_Enum.selection CurrentSelection { get => currentSelection; set => currentSelection = value; }
    public OO_Enum.animations Animations { get => animations; set => animations = value; }
    public OO_Enum.enemyState CurrentEnemyState { get => currentEnemyState; set => currentEnemyState = value; }
    public OO_Enum.animationsEnemy AnimationsEnemy { get => animationsEnemy; set => animationsEnemy = value; }
    public bool ClientTeam { get => clientTeam; set => clientTeam = value; }
    public OO_Actions HostActions { get => hostActions; set => hostActions = value; }
    public OO_Actions ClientActions { get => clientActions; set => clientActions = value; }
    public OO_UI Ui { get => ui; set => ui = value; }
    public OO_Lists Lists { get => lists; set => lists = value; }
    public OO_Sound Sound { get => sound; set => sound = value; }

    #endregion

}
