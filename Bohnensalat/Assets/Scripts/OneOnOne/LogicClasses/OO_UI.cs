﻿using UnityEngine;
using Mirror;
using UnityEngine.UI;
using System.Collections.Generic;
using static OO_Enum.gameState;

public class OO_UI : NetworkBehaviour
{
    // Verwaltung der graphischen Elemente

    #region Header

    [Header("UI References")]
    [SerializeField] private GameObject coinPanel;
    [SerializeField] private GameObject fusionPanel;
    [SerializeField] private GameObject winPanel;
    [SerializeField] private GameObject hudPanel;
    [SerializeField] private GameObject patternPanel;

    [SerializeField] private Button fusionChoice1;
    [SerializeField] private Button fusionChoice2;
    [SerializeField] private Button fusionChoice3;

    [SerializeField] private Button fusionOwnMove;
    [SerializeField] private Button fusionOwnAttack;
    [SerializeField] private Button fusionOtherMove;
    [SerializeField] private Button fusionOtherAttack;

    [SerializeField] private Button skipRoundButton;

    // Text des Rundenanzeigers im Spiel
    [Header("Text References")]
    [SerializeField] private Text roundText;
    [SerializeField] private Text winText;

    private OO_Controller controller;
    private OO_Lists lists;
    private OO_Material material;
    private OO_Sound sound;
    private OO_Animation animation;
    private OO_Selection selection;
    private OO_Actions actions;
    private OO_Board board;

    // Rundenzähler
    private int roundCount = 1;

    // Zwischenspeicher der Pattern für das PatternPanel
    List<int[]> myMovePattern;
    List<int[]> myAttackPattern;
    List<int[]> otherMovePattern;
    List<int[]> otherAttackPattern;

    string myMovePatternName;
    string myAttackPatternName;
    string otherMovePatternName;
    string otherAttackPatternName;

    // Speicher für Buttons, welches Pattern zugewiesen wird und ob Move- oder Attack-Pattern
    private (List<int[]>, string, bool) choice1;
    private (List<int[]>, string, bool) choice2;
    private (List<int[]>, string, bool) choice3;

    public void Awake()
    {
        controller = GetComponent<OO_Controller>();
        lists = GetComponent<OO_Lists>();
        material = GetComponent<OO_Material>();
        sound = GetComponent<OO_Sound>();
        animation = GetComponent<OO_Animation>();
        //actions = GetComponent<OO_Actions>();
        board = GetComponent<OO_Board>();
    }

    #endregion



    #region Methoden

    #region Teamfenster
    //public void ClickedOnTeamBlueButton()
    //{
    //    // Spieler wählt ein Team aus

    //    teamPanel.SetActive(false);
    //    controller.PlayerTeam = true;
    //    enemy.EnemyTeam = false;

    //    // Starte Kameraschwenk auf Team Rot und weist Spieler Team Rot zu
    //    animation.playAnimation(animation.CameraAnimator, "CameraSetTeamBlue");

    //    // Blende Teamauswahl aus und blende nach 1.1 Sekunden Münzwurf ein damit der nächste Zustand erreicht wird
    //    Invoke("ActiveCoinPanel", 1.1f);

    //    // Spiele Klicksound ab
    //    sound.playSound(sound.SoundSource, sound.ButtonSound);
    //}

    #endregion

    #region Münzwurffenster
    public void ClickedOnCoinTossButton()
    {
        // Spieler entscheidet über Münzwurf wer anfängt

        // Blende Münzwurffenster aus
        coinPanel.SetActive(false);
        if (controller.HostTeam)
        {
            animation.playAnimation(GameObject.Find("HostCamera").GetComponent<Animator>(), "CameraSetTeamBlue");
            animation.playAnimation(GameObject.Find("ClientCamera").GetComponent<Animator>(), "CameraSetTeamRed");
        }
        else
        {
            animation.playAnimation(GameObject.Find("HostCamera").GetComponent<Animator>(), "CameraSetTeamRed");
            animation.playAnimation(GameObject.Find("ClientCamera").GetComponent<Animator>(), "CameraSetTeamBlue");
        }
        // Färbe zufällig ein Team passiv, das andere Team fängt das Spiel an
        if (Random.value < 0.5)
        {
            foreach (GameObject g in lists.TeamFalseList)
            {
                material.SetMaterial(g, material.TeamFalsePassivMaterial);
                //g.GetComponentInChildren<OO_Unit>().IsActiv = false;
                CmdSetUnitActiv(g, false);
            }

            controller.TeamTurn = true;

            // Blende HUD ein
            hudPanel.SetActive(true);
        }

        else
        {
            foreach (GameObject g in lists.TeamTrueList)
            {
                material.SetMaterial(g, material.TeamTruePassivMaterial);
                //g.GetComponentInChildren<OO_Unit>().IsActiv = false; 
                CmdSetUnitActiv(g, false);
            }

            controller.TeamTurn = false;
            hudPanel.SetActive(true);
        }

        // Schalte Spieleraktionen frei und somit kommt der Spieler in den nächsten Zustand
        controller.CurrentGameState = OO_Enum.gameState.SELECTION_STATE;


        // Spiele KnopfSound ab
        sound.playSound(sound.SoundSource, sound.ButtonSound);
    }

    #endregion

    #region HUDFenster

    public void ClickedOnSkipButton()
    {
        // Spieler überspringt eine Runde

        // Wechselt welches Team am Zug ist
        if (!controller.TeamTurn)
        {
            // Färbt das Team des Spielers passiv und das andere Team aktiv
            foreach (GameObject g in lists.TeamTrueList)
            {
                material.SetMaterial(g, material.TeamTrueActiveMaterial);
                //g.GetComponentInChildren<OO_Unit>().IsActiv = true;
                CmdSetUnitActiv(g, true);
            }

            foreach (GameObject g in lists.TeamFalseList)
            {
                material.SetMaterial(g, material.TeamFalsePassivMaterial);
                g.GetComponentInChildren<OO_Unit>().IsActiv = false;
                CmdSetUnitActiv(g, false);
            }
        }

        else if (controller.TeamTurn)
        {
            foreach (GameObject g in lists.TeamTrueList)
            {
                material.SetMaterial(g, material.TeamTruePassivMaterial);
                g.GetComponentInChildren<OO_Unit>().IsActiv = false;
                CmdSetUnitActiv(g, false);
            }

            foreach (GameObject g in lists.TeamFalseList)
            {
                material.SetMaterial(g, material.TeamFalseActiveMaterial);
                g.GetComponentInChildren<OO_Unit>().IsActiv = true;
                CmdSetUnitActiv(g, true);
            }
        }

        // Wähle alle abgewählten Einheiten und Felder ab
        selection.DeselectSelectedUnit();

        controller.TeamTurn = !controller.TeamTurn;

        //if (controller.TeamTurn == controller.PlayerTeam)
        //{
        //    controller.CurrentGameState = OO_Enum.gameState.SELECTION_STATE;
        //}

        //else
        //{
        //    controller.CurrentGameState = OO_Enum.gameState.ENEMY_TURN_STATE;
        //}

        // Erhöhe Rundenindex und überschreibe Rundentext
        roundCount++;
        roundText.text = "Round: " + roundCount;

        board.DestroyTile();
        WinCheck();

        sound.playSound(sound.SoundSource, sound.ButtonSound);
    }

    #endregion

    #region Musterfenster

    // PatternPanel
    public void RememberPatternForPatternPanel()
    {
        // Speichern der eigenen Move- und Attack-Pattern
        myMovePattern = selection.SelectedUnit.GetComponent<OO_Unit>().MovePattern;
        myMovePatternName = selection.SelectedUnit.GetComponent<OO_Unit>().MovePatternName;
        myAttackPattern = selection.SelectedUnit.GetComponent<OO_Unit>().AttackPattern;
        myAttackPatternName = selection.SelectedUnit.GetComponent<OO_Unit>().AttackPatternName;

        // Wenn eine Unit getroffen wird
        if (selection.HitInfo.collider is CapsuleCollider)
        {
            // Speichern der Move- und Attack-Pattern der getroffenen Unit
            otherMovePattern = selection.HitInfo.transform.GetComponent<OO_Unit>().MovePattern;
            otherMovePatternName = selection.HitInfo.transform.GetComponent<OO_Unit>().MovePatternName;
            otherAttackPattern = selection.HitInfo.transform.GetComponent<OO_Unit>().AttackPattern;
            otherAttackPatternName = selection.HitInfo.transform.GetComponent<OO_Unit>().AttackPatternName;

            // Aufaddieren der Level der beiden Units
            selection.SelectedUnit.GetComponent<OO_Unit>().CurrentLevel += selection.HitInfo.transform.GetComponent<OO_Unit>().CurrentLevel;

            // Referenzen der Felder und Einheiten werden angepasst
            selection.SelectedUnit.GetComponent<OO_Unit>().CurrentTile = selection.HitInfo.transform.GetComponent<OO_Unit>().CurrentTile;
            selection.SelectedUnit.GetComponent<OO_Unit>().CurrentTile.GetComponent<OO_Tile>().CurrentUnit = selection.SelectedUnit;
            selection.SelectedTile.GetComponent<OO_Tile>().CurrentUnit = null;

            // Getroffene Unit wird aus der Unit-Liste entfernt
            lists.UnitList.Remove(selection.HitInfo.transform.parent.gameObject);

            // Getroffene Unit wird aus ihrer Team-Liste entfernt
            if (selection.HitInfo.transform.GetComponent<OO_Unit>().Team == true)
            {
                lists.TeamTrueList.Remove(selection.HitInfo.transform.parent.gameObject);
            }

            else if (selection.HitInfo.transform.GetComponent<OO_Unit>().Team == false)
            {
                lists.TeamFalseList.Remove(selection.HitInfo.transform.parent.gameObject);
            }

            Destroy(selection.HitInfo.transform.parent.gameObject);
        }

        // Wenn ein Tile getroffen wird
        else if (selection.HitInfo.collider is BoxCollider)
        {
            // Zwischenspeichern der zu zerstörenden Unit
            GameObject unitToDestroy = selection.HitInfo.transform.GetComponent<OO_Tile>().CurrentUnit;

            // Speichern der Move- und Attack-Pattern der getroffenen Unit
            otherMovePattern = unitToDestroy.GetComponent<OO_Unit>().MovePattern;
            otherMovePatternName = unitToDestroy.GetComponent<OO_Unit>().MovePatternName;
            otherAttackPattern = unitToDestroy.GetComponent<OO_Unit>().AttackPattern;
            otherAttackPatternName = unitToDestroy.GetComponent<OO_Unit>().AttackPatternName;

            // Aufaddieren der Level der beiden Units
            selection.SelectedUnit.GetComponent<OO_Unit>().CurrentLevel += unitToDestroy.GetComponent<OO_Unit>().CurrentLevel;

            // Referenzen der Felder und Einheiten werden angepasst
            selection.SelectedUnit.GetComponent<OO_Unit>().CurrentTile = selection.HitInfo.transform.gameObject;
            selection.SelectedUnit.GetComponent<OO_Unit>().CurrentTile.GetComponent<OO_Tile>().CurrentUnit = selection.SelectedUnit;
            selection.SelectedTile.GetComponent<OO_Tile>().CurrentUnit = null;

            // Getroffene Unit wird aus der Unit-Liste entfernt
            lists.UnitList.Remove(unitToDestroy.transform.parent.gameObject);

            // Getroffene Unit wird aus ihrer Team-Liste entfernt
            if (unitToDestroy.GetComponent<OO_Unit>().Team == true)
            {
                lists.TeamTrueList.Remove(unitToDestroy.transform.parent.gameObject);
            }
            else if (unitToDestroy.GetComponent<OO_Unit>().Team == false)
            {
                lists.TeamFalseList.Remove(unitToDestroy.transform.parent.gameObject);
            }

            Destroy(unitToDestroy.transform.parent.gameObject);
        }
    }

    public void SetPatternButtonName()
    {
        fusionOwnMove.GetComponentInChildren<Text>().text = myMovePatternName;
        fusionOwnAttack.GetComponentInChildren<Text>().text = myAttackPatternName;
        fusionOtherMove.GetComponentInChildren<Text>().text = otherMovePatternName;
        fusionOtherAttack.GetComponentInChildren<Text>().text = otherAttackPatternName;
    }

    public void PatternPanelEqualityCheck()
    {
        if (myMovePatternName == otherMovePatternName)
        {
            fusionOwnMove.interactable = false;
            fusionOtherMove.interactable = false;
        }

        if (myAttackPatternName == otherAttackPatternName)
        {
            fusionOwnAttack.interactable = false;
            fusionOtherAttack.interactable = false;
        }
        PatternPanelCheck();
    }

    public void ClickedOnMyMovePatternButton()
    {
        sound.playSound(sound.SoundSource, sound.LevelUpSound);

        selection.SelectedUnit.GetComponent<OO_Unit>().MovePattern = myMovePattern;
        selection.SelectedUnit.GetComponent<OO_Unit>().MovePatternName = myMovePatternName;

        fusionOwnMove.interactable = false;
        fusionOtherMove.interactable = false;

        PatternPanelCheck();
    }

    public void ClickedOnMyAttackPatternButton()
    {
        sound.playSound(sound.SoundSource, sound.LevelUpSound);

        selection.SelectedUnit.GetComponent<OO_Unit>().AttackPattern = myAttackPattern;
        selection.SelectedUnit.GetComponent<OO_Unit>().AttackPatternName = myAttackPatternName;

        fusionOwnAttack.interactable = false;
        fusionOtherAttack.interactable = false;

        PatternPanelCheck();
    }

    public void ClickedOnOtherMovePatternButton()
    {
        sound.playSound(sound.SoundSource, sound.LevelUpSound);

        selection.SelectedUnit.GetComponent<OO_Unit>().MovePattern = otherMovePattern;
        selection.SelectedUnit.GetComponent<OO_Unit>().MovePatternName = otherMovePatternName;

        fusionOtherMove.interactable = false;
        fusionOwnMove.interactable = false;

        PatternPanelCheck();
    }

    public void ClickedOnOtherAttackPatternButton()
    {
        sound.playSound(sound.SoundSource, sound.LevelUpSound);

        selection.SelectedUnit.GetComponent<OO_Unit>().AttackPattern = otherAttackPattern;
        selection.SelectedUnit.GetComponent<OO_Unit>().AttackPatternName = otherAttackPatternName;

        fusionOwnAttack.interactable = false;
        fusionOtherAttack.interactable = false;

        PatternPanelCheck();
    }

    private void PatternPanelCheck()
    {
        // Prüft ob eine Bewegungs und Angriffsmuster ausgewählt wurden und blendet Fusionsfenster ein

        if (fusionOtherAttack.interactable == false && fusionOwnAttack.interactable == false &&
            fusionOtherMove.interactable == false && fusionOwnMove.interactable == false)
        {
            PatternPanel.SetActive(false);
            SetPatternButtonsInteractable();
            fusionChoice3.interactable = true;
            FusionPanel.SetActive(true);
            SetFusionChoice();
        }
    }

    #endregion

    #region Fusionsfenster

    public void SetFusionChoice()
    {
        List<(List<int[]>, string)> tmp;

        // Entscheidung per Zufall, ob Move- oder Attack-Pattern (true = Move, false = AP)
        choice1.Item3 = Random.value > 0.5;
        choice2.Item3 = Random.value > 0.5;

        // Zwischenspeichern einer temporären Liste, die aus der gesamten Pattern-Liste passend zum Level der fusionierten Unit genommen wird
        if (selection.SelectedUnit.GetComponent<OO_Unit>().CurrentLevel == 2)
        {
            tmp = new List<(List<int[]>, string)>(lists.PatternList[1]);
            choice1.Item3 = false;
            choice2.Item3 = false;
        }
        else if (selection.SelectedUnit.GetComponent<OO_Unit>().CurrentLevel <= 5)
        {
            tmp = new List<(List<int[]>, string)>(lists.PatternList[selection.SelectedUnit.GetComponent<OO_Unit>().CurrentLevel - 1]);
        }
        else
        {
            tmp = new List<(List<int[]>, string)>(lists.PatternList[4]);
        }

        // Generierung einer Zufallszahl im Bereich 0 - Länge von tmp
        int random = Random.Range(0, tmp.Count);

        // Tupel für Button 1 bekommt ein Pattern aus tmp zugewiesen
        choice1.Item1 = tmp[random].Item1;
        choice1.Item2 = tmp[random].Item2;

        // Text für Button mit Beschreibung, ob es Move- oder Attack-Pattern ist
        if (choice1.Item3)
        {
            FusionChoice1.GetComponentInChildren<Text>().text = tmp[random].Item2 + " MP";
        }
        else
        {
            FusionChoice1.GetComponentInChildren<Text>().text = tmp[random].Item2 + " AP";
        }
        // Entfernen des Patterns aus der Liste, um Doppelung zu vermeiden
        tmp.RemoveAt(random);

        random = Random.Range(0, tmp.Count);
        choice2.Item1 = tmp[random].Item1;
        choice2.Item2 = tmp[random].Item2;
        if (choice2.Item3)
        {
            FusionChoice2.GetComponentInChildren<Text>().text = tmp[random].Item2 + " MP";
        }
        else
        {
            FusionChoice2.GetComponentInChildren<Text>().text = tmp[random].Item2 + " AP";
        }
        tmp.RemoveAt(random);

        if (tmp.Count != 0)
        {
            random = Random.Range(0, tmp.Count);
            choice3.Item1 = tmp[random].Item1;
            choice3.Item2 = tmp[random].Item2;
            choice3.Item3 = Random.value > 0.5;
            if (choice3.Item3)
            {
                FusionChoice3.GetComponentInChildren<Text>().text = tmp[random].Item2 + " MP";
            }
            else
            {
                FusionChoice3.GetComponentInChildren<Text>().text = tmp[random].Item2 + " AP";
            }
            tmp.RemoveAt(random);
        }
        else
        {
            fusionChoice3.interactable = false;
            fusionChoice3.GetComponentInChildren<Text>().text = "";
        }
    }

    public void ClickedOnChoice1()
    {
        sound.playSound(sound.SoundSource, sound.LevelUpSound);

        // Setzen der Variable, dass Button 1 gewählt wurde
        if (choice1.Item3)
        {
            selection.SelectedUnit.GetComponent<OO_Unit>().MovePattern = choice1.Item1;
            selection.SelectedUnit.GetComponent<OO_Unit>().MovePatternName = choice1.Item2;
        }
        else
        {
            selection.SelectedUnit.GetComponent<OO_Unit>().AttackPattern = choice1.Item1;
            selection.SelectedUnit.GetComponent<OO_Unit>().AttackPatternName = choice1.Item2;
        }
        controller.CurrentAction = OO_Enum.action.NOT_IN_ACTION;
        controller.Animations = OO_Enum.animations.OUT_OF_ANIMATION;
        selection.DeselectSelectedUnit();
        skipRoundButton.interactable = !skipRoundButton.interactable;

        FusionPanel.SetActive(false);
        TeamPassivCheck();

    }

    public void ClickedOnChoice2()
    {
        sound.playSound(sound.SoundSource, sound.LevelUpSound);

        if (choice2.Item3)
        {
            selection.SelectedUnit.GetComponent<OO_Unit>().MovePattern = choice2.Item1;
            selection.SelectedUnit.GetComponent<OO_Unit>().MovePatternName = choice2.Item2;
        }
        else
        {
            selection.SelectedUnit.GetComponent<OO_Unit>().AttackPattern = choice2.Item1;
            selection.SelectedUnit.GetComponent<OO_Unit>().AttackPatternName = choice2.Item2;
        }
        controller.CurrentAction = OO_Enum.action.NOT_IN_ACTION;
        controller.Animations = OO_Enum.animations.OUT_OF_ANIMATION;
        selection.DeselectSelectedUnit();
        skipRoundButton.interactable = !skipRoundButton.interactable;

        FusionPanel.SetActive(false);
        TeamPassivCheck();

    }

    public void ClickedOnChoice3()
    {
        sound.playSound(sound.SoundSource, sound.LevelUpSound);

        if (choice3.Item3)
        {
            selection.SelectedUnit.GetComponent<OO_Unit>().MovePattern = choice3.Item1;
            selection.SelectedUnit.GetComponent<OO_Unit>().MovePatternName = choice3.Item2;
        }
        else
        {
            selection.SelectedUnit.GetComponent<OO_Unit>().AttackPattern = choice3.Item1;
            selection.SelectedUnit.GetComponent<OO_Unit>().AttackPatternName = choice3.Item2;
        }
        controller.CurrentAction = OO_Enum.action.NOT_IN_ACTION;
        controller.Animations = OO_Enum.animations.OUT_OF_ANIMATION;
        selection.DeselectSelectedUnit();
        skipRoundButton.interactable = !skipRoundButton.interactable;

        FusionPanel.SetActive(false);
        TeamPassivCheck();
    }

    #endregion

    #region Gewonnenfenster

    public void ClickedOnRematchButton()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Singleplayer");
    }

    public void ClickedOnMainmenuButton()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Mainmenu");
    }

    #endregion

    #endregion



    #region Hilfsmethoden
    [Command]
    private void CmdSetUnitActiv(GameObject unit, bool state)
    {
        unit.GetComponentInChildren<OO_Unit>().IsActiv = state;
    }

    public void DisableUI()
    {
        // Blendet von jeder Einheit die UI aus
        foreach (GameObject g in lists.UnitList)
        {
            g.GetComponentInChildren<Canvas>().gameObject.SetActive(false);
        }

        // Blendet jedes UI Element im Spiel aus
        coinPanel.SetActive(false);
        fusionPanel.SetActive(false);
        winPanel.SetActive(false);
        hudPanel.SetActive(false);
        patternPanel.SetActive(false);
    }

    private void ActiveCoinPanel()
    {
        coinPanel.SetActive(true);
    }

    public void SetPatternButtonsInteractable()
    {
        // Methode zum interactable machen der Pattern-Buttons

        fusionOwnMove.GetComponent<Button>().interactable = true;
        fusionOtherMove.GetComponent<Button>().interactable = true;
        fusionOwnAttack.GetComponent<Button>().interactable = true;
        fusionOtherAttack.GetComponent<Button>().interactable = true;
    }

    public void SkipButtonsInteractableToggle()
    {
        skipRoundButton.interactable = !skipRoundButton.interactable;
    }

    public void WinCheck()
    {
        // Prüft ob ein Team keine Einheiten hat und gibt zurück welches Team gewonnen hat
        if (lists.TeamFalseList.Count == 0)
        {
            controller.CurrentGameState = OO_Enum.gameState.UNITY_STATE;
            SkipButtonsInteractableToggle();

            winText.text = "Team Blau hat gewonnen!";
            winPanel.SetActive(true);
        }

        else if (lists.TeamTrueList.Count == 0)
        {
            controller.CurrentGameState = OO_Enum.gameState.UNITY_STATE;
            SkipButtonsInteractableToggle();

            winText.text = "Team Rot hat gewonnen!";
            winPanel.SetActive(true);
        }
    }

    public void TeamPassivCheck()
    {
        int indexFalse = 0;
        int indexTrue = 0;

        if (controller.TeamTurn == false)
        {
            foreach (GameObject g in lists.TeamFalseList)
            {
                if (g.GetComponentInChildren<OO_Unit>().IsActiv == false)
                {
                    indexFalse++;
                }
            }
        }

        if (controller.TeamTurn == true)
        {
            foreach (GameObject g in lists.TeamTrueList)
            {
                if (g.GetComponentInChildren<OO_Unit>().IsActiv == false)
                {
                    indexTrue++;
                }
            }
        }

        if (indexFalse == lists.TeamFalseList.Count || indexTrue == lists.TeamTrueList.Count)
        {
            ClickedOnSkipButton();
        }

        skipRoundButton.interactable = true;
    }

    #endregion



    #region Getter Setter

    public GameObject CoinPanel { get => coinPanel; }
    public GameObject FusionPanel { get => fusionPanel; }
    public GameObject WinPanel { get => winPanel; }
    public GameObject HudPanel { get => hudPanel; }
    public GameObject PatternPanel { get => patternPanel; }
    public Button FusionOwnMove { get => fusionOwnMove; set => fusionOwnMove = value; }
    public Button FusionOwnAttack { get => fusionOwnAttack; set => fusionOwnAttack = value; }
    public Button FusionOtherMove { get => fusionOtherMove; set => fusionOtherMove = value; }
    public Button FusionOtherAttack { get => fusionOtherAttack; set => fusionOtherAttack = value; }
    public Button FusionChoice1 { get => fusionChoice1; set => fusionChoice1 = value; }
    public Button FusionChoice2 { get => fusionChoice2; set => fusionChoice2 = value; }
    public Button FusionChoice3 { get => fusionChoice3; set => fusionChoice3 = value; }
    public Button SkipRoundButton { get => skipRoundButton; set => skipRoundButton = value; }
    public OO_Selection Selection { get => selection; set => selection = value; }
    public OO_Actions Actions { get => actions; set => actions = value; }

    #endregion
} 