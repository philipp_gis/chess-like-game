﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class OO_EnemyBot : MonoBehaviour
{
    // Verwaltung des Bots

    #region Header

    private bool enemyTeam;
    private bool isAnimating;
    private bool newRound;

    public List<GameObject> activeUnits;
    public List<string> activActions;
    public List<GameObject> possibleMoveables;
    public List<GameObject> possibleAttackables;
    public List<GameObject> possibleFusionables;
    public GameObject target;
    public Vector3 startPosition;
    public Vector3 endPosition;
    public float startTime;
    public float endTime;

    private OO_Controller controller;
    private OO_Lists lists;
    private OO_Selection selection;
    private OO_Material material;
    private OO_Sound sound;
    private OO_UI ui;

    public void Awake()
    {
        controller = GetComponent<OO_Controller>();
        lists = GetComponent<OO_Lists>();
        selection = GetComponent<OO_Selection>();
        material = GetComponent<OO_Material>();
        sound = GetComponent<OO_Sound>();
        ui = GetComponent<OO_UI>();

        activActions = new List<string> { "Move", "Attack", "Fusion" };
        isAnimating = false;
        newRound = true;
    }

    #endregion



    #region Methoden

    public void ActivUnitCheck()
    {
        // Prüft ob es noch Einheiten gibt die nicht passiv sind

        ui.SkipRoundButton.interactable = false;

        if (newRound)
        {
            // Liste soll neu angelget werden wenn eine neue Runde für den Bot anfängt
            activeUnits = new List<GameObject> ();

            // Je nach dem welches Team der Bot hat, schaut er welche Einheiten noch eine Aktion ausführen können und packt diese in eine Liste
            if (enemyTeam == false)
            {
                foreach (GameObject g in lists.TeamFalseList)
                {
                    if (g.GetComponentInChildren<OO_Unit>().IsActiv)
                    {
                        activeUnits.Add(g);
                    }
                }
            }

            else if (enemyTeam == true)
            {
                foreach (GameObject g in lists.TeamTrueList)
                {
                    if (g.GetComponentInChildren<OO_Unit>().IsActiv)
                    {
                        activeUnits.Add(g);
                    }
                }
            }

            newRound = false;

            SelectRandomUnit(); 
        }
    }
    void SelectRandomUnit()
    {
        // Wähle zufällig Einheit aus Liste aus

        int random = Random.Range(0, activeUnits.Count);

        selection.SelectedUnit = activeUnits[random];
        selection.SelectedTile = activeUnits[random].GetComponentInChildren<OO_Unit>().CurrentTile;

        Debug.Log("Bot hat Einheit " + selection.SelectedUnit.name + " ausgewählt");

        List<int[]> attackPattern;
        List<int[]> movePattern;

        int localX;
        int localZ;

        attackPattern = selection.SelectedUnit.GetComponentInChildren<OO_Unit>().AttackPattern;
        movePattern = selection.SelectedUnit.GetComponentInChildren<OO_Unit>().MovePattern;
        localX = (int)selection.SelectedUnit.transform.position.x;
        localZ = (int)selection.SelectedUnit.transform.position.z;

        foreach (int[] i in attackPattern)
        {
            // Koordinaten des Pattern in Weltkoordinaten umwandeln
            int worldX = i[0] + localX;
            int worldZ = i[1] + localZ;

            // Vergleiche Koordinaten von Muster und Felder, wenn diese übereinstimmen 
            foreach (GameObject g in lists.TileList)
            {
                if (worldX == g.transform.position.x && worldZ == g.transform.position.z)
                {
                    g.GetComponent<OO_Tile>().Type = OO_Enum.tileReachability.ATTACKABLE;
                    break;
                }
            }
        }

        foreach (int[] i in movePattern)
        {
            // Koordinaten des Pattern in Weltkoordinaten umwandeln
            int worldX = i[0] + localX;
            int worldZ = i[1] + localZ;

            // Vergleiche Koordinaten von Muster und Felder, wenn diese übereinstimmen 
            foreach (GameObject g in lists.TileList)
            {
                if (worldX == g.transform.position.x && worldZ == g.transform.position.z)
                {
                    if (g.GetComponent<OO_Tile>().Type == OO_Enum.tileReachability.ATTACKABLE)
                    {
                        g.GetComponent<OO_Tile>().Type = OO_Enum.tileReachability.BOTHABLE;
                    }
                    else
                    {
                        g.GetComponent<OO_Tile>().Type = OO_Enum.tileReachability.MOVEABLE;
                    }
                    break;
                }
            }
        }
        PossibilityCheck();
    }

    void PossibilityCheck()
    {
        // Welche Aktion ist dem Bot möglich

        // Listen sollen neu angelget werden wenn eine neue Einheit vom Bot ausgewählt wurde
        activActions = new List<string> { "Move", "Attack", "Fusion" };
        possibleMoveables = new List<GameObject> ();
        possibleAttackables = new List<GameObject> ();
        possibleFusionables = new List<GameObject> ();

        // Packt jedes Feld das über das Bewegungsmuster erreicht werden kann und keine Einheit besitzt in die Bewegungsliste
        foreach (GameObject g in lists.TileList)
        {
            if (g.GetComponent<OO_Tile>().Type == OO_Enum.tileReachability.MOVEABLE || g.GetComponent<OO_Tile>().Type == OO_Enum.tileReachability.BOTHABLE)
            {
                if (!g.GetComponent<OO_Tile>().CurrentUnit)
                {
                    possibleMoveables.Add(g);
                    //Debug.Log("Bot hat Feld " + g.name + " als begehbar bewertet");
                }
            }
        }

        // Wenn die Bewegungsliste leer ist wird dem Bot die Auswahl der Bewegung genommen
        if (possibleMoveables.Count == 0)
        {
            activActions.Remove("Move");
            //Debug.Log("Bot hat KEIN Feld als begehrbar bewertet");
        }

        // Packt jedes Feld das über das Angriffsmuster erreicht werden kann und eine dem Bot feindliche Einheit besitzt in die Angriffsliste
        foreach (GameObject g in lists.TileList)
        {
            if (g.GetComponent<OO_Tile>().Type == OO_Enum.tileReachability.ATTACKABLE || g.GetComponent<OO_Tile>().Type == OO_Enum.tileReachability.BOTHABLE)
            {
                if (g.GetComponent<OO_Tile>().CurrentUnit && g.GetComponent<OO_Tile>().CurrentUnit.GetComponentInChildren<OO_Unit>().Team != enemyTeam)
                {
                    possibleAttackables.Add(g);
                    //Debug.Log("Bot hat Feld " + g.name + " als angreifbar bewertet");
                }
            }
        }

        // Wenn die Angriffsliste leer ist wird dem Bot die Auswahl der Bewegung genommen
        if (possibleAttackables.Count == 0)
        {
            activActions.Remove("Attack");
            //Debug.Log("Bot hat KEIN Feld als angreifbar bewertet");
        }

        // Packt jedes Feld das über das Bewegungsmuster erreicht werden kann und eine dem Bot freundliche Einheit besitzt in die Fusionsliste
        foreach (GameObject g in lists.TileList)
        {
            if (g.GetComponent<OO_Tile>().Type == OO_Enum.tileReachability.MOVEABLE || g.GetComponent<OO_Tile>().Type == OO_Enum.tileReachability.BOTHABLE)
            {
                if (g.GetComponent<OO_Tile>().CurrentUnit && g.GetComponent<OO_Tile>().CurrentUnit.GetComponentInChildren<OO_Unit>().Team == enemyTeam)
                {
                    possibleFusionables.Add(g);
                    //Debug.Log("Bot hat Feld " + g.name + " als fusionirbar bewertet");
                }
            }
        }

        // Wenn die Angriffsliste leer ist wird dem Bot die Auswahl der Bewegung genommen
        if (possibleFusionables.Count == 0)
        {
            activActions.Remove("Fusion");
            //Debug.Log("Bot hat KEIN Feld als fusionierbar bewertet");
        }

        RandomIntention();
    }


    void RandomIntention()
    {
        // Bot wählt zufällig eine Aktion aus die er mit der ausgewählten Einheit ausführen möchte

        // Wenn die Aktionsliste des Bots leer ist, setzt er die ausgewählte Einheit passiv, vergisst sie
        if (activActions.Count == 0)
        {
            // Einheit wird aus den vom Bot noch auswählbaren Einheiten entfernt
            activeUnits.Remove(selection.SelectedUnit);
            Debug.Log("Bot hat KEINE Aktion für die Einheit " + selection.SelectedUnit.name + " gefunden");

            selection.SelectedUnit.GetComponentInChildren<OO_Unit>().IsActiv = false;
            
            if(enemyTeam == false)
            {
                material.SetMaterial(selection.SelectedUnit, material.TeamFalsePassivMaterial);
            }
            else if (enemyTeam == true)
            {
                material.SetMaterial(selection.SelectedUnit, material.TeamTruePassivMaterial);
            }

            selection.SelectedTile = null;
            selection.SelectedUnit = null;

            SelectRandomUnit();
        }

        // Der Bot wählt zufällig eine Aktion aus den ihm verbleibenden Aktionen aus und setzt sein Ziel zufällig.
        else if (activActions.Count > 0)
        {
            int random = Random.Range(0, activActions.Count);

            if (activActions[random] == "Move")
            {
                controller.CurrentEnemyState = OO_Enum.enemyState.MOVE;

                // Bot wählt zufällig wohin er die Aktion ausführen möchte
                target = possibleMoveables[Random.Range(0, possibleMoveables.Count)];
                Debug.Log("Bot hat Feld " + target.name + " als Bewegungsziel ausgewählt");
            }

            else if (activActions[random] == "Attack")
            {
                
                controller.CurrentEnemyState = OO_Enum.enemyState.ATTACK;

                // Bot wählt zufällig wohin er die Aktion ausführen möchte
                target = possibleAttackables[Random.Range(0, possibleAttackables.Count)];
                Debug.Log("Bot hat Feld " + target.name + " als Angriffsziel ausgewählt");
            }

            else if (activActions[random] == "Fusion")
            {
                controller.CurrentEnemyState = OO_Enum.enemyState.FUSION;

                // Bot wählt zufällig wohin er die Aktion ausführen möchte
                target = possibleFusionables[Random.Range(0, possibleFusionables.Count)];
                Debug.Log("Bot hat Feld " + target.name + " als Fusionsziel ausgewählt");
            }

            LerpAnimation();
        }
    }

    void LerpAnimation()
    {
        // Führt ein Lerp mit Animation zum vom Bot ausgewählten Ziel

        // Ermittelt die Position der ausgewählten Einheit
        float playerHorizontalPosition = selection.SelectedUnit.transform.position.x;
        float playerVerticalPosition = selection.SelectedUnit.transform.position.z;
        Vector3 playerTransform = new Vector3(playerHorizontalPosition, 1, playerVerticalPosition);

        // Ermittelt die angeklickte Zielposition
        float targetHorizontalPosition = target.transform.position.x;
        float targetVerticalPosition = target.transform.position.z;
        Vector3 targetTransform = new Vector3(targetHorizontalPosition, 1, targetVerticalPosition);

        // Lege Start und Endpunkt für LERP fest
        startPosition = playerTransform;
        endPosition = targetTransform;

        // Lege LERP Dauer fest
        startTime = Time.time;
        endTime = 1f;

        selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", true);
        sound.playSound(sound.SoundSource, sound.MoveSound);

        controller.AnimationsEnemy = OO_Enum.animationsEnemy.IN_ANIMATION;
        //isAnimating = true;
    }

    private void FixedUpdate()
    {
        //if (controller.AnimationsEnemy == OO_Enum.animationsEnemy.IN_ANIMATION && controller.CurrentGameState == OO_Enum.gameState.ENEMY_TURN_STATE)
        //{
        //    float actualTime;
        //    float progress;

        //    // Bestimmt den Moment der Zeitmessung und wandelt es in den prozentualen Anteil des gesamten LERP
        //    actualTime = Time.time - startTime;
        //    progress = actualTime / endTime;

        //    // Positioniere Einheit in die Position des Lerp anhand des Fortschritts der Gesamtstrecke
        //    selection.SelectedUnit.transform.position = Vector3.Lerp(startPosition, endPosition, progress);

        //    // Wenn 100%  des LERP erreicht wurde

        //    if (progress >= 1.0f)
        //    {
        //        Debug.Log(progress);
        //        Debug.Log(controller.CurrentEnemyState);

        //        if (controller.CurrentEnemyState == OO_Enum.enemyState.MOVE)
        //        {
        //            controller.CurrentEnemyState = OO_Enum.enemyState.POST_MOVE_WORK;
        //            Debug.Log("In POST_MOVE_WORK");
        //        }

        //        else if (controller.CurrentEnemyState == OO_Enum.enemyState.ATTACK)
        //        {
        //            controller.CurrentEnemyState = OO_Enum.enemyState.POST_ATTACK_WORK;
        //            Debug.Log("In POST_ATTACK_WORK");
        //        }

        //        else if (controller.CurrentEnemyState == OO_Enum.enemyState.FUSION)
        //        {
        //            controller.CurrentEnemyState = OO_Enum.enemyState.POST_FUSION_WORK;
        //            Debug.Log("In POST_FUSION_WORK");
        //        }

        //        controller.AnimationsEnemy = OO_Enum.animationsEnemy.OUT_OF_ANIMATION;
        //        //isAnimating = false;

        //        PostActionWork();
        //    }
        //}
    }

    void PostActionWork()
    {
        if (controller.CurrentEnemyState == OO_Enum.enemyState.POST_MOVE_WORK)
        {
            // Beende Animationsloop
            selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", false);

            // Altes Feld verliert seine Referenz auf die Einheit
            selection.SelectedTile.GetComponent<OO_Tile>().CurrentUnit = null;

            // Einheit und Zielfeld setzen sich gegenseitig als Referenz
            selection.SelectedUnit.GetComponentInChildren<OO_Unit>().CurrentTile = target.transform.gameObject;
            selection.SelectedUnit.GetComponentInChildren<OO_Unit>().CurrentTile.GetComponent<OO_Tile>().CurrentUnit = selection.SelectedUnit.transform.GetChild(0).transform.gameObject;

            // Einheit abwählen und passiv setzen
            selection.SelectedUnit.GetComponentInChildren<OO_Unit>().IsActiv = false;
            selection.SelectedTile = null;
            selection.SelectedUnit = null;

            foreach (GameObject g in lists.TileList)
            {
                if (g.GetComponent<OO_Tile>().Type != OO_Enum.tileReachability.DEFAULT)
                {
                    g.GetComponent<OO_Tile>().Type = OO_Enum.tileReachability.DEFAULT;
                }
            }

            ui.TeamPassivCheck();

            controller.CurrentEnemyState = OO_Enum.enemyState.NOT_IN_ACTION;

            newRound = true;
        }

        else if (controller.CurrentEnemyState == OO_Enum.enemyState.POST_ATTACK_WORK)
        {
            // Beende Animationsloop
            selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", false);

            // Zwischenspeichern der zu zerstörenden Unit
            GameObject unitToDestroy = target.transform.GetComponent<OO_Tile>().CurrentUnit;

            // Alte Einheit verliert seine Referenz
            selection.SelectedTile.GetComponent<OO_Tile>().CurrentUnit = null;

            // Einheit und Zielfeld setzen sich gegenseitig als Referenz
            selection.SelectedUnit.GetComponentInChildren<OO_Unit>().CurrentTile = target.transform.gameObject;
            selection.SelectedUnit.GetComponentInChildren<OO_Unit>().CurrentTile.GetComponent<OO_Tile>().CurrentUnit = selection.SelectedUnit.transform.GetChild(0).transform.gameObject; ;

            // Angegriffene Unit wird aus der Unit-Liste entfernt
            lists.UnitList.Remove(unitToDestroy.transform.parent.gameObject);

            // Angegriffene Unit wird aus seiner Team-Liste entfernt
            if (unitToDestroy.GetComponentInChildren<OO_Unit>().Team == true)
            {
                lists.TeamTrueList.Remove(unitToDestroy.transform.parent.gameObject);
            }

            else if (unitToDestroy.GetComponentInChildren<OO_Unit>().Team == false)
            {
                lists.TeamFalseList.Remove(unitToDestroy.transform.parent.gameObject);
            }

            Destroy(unitToDestroy.transform.parent.gameObject);

            // Einheit abwählen und passiv setzen
            selection.SelectedUnit.GetComponentInChildren<OO_Unit>().IsActiv = false;
            selection.SelectedTile = null;
            selection.SelectedUnit = null;

            foreach (GameObject g in lists.TileList)
            {
                if (g.GetComponent<OO_Tile>().Type != OO_Enum.tileReachability.DEFAULT)
                {
                    g.GetComponent<OO_Tile>().Type = OO_Enum.tileReachability.DEFAULT;
                }
            }

            // Abfrage ob ein Team gewonnen hat
            ui.WinCheck();
            ui.TeamPassivCheck();

            controller.CurrentEnemyState = OO_Enum.enemyState.NOT_IN_ACTION;

            newRound = true;
        }

        else if (controller.CurrentEnemyState == OO_Enum.enemyState.POST_FUSION_WORK)
        {
            selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", false);

            // Speichern der eigenen Move- und Attack-Pattern
            List<int[]> myMovePattern = selection.SelectedUnit.GetComponentInChildren<OO_Unit>().MovePattern;
            string myMovePatternName = selection.SelectedUnit.GetComponentInChildren<OO_Unit>().MovePatternName;
            List<int[]> myAttackPattern = selection.SelectedUnit.GetComponentInChildren<OO_Unit>().AttackPattern;
            string myAttackPatternName = selection.SelectedUnit.GetComponentInChildren<OO_Unit>().AttackPatternName;

            // Zwischenspeichern der zu zerstörenden Unit
            GameObject unitToDestroy = target.transform.GetComponent<OO_Tile>().CurrentUnit;

            // Speichern der Move- und Attack-Pattern der getroffenen Unit
            List<int[]> otherMovePattern = unitToDestroy.GetComponentInChildren<OO_Unit>().MovePattern;
            string otherMovePatternName = unitToDestroy.GetComponentInChildren<OO_Unit>().MovePatternName;
            List<int[]> otherAttackPattern = unitToDestroy.GetComponentInChildren<OO_Unit>().AttackPattern;
            string otherAttackPatternName = unitToDestroy.GetComponentInChildren<OO_Unit>().AttackPatternName;

            // Aufaddieren der Level der beiden Units
            selection.SelectedUnit.GetComponentInChildren<OO_Unit>().CurrentLevel += unitToDestroy.GetComponentInChildren<OO_Unit>().CurrentLevel;

            // Referenzen der Felder und Einheiten werden angepasst
            selection.SelectedUnit.GetComponentInChildren<OO_Unit>().CurrentTile = target.transform.gameObject;
            selection.SelectedUnit.GetComponentInChildren<OO_Unit>().CurrentTile.GetComponent<OO_Tile>().CurrentUnit = selection.SelectedUnit.transform.GetChild(0).transform.gameObject; ;
            selection.SelectedTile.GetComponent<OO_Tile>().CurrentUnit = null;

            // Getroffene Unit wird aus der Unit-Liste entfernt
            lists.UnitList.Remove(unitToDestroy.transform.parent.gameObject);
            activeUnits.Remove(unitToDestroy.transform.parent.gameObject);

            // Getroffene Unit wird aus ihrer Team-Liste entfernt
            if (unitToDestroy.GetComponentInChildren<OO_Unit>().Team == true)
            {
                lists.TeamTrueList.Remove(unitToDestroy.transform.parent.gameObject);
                Debug.Log(unitToDestroy.name + " aus Liste gelöscht");
            }
            else if (unitToDestroy.GetComponentInChildren<OO_Unit>().Team == false)
            {
                lists.TeamFalseList.Remove(unitToDestroy.transform.parent.gameObject);
            }
            
            Debug.Log(unitToDestroy.name + " wird zerstört");
            Destroy(unitToDestroy.transform.parent.gameObject);
            Debug.Log(unitToDestroy.name + " wurde zerstört");

            // Bot wählt zufällig zwischen den Mustern aus 
            bool rnd = Random.value < 0.5f;

            if (rnd)
            {
                selection.SelectedUnit.GetComponentInChildren<OO_Unit>().MovePattern = myMovePattern;
                selection.SelectedUnit.GetComponentInChildren<OO_Unit>().MovePatternName = myMovePatternName;
            }

            else if (!rnd)
            {
                selection.SelectedUnit.GetComponentInChildren<OO_Unit>().MovePattern = otherMovePattern;
                selection.SelectedUnit.GetComponentInChildren<OO_Unit>().MovePatternName = otherMovePatternName;
            }

            rnd = Random.value < 0.5f;

            if (rnd)
            {
                selection.SelectedUnit.GetComponentInChildren<OO_Unit>().AttackPattern = myAttackPattern;
                selection.SelectedUnit.GetComponentInChildren<OO_Unit>().AttackPatternName = myAttackPatternName;
            }

            else if (!rnd)
            {
                selection.SelectedUnit.GetComponentInChildren<OO_Unit>().AttackPattern = otherAttackPattern;
                selection.SelectedUnit.GetComponentInChildren<OO_Unit>().AttackPatternName = otherAttackPatternName;
            }

            // Zwischenspeichern einer temporären Liste, die aus der gesamten Pattern-Liste passend zum Level der fusionierten Unit genommen wird
            List<(List<int[]>, string)> tmp;

            if (selection.SelectedUnit.GetComponentInChildren<OO_Unit>().CurrentLevel <= 5)
            {
                tmp = new List<(List<int[]>, string)>(lists.PatternList[selection.SelectedUnit.GetComponentInChildren<OO_Unit>().CurrentLevel - 1]);
            }
            else
            {
                tmp = new List<(List<int[]>, string)>(lists.PatternList[4]);
            }

            // Generierung einer Zufallszahl im Bereich 0 - Länge von tmp
            int random = Random.Range(0, tmp.Count);

            // Bot wählt zufällig zwischen Fusionsmuster aus
            if (rnd)
            {
                selection.SelectedUnit.GetComponentInChildren<OO_Unit>().MovePattern = tmp[random].Item1;
                selection.SelectedUnit.GetComponentInChildren<OO_Unit>().MovePatternName = tmp[random].Item2;
            }

            else if (!rnd)
            {
                selection.SelectedUnit.GetComponentInChildren<OO_Unit>().AttackPattern = tmp[random].Item1;
                selection.SelectedUnit.GetComponentInChildren<OO_Unit>().AttackPatternName = tmp[random].Item2;
            }

            selection.SelectedUnit.GetComponentInChildren<OO_Unit>().IsActiv = false;
            selection.SelectedTile = null;
            selection.SelectedUnit = null;

            foreach (GameObject g in lists.TileList)
            {
                if (g.GetComponent<OO_Tile>().Type != OO_Enum.tileReachability.DEFAULT)
                {
                    g.GetComponent<OO_Tile>().Type = OO_Enum.tileReachability.DEFAULT;
                }
            }
            
            ui.TeamPassivCheck();

            controller.CurrentEnemyState = OO_Enum.enemyState.NOT_IN_ACTION;

            newRound = true;
        }
    }

    #endregion


    #region Getter Setter

    public bool EnemyTeam { get => enemyTeam; set => enemyTeam = value; }

    #endregion
}