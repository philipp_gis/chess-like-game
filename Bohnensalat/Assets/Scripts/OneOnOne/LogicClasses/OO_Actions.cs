﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using static OO_Enum.unitTypes;
using static OO_Enum.gameState;
using UnityEngine.UI;

public class OO_Actions : NetworkBehaviour
{
    #region Header

    // Selection wird vom Controller zugewiesen -> keine Zuweisung mehr in Awake()
    [SerializeField] private OO_Selection selection;
    private OO_Sound sound;
    private OO_Lists lists;
    private OO_UI ui;
    private OO_Controller controller;

    // Positionen von der aus das Objekt zum Ziel bewegt werden soll
    [SyncVar] private Vector3 startPosition;
    [SyncVar] private Vector3 endPosition;

    // Zeitpunkt von dem an die Zeit gemessen wird und wann aufgehört werden soll
    [SyncVar] private float startTime;
    [SyncVar] private float endTime;


    private void Awake()
    {
        //sound = GetComponent<OO_Sound>();
        //lists = GetComponent<OO_Lists>();
        //ui = GetComponent<OO_UI>();
        //controller = GetComponent<OO_Controller>();
    }

    private void Start()
    {
        controller = selection.Controller;
        sound = controller.Sound;
        lists = controller.Lists;
        ui = controller.Ui;
    }

    #endregion



    #region Methoden

    #region Move
    [Command]
    public void CmdMove()
    {
        // Einheit wird auf freies angeklickte Feld bewegt
        if (selection.HitInfoTransform.GetComponent<OO_Tile>().Type == OO_Enum.tileReachability.MOVEABLE || selection.HitInfoTransform.GetComponent<OO_Tile>().Type == OO_Enum.tileReachability.BOTHABLE)
        {
            // Ermittelt die Position der ausgewählten Einheit
            float playerHorizontalPosition = selection.SelectedUnit.transform.parent.position.x;
            float playerVerticalPosition = selection.SelectedUnit.transform.parent.position.z;
            Vector3 playerTransform = new Vector3(playerHorizontalPosition, 1, playerVerticalPosition);

            // Ermittelt die angeklickte Zielposition
            float targetHorizontalPosition = selection.HitInfoTransform.position.x;
            float targetVerticalPosition = selection.HitInfoTransform.position.z;
            Vector3 targetTransform = new Vector3(targetHorizontalPosition, 1, targetVerticalPosition);

            // Lege Start und Endpunkt für LERP fest
            startPosition = playerTransform;
            endPosition = targetTransform;

            // Lege LERP Dauer fest
            startTime = Time.time;
            endTime = 1f;

            //selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", true);
            sound.playSound(sound.SoundSource, sound.MoveSound);

            // Während der Animation darf nicht geskipt werden
            ui.SkipButtonsInteractableToggle();

            controller.CurrentGameState = OO_Enum.gameState.UNITY_STATE;
            controller.CurrentAction = OO_Enum.action.MOVE;
            controller.Animations = OO_Enum.animations.IN_ANIMATION;
        }
    }

    [Command]
    public void CmdPostMoveWork()
    {
        Debug.Log("PostMoveWork entered");
        // Beende Animationsloop
        selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", false);

        // Altes Feld verliert seine Referenz auf die Einheit
        selection.SelectedTile.GetComponent<OO_Tile>().CurrentUnit = null;

        // Einheit und Zielfeld setzen sich gegenseitig als Referenz
        selection.SelectedUnit.GetComponent<OO_Unit>().CurrentTile = selection.HitInfoTransform.gameObject;
        selection.SelectedUnit.GetComponent<OO_Unit>().CurrentTile.GetComponent<OO_Tile>().CurrentUnit = selection.SelectedUnit;

        // Einheit abwählen und passiv setzen
        selection.SelectedUnit.GetComponent<OO_Unit>().IsActiv = false;
        selection.DeselectSelectedUnit();

        ui.SkipButtonsInteractableToggle();

        // Zustände zurücksetzen
        controller.CurrentAction = OO_Enum.action.NOT_IN_ACTION;
        controller.CurrentSelection = OO_Enum.selection.NOTHING_SELECTED;
        controller.CurrentGameState = OO_Enum.gameState.SELECTION_STATE;
        controller.Animations = OO_Enum.animations.OUT_OF_ANIMATION;
        //selection = null;

        ui.TeamPassivCheck();
    }

    #endregion

    #region Attack

    // Töten einer feindlichen Einheit
    public void Attack()
    {
        if (selection.HitInfo.collider is CapsuleCollider &&
           (selection.HitInfo.transform.GetComponentInChildren<OO_Unit>().CurrentTile.GetComponent<OO_Tile>().Type ==
            OO_Enum.tileReachability.ATTACKABLE ||
            selection.HitInfo.transform.GetComponentInChildren<OO_Unit>().CurrentTile.GetComponent<OO_Tile>().Type ==
            OO_Enum.tileReachability.BOTHABLE) ||
            selection.HitInfo.collider is BoxCollider &&
           (selection.HitInfo.transform.GetComponent<OO_Tile>().Type == OO_Enum.tileReachability.ATTACKABLE ||
            selection.HitInfo.transform.GetComponent<OO_Tile>().Type == OO_Enum.tileReachability.BOTHABLE))
        {
            // Ermittelt die Position der ausgewählten Einheit
            float playerHorizontalPosition = selection.SelectedUnit.transform.parent.position.x;
            float playerVerticalPosition = selection.SelectedUnit.transform.parent.position.z;
            Vector3 playerTransform = new Vector3(playerHorizontalPosition, 1, playerVerticalPosition);

            // Ermittelt die angeklickte Zielposition
            float targetHorizontalPosition = selection.HitInfo.transform.position.x;
            float targetVerticalPosition = selection.HitInfo.transform.position.z;
            Vector3 targetTransform = new Vector3(targetHorizontalPosition, 1, targetVerticalPosition);

            // Lege Start und Endpunkt für LERP fest
            startPosition = playerTransform;
            endPosition = targetTransform;

            // Lege LERP Dauer fest
            startTime = Time.time;
            endTime = 1f;

            selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", true);

            // Während der Animation darf nicht geskipt werden
            ui.SkipButtonsInteractableToggle();

            controller.CurrentGameState = OO_Enum.gameState.UNITY_STATE;
            controller.CurrentAction = OO_Enum.action.ATTACK;
            controller.Animations = OO_Enum.animations.IN_ANIMATION;
        }
    }

    public void PostAttackWork()
    {
        // Beende Animationsloop
        selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", false);

        // Wenn eine Unit getroffen wird
        if (selection.HitInfo.collider is CapsuleCollider)
        {
            // Einheit und Zielfeld setzen sich gegenseitig als Referenz
            selection.SelectedUnit.GetComponent<OO_Unit>().CurrentTile = selection.HitInfo.transform.GetComponent<OO_Unit>().CurrentTile;
            selection.SelectedUnit.GetComponent<OO_Unit>().CurrentTile.GetComponent<OO_Tile>().CurrentUnit = selection.SelectedUnit;

            // Alte Einheit verliert seine Referenz
            selection.SelectedTile.GetComponent<OO_Tile>().CurrentUnit = null;

            // Angegriffene Unit wird aus der Unit-Liste entfernt
            lists.UnitList.Remove(selection.HitInfo.transform.parent.gameObject);

            // Angegriffene Unit wird aus seiner Team-Liste entfernt
            if (selection.HitInfo.transform.GetComponent<OO_Unit>().Team == true)
            {
                lists.TeamTrueList.Remove(selection.HitInfo.transform.parent.gameObject);
            }

            else if (selection.HitInfo.transform.GetComponent<OO_Unit>().Team == false)
            {
                lists.TeamFalseList.Remove(selection.HitInfo.transform.parent.gameObject);
            }

            Destroy(selection.HitInfo.transform.parent.gameObject);
        }

        // Wenn ein Tile getroffen wird
        else if (selection.HitInfo.collider is BoxCollider)
        {
            // Zwischenspeichern der zu zerstörenden Unit
            GameObject unitToDestroy = selection.HitInfo.transform.GetComponent<OO_Tile>().CurrentUnit;

            // Alte Einheit verliert seine Referenz
            selection.SelectedTile.GetComponent<OO_Tile>().CurrentUnit = null;

            // Einheit und Zielfeld setzen sich gegenseitig als Referenz
            selection.SelectedUnit.GetComponent<OO_Unit>().CurrentTile = selection.HitInfo.transform.gameObject;
            selection.SelectedUnit.GetComponent<OO_Unit>().CurrentTile.GetComponent<OO_Tile>().CurrentUnit = selection.SelectedUnit;

            // Angegriffene Unit wird aus der Unit-Liste entfernt
            lists.UnitList.Remove(unitToDestroy.transform.parent.gameObject);

            // Angegriffene Unit wird aus seiner Team-Liste entfernt
            if (unitToDestroy.GetComponent<OO_Unit>().Team == true)
            {
                lists.TeamTrueList.Remove(unitToDestroy.transform.parent.gameObject);
            }

            else if (unitToDestroy.GetComponent<OO_Unit>().Team == false)
            {
                lists.TeamFalseList.Remove(unitToDestroy.transform.parent.gameObject);
            }

            Destroy(unitToDestroy.transform.parent.gameObject);
        }

        // Einheit abwählen und passiv setzen
        selection.SelectedUnit.GetComponent<OO_Unit>().IsActiv = false;
        selection.DeselectSelectedUnit();

        ui.SkipButtonsInteractableToggle();

        // Zustände zurücksetzen
        controller.Animations = OO_Enum.animations.OUT_OF_ANIMATION;
        controller.CurrentAction = OO_Enum.action.NOT_IN_ACTION;
        controller.CurrentSelection = OO_Enum.selection.NOTHING_SELECTED;
        controller.CurrentGameState = OO_Enum.gameState.SELECTION_STATE;
        
        // Abfrage ob ein Team gewonnen hat
        ui.WinCheck();
        ui.TeamPassivCheck();
    }

    #endregion

    #region Fusion

    // Vereinigung zweier verbündeter Einheiten
    public void Fusion()
    {
        if ((selection.HitInfo.collider is CapsuleCollider
            && (selection.HitInfo.transform.GetComponentInChildren<OO_Unit>().CurrentTile.GetComponent<OO_Tile>().Type == OO_Enum.tileReachability.MOVEABLE ||
            selection.HitInfo.transform.GetComponentInChildren<OO_Unit>().CurrentTile.GetComponent<OO_Tile>().Type == OO_Enum.tileReachability.BOTHABLE)) 
            || 
            selection.HitInfo.collider is BoxCollider &&
            (selection.HitInfo.transform.GetComponent<OO_Tile>().Type == OO_Enum.tileReachability.MOVEABLE ||
            selection.HitInfo.transform.GetComponent<OO_Tile>().Type == OO_Enum.tileReachability.BOTHABLE))
        {
            // Ermittelt die Position der ausgewählten Einheit
            float playerHorizontalPosition = selection.SelectedUnit.transform.parent.position.x;
            float playerVerticalPosition = selection.SelectedUnit.transform.parent.position.z;
            Vector3 playerTransform = new Vector3(playerHorizontalPosition, 1, playerVerticalPosition);

            // Ermittelt die angeklickte Zielposition
            float targetHorizontalPosition = selection.HitInfo.transform.position.x;
            float targetVerticalPosition = selection.HitInfo.transform.position.z;
            Vector3 targetTransform = new Vector3(targetHorizontalPosition, 1, targetVerticalPosition);

            // Lege Start und Endpunkt für LERP fest
            startPosition = playerTransform;
            endPosition = targetTransform;

            // Lege LERP Dauer fest
            startTime = Time.time;
            endTime = 1f;

            selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", true);

            // Während der Animation darf nicht geskipt werden
            ui.SkipButtonsInteractableToggle();

            controller.CurrentGameState = OO_Enum.gameState.UNITY_STATE;
            controller.CurrentAction = OO_Enum.action.FUSION;
            controller.Animations = OO_Enum.animations.IN_ANIMATION;
        }
    }

    public void PostFusionWork()
    {
        // Beende Animationsloop
        selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", false);
        selection.SelectedUnit.GetComponent<OO_Unit>().IsActiv = false;

        controller.CurrentGameState = OO_Enum.gameState.UNITY_STATE;

        ui.RememberPatternForPatternPanel();
        ui.SetPatternButtonName();
        
        ui.PatternPanel.SetActive(true);
        
        ui.PatternPanelEqualityCheck();


        // Zuständ zurücksetzen
        controller.Animations = OO_Enum.animations.OUT_OF_ANIMATION;
        controller.CurrentAction = OO_Enum.action.NOT_IN_ACTION;
    }

    #endregion

    #region Lerp
    private void FixedUpdate()
    {
        if (controller.Animations == OO_Enum.animations.IN_ANIMATION && controller.TeamTurn == selection.MyTeam)
        {
            float actualTime;
            float progress;

            // Bestimmt den Moment der Zeitmessung und wandelt es in den prozentualen Anteil des gesamten LERP
            actualTime = Time.time - startTime;
            progress = actualTime / endTime;

            // Funktioniert noch nicht
            //CmdLerp(startPosition, endPosition, progress);

            // Positioniere Einheit in die Position des Lerp anhand des Fortschritts der Gesamtstrecke
            Debug.Log(selection);
            Debug.Log(selection.SelectedUnit);
            Debug.Log(selection.SelectedUnit.transform);
            Debug.Log(selection.SelectedUnit.transform.parent);
            Debug.Log(selection.SelectedUnit.transform.parent.position);
            Debug.Log(startPosition);
            Debug.Log(endPosition);
            selection.SelectedUnit.transform.parent.position = Vector3.Lerp(startPosition, endPosition, progress);



            // Wenn 100%  des LERP erreicht wurde
            if (progress >= 1.0f)
            {
                if (controller.CurrentAction == OO_Enum.action.MOVE)
                {
                    controller.CurrentAction = OO_Enum.action.POST_MOVE_WORK;
                }

                else if (controller.CurrentAction == OO_Enum.action.ATTACK)
                {
                    sound.playSound(sound.SoundSource, sound.AttackSound);
                    controller.CurrentAction = OO_Enum.action.POST_ATTACK_WORK;
                }

                else if (controller.CurrentAction == OO_Enum.action.FUSION)
                {
                    sound.playSound(sound.SoundSource, sound.FusionSound);
                    controller.CurrentAction = OO_Enum.action.POST_FUSION_WORK;
                }
            }
        }
    }

    #endregion

    #region Hilfsmethoden

    [Command]
    public void CmdLerp(Vector3 startPosition, Vector3 endPosition, float progress)
    {
        RpcLerp(startPosition, endPosition, progress);
    }

    [ClientRpc]
    public void RpcLerp(Vector3 startPosition, Vector3 endPosition, float progress)
    {
        selection.SelectedUnit.transform.parent.position = Vector3.Lerp(startPosition, endPosition, progress);
    }


    #endregion


    #region Getter Setter

    public OO_Selection Selection { get => selection; set => selection = value; }

    #endregion


    #endregion
}