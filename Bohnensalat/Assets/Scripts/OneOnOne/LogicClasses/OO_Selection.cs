﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.EventSystems;
using static OO_Enum.unitTypes;
using Mirror.Websocket;
using System;

public class OO_Selection : NetworkBehaviour
{
    // Verwaltung der Auswahl von Objekten

    #region Header

    [SerializeField] private OO_Controller controller;
    [SerializeField] private OO_Actions actions;
    [SerializeField] private OO_Material material;
    [SerializeField] private OO_Sound sound;
    [SerializeField] private OO_Lists lists;

    // Merkt sich welche Einheit, Feld ausgewählt ist
    [Header("READ ONLY")]
    [SyncVar] [SerializeField] private GameObject selectedUnit;
    [SyncVar] [SerializeField] private GameObject selectedTile;
    [SyncVar] [SerializeField] private string selectedUnitString;
    [SyncVar] [SerializeField] private string selectedTileString;
    [SyncVar] [SerializeField] private Transform hitInfoTransform;
    [SerializeField] private bool myTeam;
    [SerializeField] private bool foundByController = false;
    [SerializeField] private bool stringProcessing = false;
    [SerializeField] private bool selectionDone = false;

    private bool input1;
    private bool input2;

    public RaycastHit hitInfo;
    [SerializeField] private bool hit;

    // Wenn der lokale Spieler eine Eingabe tätigt,
    // wird der Controller beauftragt diese zu interpretieren
    private void Update()
    {
        if(isLocalPlayer)
        { 
            input1 = Input.GetMouseButtonDown(0);
            input2 = Input.GetMouseButtonDown(1);

            // Debug.Log(hitInfo.transform.gameObject.name);
            if (input1 || input2)
            {                
                controller.InterpretInput(myTeam, input1, input2, gameObject);
            }
        }
    }

    private void Start()
    {
        actions = GetComponent<OO_Actions>();
        controller = GameObject.Find("Scripts").GetComponent<OO_Controller>();
        // Einschalten der spieler-eigenen Camera und AudioListeners
        if (isLocalPlayer)
        {
            GetComponent<Camera>().enabled = true;
            GetComponent<AudioListener>().enabled = true;
            GetComponentInChildren<SpriteRenderer>().enabled = true;
        }
        // Gewährleistung, dass die nicht spieler-eigenen Selection das andere Team ist
        else
        {
            myTeam = !myTeam;
        }

        // Benenne die Spieler-Objekte korrekt
        if(myTeam == controller.HostTeam)
        {
            this.transform.name = "HostCamera";
        }
        if(myTeam == controller.ClientTeam)
        {
            this.transform.name = "ClientCamera";
        }

        // Weise alle Scripte zu und setze die Selections im Controller entsprechend
        if (!foundByController)
        {
            controller.AssignSelection(myTeam, gameObject);
        }
    }

    #endregion



    #region Verwaltungsmethoden

    public void ChoseUnit()
    {
        // Objekte die auswählbar sind
        Debug.Log("ChoseUnit");
        sound.playSound(sound.SoundSource, sound.ClickSound);

        // Raycast um festzustellen was der Spieler anklickt
        hitInfo = new RaycastHit();
        hit = Physics.Raycast(GetComponent<Camera>().ScreenPointToRay(Input.mousePosition), out hitInfo);
        //CmdSetHitInfo(hitInfo.transform);


        // kein Treffer
        if (!hit) { }

        else if (hitInfo.collider is BoxCollider)
        {
            Debug.Log(hitInfo.transform.GetComponent<NetworkIdentity>());
            
            // Freies Feld
            if (!hitInfo.transform.GetComponent<OO_Tile>().CurrentUnit) { }

            // Feld von Verbündeten
            else if (hitInfo.transform.GetComponent<OO_Tile>().CurrentUnit.GetComponentInChildren<OO_Unit>().Team == MyTeam)
            {
                SelectUnit();
            }

            // Feld von Gegner
            else if (hitInfo.transform.GetComponent<OO_Tile>().CurrentUnit.GetComponentInChildren<OO_Unit>().Team != MyTeam)
            {
                SelectUnit();
            }
        }

        else if (hitInfo.collider is CapsuleCollider)
        {
            Debug.Log(hitInfo.transform.parent.transform.GetComponent<NetworkIdentity>());
            // verbündete Einheit
            if (hitInfo.transform.GetComponent<OO_Unit>().Team == MyTeam)
            {
                SelectUnit();
            }

            // feindliche Einheiten
            else if (hitInfo.transform.GetComponent<OO_Unit>().Team != MyTeam)
            {
                SelectUnit();
            }
        }
    }

    public void ChoseAction()
    {
        // Aktionen die vom Spieler ausgeführt werden können je nachdem was angeklickt wird

        hitInfo = new RaycastHit();
        hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
        CmdSetHitInfo(hitInfo.transform);
        // Kein Treffer
        if (!hit) { }

        else if (hitInfo.collider is BoxCollider)
        {
            // Freies Feld
            if (!hitInfo.transform.GetComponent<OO_Tile>().CurrentUnit)
            {
                if (selectedUnit.GetComponentInChildren<OO_Unit>().IsActiv == true)
                {
                    actions.CmdMove();
                }
            }

            // eigenes Feld
            else if (hitInfo.transform.GetComponent<OO_Tile>().CurrentUnit == selectedUnit) { }

            // Feld von Verbündeten
            else if (hitInfo.transform.GetComponent<OO_Tile>().CurrentUnit.GetComponentInChildren<OO_Unit>().Team == MyTeam)
            {
                if (selectedUnit.GetComponentInChildren<OO_Unit>().IsActiv == true)
                {
                    actions.Fusion();
                }
            }

            // Feld von Gegner
            else if (hitInfo.transform.GetComponent<OO_Tile>().CurrentUnit.GetComponentInChildren<OO_Unit>().Team != MyTeam)
            {
                if (selectedUnit.GetComponentInChildren<OO_Unit>().IsActiv == true)
                {
                    actions.Attack();
                }
            }
        }

        else if (hitInfo.collider is CapsuleCollider)
        {
            // eigene Einheit
            if (hitInfo.transform == selectedUnit.GetComponentInChildren<OO_Unit>().transform) { }

            // verbündete Einheit
            else if (hitInfo.transform.GetComponent<OO_Unit>().Team == MyTeam)
            {
                if (selectedUnit.GetComponentInChildren<OO_Unit>().IsActiv == true)
                {
                    actions.Fusion();
                }
            }

            // feindliche Einheiten
            else if (hitInfo.transform.GetComponent<OO_Unit>().Team != MyTeam)
            {
                if (selectedUnit.GetComponentInChildren<OO_Unit>().IsActiv == true)
                {
                    actions.Attack();
                }
            }
        }
    }

    #endregion



    #region Methoden

    public void SelectUnit()
    {
        // Setzt Einheit und dessen Feld als ausgewählt
        // Findet über Server statt, damit die SyncVars getriggert werden
        SelectObjects();
        Debug.Log("SelectObjects end");
        StartCoroutine(WaitForStrings());
        Debug.Log("SelectUnit end");
        // Färbe Felder der Einheit in der Sie eine Aktion ausführen kann        
    }

    IEnumerator WaitForStrings()
    {
        Debug.Log("WaitForStrings start");
        yield return new WaitUntil(() => stringProcessing);
        Debug.Log("WaitForStrings continue");
        selectedUnitString = selectedUnitString[0].ToString() + selectedUnitString[1].ToString() + selectedUnitString[selectedUnitString.Length - 1];

        CmdCallSelection(myTeam, selectedUnitString, selectedTileString);

        Debug.Log("Coroutine start");
        StartCoroutine(WaitForSelection());
    }

    IEnumerator WaitForSelection()
    {
        yield return new WaitUntil(() => selectionDone);
        ColorPattern();
        if (selectedUnit.GetComponentInChildren<OO_Unit>().Team == myTeam)
        {
            // Färbt Einheit und dessen Feld als Freund
            //material.SetMaterial(selectedUnit, material.AllySelectedMaterial);
            material.SetMaterial(selectedTile, material.AllySelectedMaterial);

            // Zustand wechselt in Verbündeten ausgewählt
            controller.CurrentSelection = OO_Enum.selection.ALLY;
            controller.CurrentGameState = OO_Enum.gameState.ACTION_STATE;
        }

        else if (selectedUnit.GetComponentInChildren<OO_Unit>().Team != myTeam)
        {
            // Färbt Einheit und dessen Feld als Feind
            //material.SetMaterial(selectedUnit, material.EnemySelectedMaterial);
            material.SetMaterial(selectedTile, material.EnemySelectedMaterial);

            // Zustand wechselt in Feind ausgewählt
            controller.CurrentSelection = OO_Enum.selection.ENEMY;
            controller.CurrentGameState = OO_Enum.gameState.ACTION_STATE;
        }
        Debug.Log("Coroutine end");

    }

    public void DeselectSelectedUnit()
    {
        if (selectedUnit)
        {
            // Bringe Felder auf Standardfarbe
            DecolorSelectedUnit();

            // Setze Feldzustand auf Standard
            foreach (GameObject g in lists.TileList)
            {
                if (g.GetComponent<OO_Tile>().Type != OO_Enum.tileReachability.DEFAULT)
                {
                    CmdSetTileType(g, OO_Enum.tileReachability.DEFAULT);
                }
            }

            // Wähle Einheit und Feld ab
            // Findet über Server statt, damit die SyncVars getriggert werden
            CmdDeselectObjects();

            // Setze Zustände zurück
            controller.CurrentSelection = OO_Enum.selection.NOTHING_SELECTED;
            controller.CurrentGameState = OO_Enum.gameState.SELECTION_STATE;
        }
    }

    #endregion



    #region Hilfsmethoden

    [Command]
    private void CmdSetHitInfo(Transform hitTransform)
    {
        hitInfoTransform = hitTransform;
    }

    private void DecolorSelectedUnit()
    {
        // Färbt die ausgewählte Einheit und ihre Felder zurück in den Standard

        if (selectedUnit.GetComponent<OO_Unit>().Team == false)
        {
            if (selectedUnit.GetComponent<OO_Unit>().IsActiv)
            {
                material.SetMaterial(selectedUnit, material.TeamFalseActiveMaterial);
            }

            else if (!selectedUnit.GetComponent<OO_Unit>().IsActiv)
            {
                material.SetMaterial(selectedUnit, material.TeamFalsePassivMaterial);
            }
        }

        else if (selectedUnit.GetComponent<OO_Unit>().Team == true)
        {
            if (selectedUnit.GetComponent<OO_Unit>().IsActiv)
            {
                material.SetMaterial(selectedUnit, material.TeamTrueActiveMaterial);
            }

            else if (!selectedUnit.GetComponent<OO_Unit>().IsActiv)
            {
                material.SetMaterial(selectedUnit, material.TeamTruePassivMaterial);
            }
        }

        foreach (GameObject g in lists.TileList)
        {
            if (g.GetComponent<OO_Tile>().Type != OO_Enum.tileReachability.DEFAULT)
            {
                material.SetMaterial(g, material.TileDefaultMaterial);
            }
        }
    }

    [Command]
    private void CmdCallSelection(bool team, string unit, string tile)
    {
        if(team == myTeam)
        {
            RpcCallSelection(unit, tile);
        }
    }

    [ClientRpc]
    private void RpcCallSelection(string unit, string tile)
    {
        Debug.Log("RpcCallSelection start");
        selectedUnit = GameObject.Find("UnitFolder").transform.Find(unit).transform.GetChild(0).gameObject;
        selectedTile = GameObject.Find("TileFolder").transform.Find(tile).gameObject;
        selectionDone = true;
        Debug.Log("RpcCallSelection end");
    }

    [Command]
    private void CmdSelectObjects(string unit, string tile)
    {
        Debug.Log("Entered CmdSelectObjects");
        RpcSelectObjects(unit, tile);
        Debug.Log("Leaving CmdSelectObjects");
    }

    [ClientRpc]
    private void RpcSelectObjects(string unit, string tile)
    {
        Debug.Log("Entered RpcSelectObjects");
        selectedUnitString = unit;
        selectedTileString = tile;
        stringProcessing = true;
        Debug.Log("Leaving RpcSelectObjects");
    }

    private void SelectObjects()
    {
        // Wählt angeklickte Einheit aus

        // Einheit
        if (hitInfo.transform.GetComponent<OO_Unit>())
        {
            //// Einheit und dessen Feld wird ausgewählt
            //selectedUnit = hitInfo.transform.gameObject;
            //selectedTile = hitInfo.transform.GetComponent<OO_Unit>().CurrentTile.GetComponent<OO_Tile>().gameObject;
            CmdSelectObjects(hitInfo.transform.gameObject.name, hitInfo.transform.GetComponent<OO_Unit>().CurrentTile.GetComponent<OO_Tile>().gameObject.name);
        }

        // Feld
        else if (hitInfo.transform.GetComponent<OO_Tile>())
        {
            // Feld und darauf stehende Einheit wird ausgewählt
            //selectedTile = hitInfo.transform.gameObject;
            //selectedUnit = hitInfo.transform.GetComponent<OO_Tile>().CurrentUnit.GetComponentInChildren<OO_Unit>().gameObject;
            CmdSelectObjects(hitInfo.transform.GetComponent<OO_Tile>().CurrentUnit.GetComponentInChildren<OO_Unit>().gameObject.name, hitInfo.transform.gameObject.name);
        }
    }

    [Command]
    private void CmdDeselectObjects()
    {
        RpcDeselectObjects();
    }

    [ClientRpc]
    private void RpcDeselectObjects()
    {
        selectedTile = null;
        selectedUnit = null;
        selectedTileString = "";
        selectedUnitString = "";
        selectionDone = false;
        stringProcessing = false;
    }

    public void ColorPattern()
    {
        // Färbt die Felder anhand der übergebenen Muster der Einheiten

        List<int[]> attackPattern;
        List<int[]> movePattern;

        int localX;
        int localZ;

        attackPattern = selectedUnit.GetComponent<OO_Unit>().AttackPattern;
        movePattern = selectedUnit.GetComponent<OO_Unit>().MovePattern;
        localX = (int)selectedUnit.transform.position.x;
        localZ = (int)selectedUnit.transform.position.z;

        foreach (int[] i in attackPattern)
        {
            // Koordinaten des Pattern in Weltkoordinaten umwandeln
            int worldX = i[0] + localX;
            int worldZ = i[1] + localZ;

            // Vergleiche Koordinaten von Muster und Felder, wenn diese übereinstimmen 
            foreach (GameObject g in lists.TileList)
            {
                if (worldX == g.transform.position.x && worldZ == g.transform.position.z)
                {
                    material.SetMaterial(g, material.TileAttackMaterial);
                    //g.GetComponent<OO_Tile>().Type = OO_Enum.tileReachability.ATTACKABLE;
                    CmdSetTileType(g, OO_Enum.tileReachability.ATTACKABLE);
                    break;
                }
            }
        }

        foreach (int[] i in movePattern)
        {
            // Koordinaten des Pattern in Weltkoordinaten umwandeln
            int worldX = i[0] + localX;
            int worldZ = i[1] + localZ;

            // Vergleiche Koordinaten von Muster und Felder, wenn diese übereinstimmen 
            foreach (GameObject g in lists.TileList)
            {
                if (worldX == g.transform.position.x && worldZ == g.transform.position.z)
                {
                    if (g.GetComponent<OO_Tile>().Type == OO_Enum.tileReachability.ATTACKABLE)
                    {
                        material.SetMaterial(g, material.TileBothableMaterial);
                        CmdSetTileType(g, OO_Enum.tileReachability.BOTHABLE);
                        //g.GetComponent<OO_Tile>().Type = OO_Enum.tileReachability.BOTHABLE;
                    }
                    else
                    {
                        material.SetMaterial(g, material.TileMoveMaterial);
                        CmdSetTileType(g, OO_Enum.tileReachability.MOVEABLE);
                        //g.GetComponent<OO_Tile>().Type = OO_Enum.tileReachability.MOVEABLE;
                    }
                    break;
                }
            }
        }
    }

    [Command]
    private void CmdSetTileType(GameObject tile, OO_Enum.tileReachability type)
    {
        tile.GetComponent<OO_Tile>().Type = type;
    }

    #endregion



    #region Getter Setter

    public GameObject SelectedUnit { get => selectedUnit; set => selectedUnit = value; }
    public GameObject SelectedTile { get => selectedTile; set => selectedTile = value; }
    public RaycastHit HitInfo { get => hitInfo; }
    public bool Hit { get => hit; }
    public bool FoundByController { get => foundByController; set => foundByController = value; }
    public bool MyTeam { get => myTeam; set => myTeam = value; }
    public OO_Actions Actions { get => actions; set => actions = value; }
    public OO_Material Material { get => material; set => material = value; }
    public OO_Sound Sound { get => sound; set => sound = value; }
    public OO_Lists Lists { get => lists; set => lists = value; }
    public OO_Controller Controller { get => controller; set => controller = value; }
    public Transform HitInfoTransform { get => hitInfoTransform; set => hitInfoTransform = value; }

    #endregion
}