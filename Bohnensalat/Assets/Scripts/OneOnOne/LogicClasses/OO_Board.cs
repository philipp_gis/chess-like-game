﻿using UnityEngine;
using Mirror;
using System.Collections.Generic;
using UnityEngine.Tilemaps;
using System.Diagnostics;

public class OO_Board : NetworkBehaviour
{
    // Verwaltung des Spielfeldes

    #region Header

    private OO_Lists lists;
    private OO_Material material;
    private OO_Pattern pattern;
    private OO_Sound sound;

    // Ordner in dennen die Felder und Einheiten abgelegt werden sollen um eine bessere Übersicht in der UnityHierarchy zu bewahren

    [Header("Folder References")]
    [SerializeField] private GameObject unitFolder;
    [SerializeField] private GameObject tileFolder;

    // Mit der Spielfeldbreite und Tiefe wird die Spielfeldgröße ermittelt

    [Header("Board Attributes")]
    [SerializeField] private int boardDepth = 10;
    [SerializeField] private int boardWidth = 10;
    private List<string> destroyTileSequenz = new List<string> { "T40", "T59", "T49", "T50", "T30", "T69", "T60", "T39", "T41", "T58",
                                                                 "T48", "T51", "T31", "T68", "T61", "T38", "T42", "T57", "T52", "T47",
                                                                 "T32", "T67", "T37", "T62", "T20", "T79", "T29", "T70", "T21", "T78",
                                                                 "T71", "T28", "T22", "T77", "T27", "T72", "T10", "T89", "T80", "T19",
                                                                 "T00", "T99", "T09", "T90", "T11", "T88", "T18", "T81", "T01", "T98",
                                                                 "T08", "T91", "T12", "T87", "T17", "T82", "T02", "T97", "T07", "T92",
                                                                 "T03", "T96", "T06", "T93", "T04", "T95", "T05", "T94", "T13", "T86",
                                                                 "T16", "T83", "T14", "T85", "T15", "T84", "T23", "T76", "T26", "T73",
                                                                 "T24", "T75", "T25", "T74", "T33", "T66", "T43", "T56", "T53", "T46",
                                                                 "T63", "T36", "T64", "T35", "T65", "T34", "T55", "T45", "T44", "T54" };

    public int destroyIndex;

    void Awake()
    {
        lists = GetComponent<OO_Lists>();
        material = GetComponent<OO_Material>();
        pattern = GetComponent<OO_Pattern>();
        sound = GetComponent<OO_Sound>();

        destroyIndex = 0;
    }

    #endregion



    #region Methoden

    // UnitFolder und TileFolder sind jetzt Prefabs in der Scene
    // Beide beinhalten bereits die erstellten Units und Tiles
    // Somit sollen die Referenzen auch für den Client gesetzt sein
    public void SetupBoard()
    {
        unitFolder.SetActive(true);
        tileFolder.SetActive(true);
        // Felder werden initialisiert, benannt, in UnityHierarchy in Ordner gesteckt und in die Feldliste gesteckt
        for(int i = 0; i < 100; i++)
        {
            GameObject tile = tileFolder.transform.GetChild(i).gameObject;
            if(i < 10)
            {
                GameObject unit = unitFolder.transform.GetChild(i).gameObject;
                lists.UnitList.Add(unit);
                lists.TeamFalseList.Add(unit);
                unit.GetComponentInChildren<OO_Unit>().CurrentTile = tile;
                unit.GetComponentInChildren<OO_Unit>().CurrentTile.GetComponent<OO_Tile>().CurrentUnit = unit.GetComponentInChildren<Transform>().GetChild(0).transform.gameObject;
                material.SetMaterial(unit, material.TeamFalseActiveMaterial);
                unit.GetComponentInChildren<OO_Unit>().Team = false;
                unit.GetComponentInChildren<OO_Unit>().IsActiv = true;

                // Einheiten bekommen Anfangsbewegungs und Angriffsmuster zugewiesen
                unit.GetComponentInChildren<OO_Unit>().MovePattern = pattern.MpBasic;
                unit.GetComponentInChildren<OO_Unit>().MovePatternName = lists.PatternBasic[0].Item2;
                unit.GetComponentInChildren<OO_Unit>().AttackPattern = pattern.rotatePattern(pattern.ApBasic, false);
                unit.GetComponentInChildren<OO_Unit>().AttackPatternName = lists.PatternBasic[1].Item2;

                // Animator für Bewegungen zugewiesen
                unit.GetComponentInChildren<OO_Unit>().Animator = (Animator)Resources.Load("Animation/MoveAnimations/Unit");
                unit.GetComponentInChildren<OO_Unit>().UnitNetworkAnimator = new NetworkAnimator();
                unit.GetComponentInChildren<OO_Unit>().UnitNetworkAnimator.animator = unit.GetComponentInChildren<OO_Unit>().Animator;
            }
            if(i > 89)
            {
                GameObject unit = unitFolder.transform.GetChild(i-80).gameObject;
                lists.UnitList.Add(unit);
                lists.TeamTrueList.Add(unit);
                unit.GetComponentInChildren<OO_Unit>().CurrentTile = tile;
                unit.GetComponentInChildren<OO_Unit>().CurrentTile.GetComponent<OO_Tile>().CurrentUnit = unit.GetComponentInChildren<Transform>().GetChild(0).transform.gameObject;
                material.SetMaterial(unit, material.TeamTrueActiveMaterial);
                unit.GetComponentInChildren<OO_Unit>().Team = true;
                unit.GetComponentInChildren<OO_Unit>().IsActiv = true;

                // Einheiten bekommen Anfangsbewegungs und Angriffsmuster zugewiesen
                unit.GetComponentInChildren<OO_Unit>().MovePattern = pattern.MpBasic;
                unit.GetComponentInChildren<OO_Unit>().MovePatternName = lists.PatternBasic[0].Item2;
                unit.GetComponentInChildren<OO_Unit>().AttackPattern = pattern.rotatePattern(pattern.ApBasic, true);
                unit.GetComponentInChildren<OO_Unit>().AttackPatternName = lists.PatternBasic[1].Item2;

                // Animator für Bewegungen zugewiesen
                unit.GetComponentInChildren<OO_Unit>().Animator = (Animator)Resources.Load("Animation/MoveAnimations/Unit");
                unit.GetComponentInChildren<OO_Unit>().UnitNetworkAnimator = new NetworkAnimator();
                unit.GetComponentInChildren<OO_Unit>().UnitNetworkAnimator.animator = unit.GetComponentInChildren<OO_Unit>().Animator;
            }
            lists.TileList.Add(tile);
        }
    }

    public void DestroyTile()
    {
        if (destroyIndex < destroyTileSequenz.Count)
        {
            GameObject tileToDestroy = null;
            GameObject unitToDestroy = null;


            foreach (GameObject g in lists.TileList)
            {
                if (g.name == destroyTileSequenz[destroyIndex])
                {
                    tileToDestroy = g;
                }
            }

            if (tileToDestroy.GetComponent<OO_Tile>().CurrentUnit)
            {
                unitToDestroy = tileToDestroy.GetComponent<OO_Tile>().CurrentUnit;

                lists.UnitList.Remove(unitToDestroy.transform.parent.gameObject);

                if (tileToDestroy.GetComponent<OO_Tile>().CurrentUnit.GetComponentInChildren<OO_Unit>().Team == false)
                {
                    lists.TeamFalseList.Remove(unitToDestroy.transform.parent.gameObject);
                }

                else if (tileToDestroy.GetComponent<OO_Tile>().CurrentUnit.GetComponentInChildren<OO_Unit>().Team == true)
                {
                    lists.TeamTrueList.Remove(unitToDestroy.transform.parent.gameObject);
                }

                Destroy(unitToDestroy.transform.parent.gameObject);
            }
            sound.playSound(sound.SoundSource, sound.TileDestructionSound);
            lists.TileList.Remove(tileToDestroy);
            Destroy(tileToDestroy);
        }
        destroyIndex++;
    }

    #endregion
}
