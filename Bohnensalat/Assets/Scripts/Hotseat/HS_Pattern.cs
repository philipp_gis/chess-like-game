﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HS_Pattern : MonoBehaviour
{
    [SerializeField] private GameObject Boards;

    private int boardWidth;
    private int boardHeight;
    private int relativPosition;
    private static bool o = false;
    private static bool P = false;
    private static bool X = true;

    public static List<Movepattern> allMP = new List<Movepattern>();
    public static List<Attackpattern> allAP = new List<Attackpattern>();

    public static List<List<Movepattern>> all_MP_Lists = new List<List<Movepattern>>();
    public static List<List<Attackpattern>> all_AP_Lists = new List<List<Attackpattern>>();

    public List<Movepattern> grade2MP = new List<Movepattern>();
    public List<Movepattern> grade3MP = new List<Movepattern>();
    public List<Movepattern> grade4MP = new List<Movepattern>();
    public List<Movepattern> grade5MP = new List<Movepattern>();
    public List<Movepattern> grade6MP = new List<Movepattern>();
    public List<Movepattern> grade7MP = new List<Movepattern>();
    public List<Movepattern> grade8MP = new List<Movepattern>();
    public List<Movepattern> grade9MP = new List<Movepattern>();
    public List<Movepattern> grade10MP = new List<Movepattern>();

    public List<Attackpattern> grade2AP = new List<Attackpattern>();
    public List<Attackpattern> grade3AP = new List<Attackpattern>();
    public List<Attackpattern> grade4AP = new List<Attackpattern>();
    public List<Attackpattern> grade5AP = new List<Attackpattern>();
    public List<Attackpattern> grade6AP = new List<Attackpattern>();
    public List<Attackpattern> grade7AP = new List<Attackpattern>();
    public List<Attackpattern> grade8AP = new List<Attackpattern>();
    public List<Attackpattern> grade9AP = new List<Attackpattern>();
    public List<Attackpattern> grade10AP = new List<Attackpattern>();


    public class Movepattern
    {
        public string name;
        public int grade;
        public bool[,] pattern;

        public Movepattern(string name, int grade, bool[,] pattern)
        {
            this.name = name;
            this.grade = grade;
            this.pattern = pattern;
            allMP.Add(this);
        }
    }

    public class Attackpattern
    {
        public string name;
        public int grade;
        public bool[,] pattern;

        public Attackpattern(string name, int grade, bool[,] pattern)
        {
            this.name = name;
            this.grade = grade;
            this.pattern = pattern;
            allAP.Add(this);
        }
    }

    // Pawn Pattern
    // Move
    public static bool[,] mp_pawn_1 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] mp_pawn_2 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] mp_pawn_3 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] mp_pawn_4 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] mp_pawn_5 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] mp_pawn_6 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] mp_pawn_7 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] mp_pawn_8 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, X, X, P, X, X, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] mp_pawn_9 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, X, X, P, X, X, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] mp_pawn_10 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, X, X, P, X, X, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    private Movepattern pawn_1_mp = new Movepattern("Pawn 1 Movement", 1, mp_pawn_1);
    private Movepattern pawn_2_mp = new Movepattern("Pawn 2 Movement", 2, mp_pawn_2);
    private Movepattern pawn_3_mp = new Movepattern("Pawn 3 Movement", 3, mp_pawn_3);
    private Movepattern pawn_4_mp = new Movepattern("Pawn 4 Movement", 4, mp_pawn_4);
    private Movepattern pawn_5_mp = new Movepattern("Pawn 5 Movement", 5, mp_pawn_5);
    private Movepattern pawn_6_mp = new Movepattern("Pawn 6 Movement", 6, mp_pawn_6);
    private Movepattern pawn_7_mp = new Movepattern("Pawn 7 Movement", 7, mp_pawn_7);
    private Movepattern pawn_8_mp = new Movepattern("Pawn 8 Movement", 8, mp_pawn_8);
    private Movepattern pawn_9_mp = new Movepattern("Pawn 9 Movement", 9, mp_pawn_9);
    private Movepattern pawn_10_mp = new Movepattern("Pawn 10 Movement", 10, mp_pawn_10);
    // Attack
    public static bool[,] ap_pawn_1 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] ap_pawn_2 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] ap_pawn_3 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] ap_pawn_4 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] ap_pawn_5 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] ap_pawn_6 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] ap_pawn_7 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] ap_pawn_8 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] ap_pawn_9 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] ap_pawn_10 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, X, X, P, X, X, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    private Attackpattern pawn_1_ap = new Attackpattern("Pawn 1 Attack", 1, ap_pawn_1);
    private Attackpattern pawn_2_ap = new Attackpattern("Pawn 2 Attack", 2, ap_pawn_2);
    private Attackpattern pawn_3_ap = new Attackpattern("Pawn 3 Attack", 3, ap_pawn_3);
    private Attackpattern pawn_4_ap = new Attackpattern("Pawn 4 Attack", 4, ap_pawn_4);
    private Attackpattern pawn_5_ap = new Attackpattern("Pawn 5 Attack", 5, ap_pawn_5);
    private Attackpattern pawn_6_ap = new Attackpattern("Pawn 6 Attack", 6, ap_pawn_6);
    private Attackpattern pawn_7_ap = new Attackpattern("Pawn 7 Attack", 7, ap_pawn_7);
    private Attackpattern pawn_8_ap = new Attackpattern("Pawn 8 Attack", 8, ap_pawn_8);
    private Attackpattern pawn_9_ap = new Attackpattern("Pawn 9 Attack", 9, ap_pawn_9);
    private Attackpattern pawn_10_ap = new Attackpattern("Pawn 10 Attack", 10, ap_pawn_10);


    // Rush Pattern
    // Move
    public static bool[,] mp_rush_2 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] mp_rush_3 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] mp_rush_4 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, X, X, P, X, X, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] mp_rush_5 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, X, X, P, X, X, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] mp_rush_6 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, X, X, P, X, X, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] mp_rush_7 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, X, X, P, X, X, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] mp_rush_8 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, X, X, X, P, X, X, X, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] mp_rush_9 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, X, X, X, P, X, X, X, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] mp_rush_10 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, X, X, X, P, X, X, X, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    private Movepattern rush_2_mp = new Movepattern("Rush 2 Movement", 2, mp_rush_2);
    private Movepattern rush_3_mp = new Movepattern("Rush 3 Movement", 3, mp_rush_3);
    private Movepattern rush_4_mp = new Movepattern("Rush 4 Movement", 4, mp_rush_4);
    private Movepattern rush_5_mp = new Movepattern("Rush 5 Movement", 5, mp_rush_5);
    private Movepattern rush_6_mp = new Movepattern("Rush 6 Movement", 6, mp_rush_6);
    private Movepattern rush_7_mp = new Movepattern("Rush 7 Movement", 7, mp_rush_7);
    private Movepattern rush_8_mp = new Movepattern("Rush 8 Movement", 8, mp_rush_8);
    private Movepattern rush_9_mp = new Movepattern("Rush 9 Movement", 9, mp_rush_9);
    private Movepattern rush_10_mp = new Movepattern("Rush 10 Movement", 10, mp_rush_10);

    // Attack
    public static bool[,] ap_rush_2 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] ap_rush_3 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] ap_rush_4 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] ap_rush_5 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, P, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] ap_rush_6 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, X, X, P, X, X, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] ap_rush_7 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, X, X, P, X, X, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] ap_rush_8 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, X, X, P, X, X, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] ap_rush_9 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, X, X, P, X, X, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] ap_rush_10 = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, X, X, X, P, X, X, X, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, X, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    private Attackpattern rush_2_ap = new Attackpattern("Rush 2 Attack", 2, ap_rush_2);
    private Attackpattern rush_3_ap = new Attackpattern("Rush 3 Attack", 3, ap_rush_3);
    private Attackpattern rush_4_ap = new Attackpattern("Rush 4 Attack", 4, ap_rush_4);
    private Attackpattern rush_5_ap = new Attackpattern("Rush 5 Attack", 5, ap_pawn_5);
    private Attackpattern rush_6_ap = new Attackpattern("Rush 6 Attack", 6, ap_rush_6);
    private Attackpattern rush_7_ap = new Attackpattern("Rush 7 Attack", 7, ap_rush_7);
    private Attackpattern rush_8_ap = new Attackpattern("Rush 8 Attack", 8, ap_rush_8);
    private Attackpattern rush_9_ap = new Attackpattern("Rush 9 Attack", 9, ap_rush_9);
    private Attackpattern rush_10_ap = new Attackpattern("Rush 10 Attack", 10, ap_rush_10);

    // Spider Pattern
    // Move
    public static bool[,] mp_spider_2 = new bool[,]
{
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
};
    public static bool[,] mp_spider_3 = new bool[,]
{
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
};
    public static bool[,] mp_spider_4 = new bool[,]
{
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
};
    public static bool[,] mp_spider_5 = new bool[,]
{
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
};
    public static bool[,] mp_spider_6 = new bool[,]
{
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
};
    public static bool[,] mp_spider_7 = new bool[,]
{
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, X, o, o, o, o, o, X, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
};
    public static bool[,] mp_spider_8 = new bool[,]
{
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, X, o, o, o, o, o, X, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
};
    public static bool[,] mp_spider_9 = new bool[,]
{
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, X, o, o, o, o, o, X, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, X, o, o, o, o, o, X, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
};
    public static bool[,] mp_spider_10 = new bool[,]
{
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, X, o, o, o, o, o, X, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, X, o, o, o, o, o, X, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
};
    private Movepattern spider_2_mp = new Movepattern("Spider 2 Movement", 2, mp_spider_2);
    private Movepattern spider_3_mp = new Movepattern("Spider 3 Movement", 3, mp_spider_3);
    private Movepattern spider_4_mp = new Movepattern("Spider 4 Movement", 4, mp_spider_4);
    private Movepattern spider_5_mp = new Movepattern("Spider 5 Movement", 5, mp_spider_5);
    private Movepattern spider_6_mp = new Movepattern("Spider 6 Movement", 6, mp_spider_6);
    private Movepattern spider_7_mp = new Movepattern("Spider 7 Movement", 7, mp_spider_7);
    private Movepattern spider_8_mp = new Movepattern("Spider 8 Movement", 8, mp_spider_8);
    private Movepattern spider_9_mp = new Movepattern("Spider 9 Movement", 9, mp_spider_9);
    private Movepattern spider_10_mp = new Movepattern("Spider 10 Movement", 10, mp_spider_10);

    // Attack
    public static bool[,] ap_spider_2 = new bool[,]{
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] ap_spider_3 = new bool[,]{
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
    };
    public static bool[,] ap_spider_4 = new bool[,]
{
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
};
    public static bool[,] ap_spider_5 = new bool[,]
{
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
};
    public static bool[,] ap_spider_6 = new bool[,]
{
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
};
    public static bool[,] ap_spider_7 = new bool[,]
{
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
};
    public static bool[,] ap_spider_8 = new bool[,]
{
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
};
    public static bool[,] ap_spider_9 = new bool[,]
{
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, X, o, o, o, o, o, X, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
};
    public static bool[,] ap_spider_10 = new bool[,]
{
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, X, o, o, o, o, o, X, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, X, X, X, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, P, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, X, o, X, o, o, o, o, o, o},
        {o, o, o, o, o, X, o, o, o, X, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
};
    private Attackpattern spider_2_ap = new Attackpattern("Spider 2 Attack", 2, ap_spider_2);
    private Attackpattern spider_3_ap = new Attackpattern("Spider 3 Attack", 3, ap_spider_3);
    private Attackpattern spider_4_ap = new Attackpattern("Spider 4 Attack", 4, ap_spider_4);
    private Attackpattern spider_5_ap = new Attackpattern("Spider 5 Attack", 5, ap_spider_5);
    private Attackpattern spider_6_ap = new Attackpattern("Spider 6 Attack", 6, ap_spider_6);
    private Attackpattern spider_7_ap = new Attackpattern("Spider 7 Attack", 7, ap_spider_7);
    private Attackpattern spider_8_ap = new Attackpattern("Spider 8 Attack", 8, ap_spider_8);
    private Attackpattern spider_9_ap = new Attackpattern("Spider 9 Attack", 9, ap_spider_9);
    private Attackpattern spider_10_ap = new Attackpattern("Spider 10 Attack", 10, ap_spider_10);






    public List<List<Movepattern>> All_MP_Lists
    {
        get
        {
            return all_MP_Lists;
        }
    }
    public List<List<Attackpattern>> All_AP_Lists
    {
        get
        {
            return all_AP_Lists;
        }
    }

    public List<Movepattern> AllMP
    {
        get
        {
            return allMP;
        }
    }
    public List<Attackpattern> AllAP
    {
        get
        {
            return allAP;
        }
    }

    public void Start()
    {
        // Pattern.createLists()
        // -->
        boardWidth = Boards.GetComponent<HS_Board>().Width;
        boardHeight = Boards.GetComponent<HS_Board>().Height;

        foreach (Movepattern m in allMP)
        {
            if (m.grade == 2) { grade2MP.Add(m); }
            if (m.grade == 3) { grade3MP.Add(m); }
            if (m.grade == 4) { grade4MP.Add(m); }
            if (m.grade == 5) { grade5MP.Add(m); }
            if (m.grade == 6) { grade6MP.Add(m); }
            if (m.grade == 7) { grade7MP.Add(m); }
            if (m.grade == 8) { grade8MP.Add(m); }
            if (m.grade == 9) { grade9MP.Add(m); }
            if (m.grade == 10) { grade10MP.Add(m); }
        }

        all_MP_Lists.Add(grade2MP);
        all_MP_Lists.Add(grade3MP);
        all_MP_Lists.Add(grade4MP);
        all_MP_Lists.Add(grade5MP);
        all_MP_Lists.Add(grade6MP);
        all_MP_Lists.Add(grade7MP);
        all_MP_Lists.Add(grade8MP);
        all_MP_Lists.Add(grade9MP);
        all_MP_Lists.Add(grade10MP);

        foreach (Attackpattern a in allAP)
        {
            if (a.grade == 2) { grade2AP.Add(a); }
            if (a.grade == 3) { grade3AP.Add(a); }
            if (a.grade == 4) { grade4AP.Add(a); }
            if (a.grade == 5) { grade5AP.Add(a); }
            if (a.grade == 6) { grade6AP.Add(a); }
            if (a.grade == 7) { grade7AP.Add(a); }
            if (a.grade == 8) { grade8AP.Add(a); }
            if (a.grade == 9) { grade9AP.Add(a); }
            if (a.grade == 10) { grade10AP.Add(a); }
        }

        all_AP_Lists.Add(grade2AP);
        all_AP_Lists.Add(grade3AP);
        all_AP_Lists.Add(grade4AP);
        all_AP_Lists.Add(grade5AP);
        all_AP_Lists.Add(grade6AP);
        all_AP_Lists.Add(grade7AP);
        all_AP_Lists.Add(grade8AP);
        all_AP_Lists.Add(grade9AP);
        all_AP_Lists.Add(grade10AP);
        // <--
    }
}
