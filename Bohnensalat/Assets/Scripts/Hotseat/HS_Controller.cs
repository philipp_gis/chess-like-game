﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HS_Controller : MonoBehaviour
{
    [SerializeField] public GameObject selected_Tile;
    [SerializeField] public GameObject selected_Unit;
    [SerializeField] private GameObject board;
    [SerializeField] private GameObject Pattern;
    [SerializeField] public GameObject buttons;
    [SerializeField] private GameObject CoinToss;
    [SerializeField] private GameObject timer;
    [SerializeField] private GameObject winpanel;

    [SerializeField] private Button Choice_1;
    [SerializeField] private Button Choice_2;
    [SerializeField] private Button Choice_3;
    [SerializeField] private Button skip_Button;

    [SerializeField] private Material selected_Material;
    [SerializeField] private Material unit_Team_One_active_Material;
    [SerializeField] private Material unit_Team_Two_active_Material;
    [SerializeField] private Material unit_Team_One_passiv_Material;
    [SerializeField] private Material unit_Team_Two_passiv_Material;
    [SerializeField] private Material tile_Material;
    [SerializeField] private Material tile_Moveable_Material;
    [SerializeField] private Material tile_Attackable_Material;
    [SerializeField] private Material tile_Bothable_Material;
    [SerializeField] private string unit_name;
    [SerializeField] private string tile_name;

    [SerializeField] private AudioClip move_source;
    [SerializeField] private AudioClip attack_source;
    [SerializeField] private AudioClip fusion_source;
    [SerializeField] private AudioClip click_source;
    [SerializeField] private AudioClip button_source;
    [SerializeField] private AudioClip levelup_source;

    private int rnd1;
    private int rnd2;
    private int rnd3;
    private bool two_move_or_attack_pattern;


    private RaycastHit hitInfo;

    private void Start()
    {
        selected_Material = (Material)Resources.Load("Material/Selected_Material", typeof(Material));
        unit_Team_One_active_Material = (Material)Resources.Load("Material/Unit_Team_1_active_Material", typeof(Material));
        unit_Team_Two_active_Material = (Material)Resources.Load("Material/Unit_Team_2_active_Material", typeof(Material));
        unit_Team_One_passiv_Material = (Material)Resources.Load("Material/Unit_Team_2_passiv_Material", typeof(Material));
        unit_Team_Two_passiv_Material = (Material)Resources.Load("Material/Unit_Team_1_passiv_Material", typeof(Material));
        tile_Material = (Material)Resources.Load("Material/Tile_Material", typeof(Material));
        tile_Moveable_Material = (Material)Resources.Load("Material/Tile_Moveable", typeof(Material));
        tile_Attackable_Material = (Material)Resources.Load("Material/Tile_Attackable", typeof(Material));
        tile_Bothable_Material = (Material)Resources.Load("Material/Tile_Bouthable", typeof(Material));
        unit_name = "Unit(Clone)";
        tile_name = "Tile(Clone)";

        buttons.SetActive(false);
        winpanel.SetActive(false);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!CoinToss.activeInHierarchy && !buttons.activeInHierarchy && !winpanel.activeInHierarchy)
            {

                hitInfo = new RaycastHit();
                bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);

                switch (hit)
                {
                    case true:

                        switch (hit_Unit())
                        {
                            case true:

                                if (selected_Unit)
                                {
                                    if (selected_Unit.GetComponent<HS_Unit>().Is_activ == true)
                                    {
                                        if (Unit_not_Target())
                                        {
                                            if (!Unit_is_Enemy())
                                            {
                                                if (moveable())
                                                {
                                                    AudioSource.PlayClipAtPoint(fusion_source, selected_Unit.transform.position);
                                                    fusion();
                                                }
                                            }

                                            else if (Unit_is_Enemy())
                                            {

                                                if (attackable())
                                                {
                                                    AudioSource.PlayClipAtPoint(attack_source, selected_Unit.transform.position);
                                                    attack();
                                                }
                                            }
                                        }
                                    }
                                }

                                else if (!selected_Unit)
                                {
                                    if (hitInfo.transform.GetComponent<HS_Unit>().Is_activ == true)
                                    {
                                        select_Unit();

                                        if (selected_Tile)
                                        {
                                            deselect_Tile();
                                        }
                                        select_Current_Tile();
                                    }
                                }
                                break;
                        }

                        switch (hit_Tile())
                        {
                            case true:

                                if (selected_Tile)
                                {
                                    if (Tile_not_Target())
                                    {
                                        if (selected_current_Unit())
                                        {
                                            if (selected_Tile.GetComponent<HS_Tile>().CurrentUnit.GetComponent<HS_Unit>().Is_activ)
                                            {
                                                if (target_current_Unit())
                                                {
                                                    if (!Tile_is_Enemy())
                                                    {
                                                        if (moveable())
                                                        {
                                                            AudioSource.PlayClipAtPoint(fusion_source, selected_Unit.transform.position);
                                                            fusion();
                                                        }
                                                    }

                                                    else if (Tile_is_Enemy())
                                                    {
                                                        if (attackable())
                                                        {
                                                            AudioSource.PlayClipAtPoint(attack_source, selected_Unit.transform.position);
                                                            attack();
                                                        }
                                                    }
                                                }

                                                else if (!target_current_Unit())
                                                {
                                                    if (moveable())
                                                    {
                                                        AudioSource.PlayClipAtPoint(move_source, selected_Unit.transform.position);
                                                        move();
                                                    }
                                                }
                                            }
                                        }

                                        else if (!selected_current_Unit())
                                        {
                                            if (target_current_Unit())
                                            {
                                                deselect_Tile();
                                                select_Tile();

                                                if (selected_Unit)
                                                {
                                                    deselect_Unit();
                                                }

                                                select_Current_Unit();
                                            }

                                            else if (!target_current_Unit())
                                            {
                                                deselect_Tile();
                                                select_Tile();
                                            }
                                        }
                                    }
                                }

                                else if (!selected_Tile)
                                {
                                    if (hitInfo.transform.GetComponent<HS_Tile>().CurrentUnit != null)
                                    {
                                        if (hitInfo.transform.GetComponent<HS_Tile>().CurrentUnit.GetComponent<HS_Unit>().Is_activ)
                                        {
                                            select_Tile();

                                            if (selected_Unit)
                                            {
                                                deselect_Unit();
                                            }

                                            if (selected_current_Unit())
                                            {
                                                select_Current_Unit();
                                            }
                                        }
                                    }
                                }
                                break;
                        }
                        break;

                    case false:
                        AudioSource.PlayClipAtPoint(click_source, new Vector3(11.44f, 10.1f, 4.44f));
                        if (selected_Unit)
                        {
                            deselect_Unit();
                        }

                        if (selected_Tile)
                        {
                            deselect_Tile();
                        }
                        break;
                }
            }
        }

        if (!buttons.activeInHierarchy)
        {
            if (Input.GetMouseButtonDown(1))
            {
                if (selected_Unit)
                {
                    deselect_Unit();
                }

                if (selected_Tile)
                {
                    deselect_Tile();
                }
            }
        }
    }




    // Controll.fusion()
    // -->
    private void fusion()
    {
        float x = selected_Unit.transform.localScale.x;
        float y = selected_Unit.transform.localScale.y;
        float z = selected_Unit.transform.localScale.z;
        float attacker_height = selected_Unit.transform.position.y;
        int lvl = selected_Unit.transform.GetComponent<HS_Unit>().Level;

        decolor_Pattern();

        if (!selected_Unit.GetComponent<HS_Unit>().Is_Enemy)
        {
            board.GetComponent<HS_Board>().Team1List.RemoveAt(board.GetComponent<HS_Board>().Team1List.IndexOf(selected_Unit));
        }
        else if (selected_Unit.GetComponent<HS_Unit>().Is_Enemy)
        {
            board.GetComponent<HS_Board>().Team2List.RemoveAt(board.GetComponent<HS_Board>().Team2List.IndexOf(selected_Unit));
        }

        Destroy(selected_Unit);

        unit_Selector();

        selected_Unit.transform.localScale += new Vector3(-0.4f + x, -0.4f + y, -0.4f + z);
        selected_Unit.transform.position += new Vector3(0f, attacker_height - 0.9f, 0f);

        selected_Unit.transform.GetComponent<HS_Unit>().Level += lvl;

        selected_Unit.GetComponentInChildren<Text>().text = selected_Unit.GetComponent<HS_Unit>().Level.ToString();

        if (selected_Unit.GetComponent<HS_Unit>().Is_activ)
        {
            decreaseTeamIndex();
        }

        selected_Unit.GetComponent<HS_Unit>().Is_activ = false;

        select_Pattern();
    }
    // <--

    // Controller.attack()
    // -->
    private void attack()
    {
        bool attacker_Team = selected_Unit.transform.GetComponent<HS_Unit>().Is_Enemy;
        Vector3 attacker_scale = selected_Unit.transform.localScale;
        float attacker_height = selected_Unit.transform.position.y;
        bool[,] attacker_movepattern = selected_Unit.GetComponent<HS_Unit>().MovePattern;
        bool[,] attacker_attackpattern = selected_Unit.GetComponent<HS_Unit>().AttackPattern;
        int lvl = selected_Unit.transform.GetComponent<HS_Unit>().Level;

        decolor_Pattern();

        int oldposteam1 = board.GetComponent<HS_Board>().Team1List.IndexOf(selected_Unit);
        int oldposteam2 = board.GetComponent<HS_Board>().Team2List.IndexOf(selected_Unit);

        if (selected_Unit.GetComponent<HS_Unit>().Is_Enemy)
        {
            board.GetComponent<HS_Board>().Team2List.Remove(selected_Unit);
        }

        if (!selected_Unit.GetComponent<HS_Unit>().Is_Enemy)
        {
            board.GetComponent<HS_Board>().Team1List.Remove(selected_Unit);
        }

        Destroy(selected_Unit);
        unit_Selector();

        if (selected_Unit.GetComponent<HS_Unit>().Is_Enemy)
        {
            board.GetComponent<HS_Board>().Team1List.Insert(oldposteam1, selected_Unit);
            board.GetComponent<HS_Board>().Team2List.RemoveAt(board.GetComponent<HS_Board>().Team2List.IndexOf(selected_Unit));
        }

        if (!selected_Unit.GetComponent<HS_Unit>().Is_Enemy)
        {
            board.GetComponent<HS_Board>().Team2List.Insert(oldposteam2, selected_Unit);
            board.GetComponent<HS_Board>().Team1List.RemoveAt(board.GetComponent<HS_Board>().Team1List.IndexOf(selected_Unit));
        }

        selected_Unit.transform.GetComponent<HS_Unit>().Level = lvl;
        selected_Unit.GetComponent<HS_Unit>().Is_Enemy = attacker_Team;
        selected_Unit.transform.localScale = attacker_scale;
        selected_Unit.transform.position += new Vector3(0f, attacker_height - 1.0f, 0f);
        selected_Unit.GetComponent<HS_Unit>().MovePattern = attacker_movepattern;
        selected_Unit.GetComponent<HS_Unit>().AttackPattern = attacker_attackpattern;
        selected_Unit.GetComponent<HS_Unit>().AttackPattern = selected_Unit.GetComponent<HS_Unit>().AttackPattern;
        selected_Unit.GetComponent<HS_Unit>().MovePattern = selected_Unit.GetComponent<HS_Unit>().MovePattern;
        selected_Unit.GetComponent<HS_Unit>().Is_activ = true;
        selected_Unit.GetComponentInChildren<Text>().text = selected_Unit.GetComponent<HS_Unit>().Level.ToString();

        decreaseTeamIndex();
        set_Unit_Passiv();

        deselect_Tile();
    }
    // <--

    // Controller.move()
    // -->
    private void move()
    {
        selected_Tile.GetComponent<HS_Tile>().CurrentUnit = null;

        deselect_Tile();
        select_Tile();
        decolor_Pattern();

        selected_Unit.GetComponent<HS_Unit>().CurrentTile = selected_Tile;
        selected_Tile.GetComponent<HS_Tile>().CurrentUnit = selected_Unit;

        float x = selected_Tile.transform.position.x;
        float z = selected_Tile.transform.position.z;
        selected_Unit.transform.position = new Vector3(x, 1f, z);

        selected_Unit.GetComponent<HS_Unit>().createBoardPosition(board.GetComponent<HS_Board>().Width);

        decreaseTeamIndex();
        set_Unit_Passiv();

        deselect_Tile();
    }
    // <--

    // Controller.fusion()
    // -->
    private void select_Pattern()
    {
        buttons.SetActive(true);
        decolor_Tile();
        int grade = selected_Unit.GetComponent<HS_Unit>().Level - 2;

        two_move_or_attack_pattern = Random.value > 0.5f;
        rnd1 = Random.Range(0, Pattern.GetComponent<HS_Pattern>().All_MP_Lists[grade].Count);
        Choice_1.GetComponentInChildren<Text>().text = Pattern.GetComponent<HS_Pattern>().All_MP_Lists[grade][rnd1].name;
        rnd2 = Random.Range(0, Pattern.GetComponent<HS_Pattern>().All_AP_Lists[grade].Count);
        Choice_2.GetComponentInChildren<Text>().text = Pattern.GetComponent<HS_Pattern>().All_AP_Lists[grade][rnd2].name;

        if (two_move_or_attack_pattern)
        {
            rnd3 = Random.Range(0, Pattern.GetComponent<HS_Pattern>().All_MP_Lists[grade].Count);
            Choice_3.GetComponentInChildren<Text>().text = Pattern.GetComponent<HS_Pattern>().All_MP_Lists[grade][rnd3].name;
        }
        else
        {
            rnd3 = Random.Range(0, Pattern.GetComponent<HS_Pattern>().All_AP_Lists[grade].Count);
            Choice_3.GetComponentInChildren<Text>().text = Pattern.GetComponent<HS_Pattern>().All_AP_Lists[grade][rnd3].name;
        }
    }
    // <--

    // Controller.fusionChoice()
    // -->
    public void choice_1_clicked()
    {
        if (selected_Unit)
        {
            AudioSource.PlayClipAtPoint(levelup_source, selected_Unit.transform.position);
        }
        int grade = selected_Unit.GetComponent<HS_Unit>().Level - 2;
        selected_Unit.GetComponent<HS_Unit>().MovePattern = Pattern.GetComponent<HS_Pattern>().All_MP_Lists[grade][rnd1].pattern;
        buttons.SetActive(false);
        decreaseTeamIndex();
        set_Unit_Passiv();
        deselect_Tile();
    }

    public void choice_2_clicked()
    {
        if (selected_Unit)
        {
            AudioSource.PlayClipAtPoint(levelup_source, selected_Unit.transform.position);
        }
        int grade = selected_Unit.GetComponent<HS_Unit>().Level - 2;
        selected_Unit.GetComponent<HS_Unit>().AttackPattern = Pattern.GetComponent<HS_Pattern>().All_AP_Lists[grade][rnd2].pattern;
        buttons.SetActive(false);
        decreaseTeamIndex();
        set_Unit_Passiv();
        deselect_Tile();
    }

    public void choice_3_clicked()
    {
        if (selected_Unit)
        {
            AudioSource.PlayClipAtPoint(levelup_source, selected_Unit.transform.position);
        }
        int grade = selected_Unit.GetComponent<HS_Unit>().Level - 2;
        if (two_move_or_attack_pattern)
        {
            selected_Unit.GetComponent<HS_Unit>().AttackPattern = Pattern.GetComponent<HS_Pattern>().All_AP_Lists[grade][rnd3].pattern;
        }
        else
        {
            selected_Unit.GetComponent<HS_Unit>().MovePattern = Pattern.GetComponent<HS_Pattern>().All_MP_Lists[grade][rnd3].pattern;
        }
        buttons.SetActive(false);
        decreaseTeamIndex();
        set_Unit_Passiv();
        deselect_Tile();
    }
    // <--

    // Controller.startGame()
    // -->
    public void coinToss_clicked()
    {
        AudioSource.PlayClipAtPoint(button_source, new Vector3(11.44f, 10.1f, 4.44f));
        CoinToss.SetActive(false);
        timer.GetComponentInChildren<HS_Timer>().Cointoss_clicked = true;
        bool cointoss = Random.value > 0.5f;

        if (cointoss)
        {
            board.GetComponent<HS_Board>().set_Team_2_activ();
            board.GetComponent<HS_Board>().set_Team_1_passiv();
            timer.GetComponentInChildren<HS_Timer>().Which_Team = false;
        }
        else
        {
            board.GetComponent<HS_Board>().set_Team_1_activ();
            board.GetComponent<HS_Board>().set_Team_2_passiv();
            timer.GetComponentInChildren<HS_Timer>().Which_Team = true;
        }
    }
    // <--

    // Controller.mainmenu()
    // -->
    public void mainmenu_clicked()
    {
        AudioSource.PlayClipAtPoint(button_source, new Vector3(11.44f, 10.1f, 4.44f));
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }
    // <--

    // Controller.rematch()
    // -->
    public void rematch_clicked()
    {
        AudioSource.PlayClipAtPoint(button_source, new Vector3(11.44f, 10.1f, 4.44f));
        UnityEngine.SceneManagement.SceneManager.LoadScene("SingleplayerBoard");
    }
    // <--

    public void skip_clicked()
    {
        if (!winpanel.activeInHierarchy)
        {
            AudioSource.PlayClipAtPoint(button_source, new Vector3(11.44f, 10.1f, 4.44f));
            timer.GetComponentInChildren<HS_Timer>().roundTime = 0;
        }
    }


    // Controller.move()
    // -->
    private bool moveable()
    {
        bool moveable = false;
        for (int i = 0; i < selected_Unit.GetComponent<HS_Unit>().BoardPosition.GetLength(0); i++)
        {
            for (int j = 0; j < selected_Unit.GetComponent<HS_Unit>().BoardPosition.GetLength(1); j++)
            {
                if (hit_Unit())
                {
                    if (selected_Unit.GetComponent<HS_Unit>().BoardPosition[i, j] == hitInfo.transform.GetComponent<HS_Unit>().CurrentTile.GetComponent<HS_Tile>().Index)
                    {
                        moveable = selected_Unit.GetComponent<HS_Unit>().MovePattern[i, j];
                    }
                }
                if (hit_Tile())
                {
                    if (selected_Unit.GetComponent<HS_Unit>().BoardPosition[i, j] == hitInfo.transform.GetComponent<HS_Tile>().Index)
                    {
                        moveable = selected_Unit.GetComponent<HS_Unit>().MovePattern[i, j];
                    }
                }
            }
        }
        return moveable;
    }
    // <--

    // Controller.attack()
    // -->
    private bool attackable()
    {
        bool attackable = false;
        for (int i = 0; i < selected_Unit.GetComponent<HS_Unit>().BoardPosition.GetLength(0); i++)
        {
            for (int j = 0; j < selected_Unit.GetComponent<HS_Unit>().BoardPosition.GetLength(1); j++)
            {
                if (hit_Unit())
                {
                    if (selected_Unit.GetComponent<HS_Unit>().BoardPosition[i, j] == hitInfo.transform.GetComponent<HS_Unit>().CurrentTile.GetComponent<HS_Tile>().Index)
                    {
                        attackable = selected_Unit.GetComponent<HS_Unit>().AttackPattern[i, j];
                    }
                }
                if (hit_Tile())
                {
                    if (selected_Unit.GetComponent<HS_Unit>().BoardPosition[i, j] == hitInfo.transform.GetComponent<HS_Tile>().Index)
                    {
                        attackable = selected_Unit.GetComponent<HS_Unit>().AttackPattern[i, j];
                    }
                }
            }
        }
        return attackable;
    }
    // <--

    // Controller.selectTile()
    // -->
    private void select_Tile()
    {
        selected_Tile = hitInfo.transform.gameObject;
        color_Tile();
    }

    private void select_Current_Tile()
    {
        selected_Tile = selected_Unit.GetComponent<HS_Unit>().CurrentTile.gameObject;
        color_Tile();
    }
    // <--

    // Controller.deselectAll()
    // -->
    public void deselect_Tile()
    {
        decolor_Tile();
        selected_Tile = null;
    }
    // <--

    // Controller.selectUnit()
    // -->
    private void select_Unit()
    {
        selected_Unit = hitInfo.transform.gameObject;
        color_Unit();
        color_Pattern();
    }

    private void select_Current_Unit()
    {
        selected_Unit = hitInfo.transform.GetComponent<HS_Tile>().CurrentUnit.gameObject;
        color_Unit();
        color_Pattern();
    }
    // <--

    // Controller.deselectAll()
    // -->
    public void deselect_Unit()
    {
        decolor_Pattern();
        decolor_Unit();
        selected_Unit = null;
    }
    // <--


    private void set_Unit_Passiv()
    {
        decolor_Pattern();
        if (selected_Unit.GetComponent<HS_Unit>().Is_Enemy)
        {
            selected_Unit.GetComponent<Renderer>().material = unit_Team_Two_passiv_Material;
        }
        else
        {
            selected_Unit.GetComponent<Renderer>().material = unit_Team_One_passiv_Material;
        }
        selected_Unit.GetComponent<HS_Unit>().Is_activ = false;
        selected_Unit = null;
    }

    private void decreaseTeamIndex()
    {
        if (selected_Unit.GetComponent<HS_Unit>().Is_Enemy)
        {
            board.GetComponent<HS_Board>().decrementTeam2();
        }
        else if (!selected_Unit.GetComponent<HS_Unit>().Is_Enemy)
        {
            board.GetComponent<HS_Board>().decrementTeam1();
        }
    }

    private void unit_Selector()
    {
        if (hit_Unit())
        {
            selected_Unit = hitInfo.transform.gameObject;
        }
        else if (hit_Tile())
        {
            selected_Unit = hitInfo.transform.GetComponent<HS_Tile>().CurrentUnit;
        }
    }


    private void color_Tile()
    {
        selected_Tile.GetComponent<Renderer>().material = selected_Material;
    }

    private void decolor_Tile()
    {
        selected_Tile.GetComponent<Renderer>().material = tile_Material;
    }

    private void color_Unit()
    {
        selected_Unit.GetComponent<Renderer>().material = selected_Material;
    }

    private void decolor_Unit()
    {
        if (selected_Unit.GetComponent<HS_Unit>().Is_Enemy)
        {
            selected_Unit.GetComponent<Renderer>().material = unit_Team_Two_active_Material;
        }
        else
        {
            selected_Unit.GetComponent<Renderer>().material = unit_Team_One_active_Material;
        }
    }

    private void color_Pattern()
    {
        for (int i = 0; i < selected_Unit.GetComponent<HS_Unit>().BoardPosition.GetLength(0); i++)
        {
            for (int j = 0; j < selected_Unit.GetComponent<HS_Unit>().BoardPosition.GetLength(1); j++)
            {
                if (selected_Unit.GetComponent<HS_Unit>().MovePattern[i, j])
                {
                    foreach (GameObject t in board.GetComponent<HS_Board>().TileList)
                    {
                        if (selected_Unit.GetComponent<HS_Unit>().BoardPosition[i, j] == t.GetComponent<HS_Tile>().Index)
                        {
                            t.GetComponent<Renderer>().material = tile_Moveable_Material;
                            break;
                        }
                    }
                }

                if (selected_Unit.GetComponent<HS_Unit>().AttackPattern[i, j])
                {
                    foreach (GameObject t in board.GetComponent<HS_Board>().TileList)
                    {
                        if (selected_Unit.GetComponent<HS_Unit>().BoardPosition[i, j] == t.GetComponent<HS_Tile>().Index)
                        {
                            t.GetComponent<Renderer>().material = tile_Attackable_Material;
                            break;
                        }
                    }
                }

                if (selected_Unit.GetComponent<HS_Unit>().MovePattern[i, j] && selected_Unit.GetComponent<HS_Unit>().AttackPattern[i, j])
                {
                    foreach (GameObject t in board.GetComponent<HS_Board>().TileList)
                    {
                        if (selected_Unit.GetComponent<HS_Unit>().BoardPosition[i, j] == t.GetComponent<HS_Tile>().Index)
                        {
                            t.GetComponent<Renderer>().material = tile_Bothable_Material;
                            break;
                        }
                    }
                }
            }
        }
    }

    private void decolor_Pattern()
    {
        for (int i = 0; i < selected_Unit.GetComponent<HS_Unit>().BoardPosition.GetLength(0); i++)
        {
            for (int j = 0; j < selected_Unit.GetComponent<HS_Unit>().BoardPosition.GetLength(1); j++)
            {
                if (selected_Unit.GetComponent<HS_Unit>().MovePattern[i, j])
                {
                    foreach (GameObject t in board.GetComponent<HS_Board>().TileList)
                    {
                        if (selected_Unit.GetComponent<HS_Unit>().BoardPosition[i, j] == t.GetComponent<HS_Tile>().Index)
                        {
                            t.GetComponent<Renderer>().material = tile_Material;
                            break;
                        }
                    }
                }

                if (selected_Unit.GetComponent<HS_Unit>().AttackPattern[i, j])
                {
                    foreach (GameObject t in board.GetComponent<HS_Board>().TileList)
                    {
                        if (selected_Unit.GetComponent<HS_Unit>().BoardPosition[i, j] == t.GetComponent<HS_Tile>().Index)
                        {
                            t.GetComponent<Renderer>().material = tile_Material;
                            break;
                        }
                    }
                }
            }
        }
    }

    private bool hit_Unit()
    {
        return hitInfo.collider is CapsuleCollider;
    }

    private bool hit_Tile()
    {
        return hitInfo.transform.name == tile_name;
    }

    private bool Unit_is_Enemy()
    {
        return hitInfo.transform.GetComponent<HS_Unit>().Is_Enemy != selected_Unit.GetComponent<HS_Unit>().Is_Enemy;
    }

    private bool Tile_is_Enemy()
    {
        return hitInfo.transform.GetComponent<HS_Tile>().CurrentUnit.GetComponent<HS_Unit>().Is_Enemy != selected_Tile.GetComponent<HS_Tile>().CurrentUnit.GetComponent<HS_Unit>().Is_Enemy;
    }

    private bool target_current_Unit()
    {
        return hitInfo.transform.GetComponent<HS_Tile>().CurrentUnit;
    }

    private bool selected_current_Unit()
    {
        return selected_Tile.GetComponent<HS_Tile>().CurrentUnit;
    }

    private bool Tile_not_Target()
    {
        return selected_Tile != hitInfo.transform.gameObject;
    }

    private bool Unit_not_Target()
    {
        return selected_Unit != hitInfo.transform.gameObject;
    }
}

