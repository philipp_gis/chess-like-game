﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HS_Unit : MonoBehaviour
{
    [SerializeField] private int index;
    [SerializeField] private GameObject currentTile;
    [SerializeField] private bool is_enemy;
    [SerializeField] private bool is_activ = false;
    [SerializeField] private int Lvl = 1;
    private static bool o = false;

    public int[,] BoardPosition { set; get; } = new int[15, 15];

    private bool[,] movePattern = new bool[,] 
    { 
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o}
    };

    private bool[,] attackPattern = new bool[,]
    {
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
        {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o}
    };

    //Bug beheben (Sprung von einer Seite des Boards zur anderen)
    public void createBoardPosition(int boardWidth)
    {
        int tileIndex = currentTile.GetComponent<HS_Tile>().Index;
        int z = BoardPosition.GetLength(0)/2;
        int x = (BoardPosition.GetLength(1) / 2) * -1;
        for(int i = 0; i < BoardPosition.GetLength(0); i++)
        {
            for(int j = 0; j < BoardPosition.GetLength(1); j++)
            {
                if(x > 0 && x > (((tileIndex - 1) / boardWidth + 1) * boardWidth) - tileIndex)
                {
                    BoardPosition[i, j] = 0;
                }
                else if (x < 0 && -1 * x >= tileIndex - (((tileIndex - 1) / boardWidth) * boardWidth))
                {
                    BoardPosition[i, j] = 0;
                }
                else
                {
                    BoardPosition[i, j] = tileIndex + (boardWidth * z) + x;
                }                
                x++;
            }
            x = (BoardPosition.GetLength(1) / 2) * -1;
            z--;
        }
    }

    private bool[,] convertPattern(bool[,] p)
    {
        bool[,] converted_p = new bool[p.GetLength(0),p.GetLength(1)];
        for (int i = 0; i < p.GetLength(0); i++)
        {
            for (int j = 0; j < p.GetLength(1); j++)
            {
                converted_p[i, j] = p[(p.GetLength(0) - (i + 1)), p.GetLength(1) - (j + 1)];
            }
        }
        return converted_p;
    }

    public bool[,] MovePattern
    {
        set
        {
            if (is_enemy)
            {
                movePattern = convertPattern(value);
            }
            else
            {
                movePattern = value;
            }
        }
        get
        {
            return movePattern;
        }
    }

    public bool[,] AttackPattern
    {
        set
        {
            if (is_enemy)
            {
                attackPattern = convertPattern(value);
            }
            else
            {
                attackPattern = value;
            }
        }
        get
        {
            return attackPattern;
        }
    }

    public int Index
    {
        set
        {
            index = value;
        }

        get
        {
            return index;
        }
    }

    public bool Is_Enemy
    {
        get
        {
            return is_enemy;
        }    
        
        set
        {
            is_enemy = value;
        }
    }

    public GameObject CurrentTile
    {
        get
        {
            return currentTile;
        }

        set
        {
            currentTile = value;
        }
    }

    public int Level
    {
        get
        {
            return Lvl;
        }

        set
        {
            Lvl = value;
        }
    }

    public bool Is_activ
    {
        get
        {
            return is_activ;
        }

        set
        {
            is_activ = value;
        }
    }
}
