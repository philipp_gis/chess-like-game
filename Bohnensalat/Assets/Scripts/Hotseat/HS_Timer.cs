﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HS_Timer : MonoBehaviour
{
    [SerializeField] private MonoBehaviour board;
    [SerializeField] private Text Timer_text;
    [SerializeField] private float startTime;
    [SerializeField] private bool cointoss_clicked;
    [SerializeField] private bool which_Team;
    [SerializeField] public float roundTime;
    [SerializeField] private int roundCounter = 1;

    private void Start()
    {
        cointoss_clicked = false;
        roundTime = startTime;
    }

    private void Update()
    {
        // Controller.startGame()
        // -->
        if (cointoss_clicked)
        {
            roundTime -= Time.deltaTime;
            Timer_text.text = "Round: " + (roundCounter).ToString() + "\n" + (roundTime).ToString("f0");

            if (roundTime < 0)
            {
                if (which_Team)
                {
                    board.GetComponent<HS_Board>().set_Team_2_activ();
                    board.GetComponent<HS_Board>().set_Team_1_passiv();
                    this.which_Team = false;
                }
                else
                {
                    board.GetComponent<HS_Board>().set_Team_1_activ();
                    board.GetComponent<HS_Board>().set_Team_2_passiv();
                    this.which_Team = true;
                }
                resetTimer();
                roundCounter++;
                board.GetComponent<HS_Board>().updateTilePriority();
                board.GetComponent<HS_Board>().disintegrate();
                board.GetComponent<HS_Board>().updateTilePriority();
            }
        }
        // <--
    }

    public void resetTimer()
    {
        roundTime = startTime;
    }

    public int RoundCounter
    {
        set
        {
            roundCounter = value;
        }
        get
        {
            return roundCounter;
        }
    }
    public bool Cointoss_clicked
    {
        set
        {
            cointoss_clicked = value;
        }
    }

    public bool Which_Team
    {
        set
        {
            which_Team = value;
        }

        get
        {
            return which_Team;
        }
    }
}
