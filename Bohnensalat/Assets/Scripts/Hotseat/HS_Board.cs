﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HS_Board : MonoBehaviour
{
    [SerializeField] private int width;
    [SerializeField] private int height;
    [SerializeField] private GameObject Tile;
    [SerializeField] private GameObject Unit;
    [SerializeField] private GameObject Pattern;
    [SerializeField] private GameObject timer;
    [SerializeField] private GameObject winpanel;
    [SerializeField] private GameObject controller;
    [SerializeField] private Camera cam;

    [SerializeField] private Text wintext;

    [SerializeField] private List<GameObject> tileList = new List<GameObject>();
    [SerializeField] private List<GameObject> UnitList = new List<GameObject>();
    public List<GameObject> Team1List = new List<GameObject>();
    public List<GameObject> Team2List = new List<GameObject>();

    private int indexCounter = 1;
    private int unitIndex = 1;
    [SerializeField] private int team1Index;
    [SerializeField] private int team2Index;

    [SerializeField] private Material unit_Team_One_active_Material;
    [SerializeField] private Material unit_Team_Two_active_Material;
    [SerializeField] private Material unit_Team_One_passiv_Material;
    [SerializeField] private Material unit_Team_Two_passiv_Material;

    [SerializeField] private AudioSource tile_destruction_source;

    private void Start()
    {
        // Board.create() 
        // -->
        unit_Team_One_active_Material = (Material)Resources.Load("Material/Unit_Team_1_active_Material", typeof(Material));
        unit_Team_Two_active_Material = (Material)Resources.Load("Material/Unit_Team_2_active_Material", typeof(Material));
        unit_Team_One_passiv_Material = (Material)Resources.Load("Material/Unit_Team_2_passiv_Material", typeof(Material));
        unit_Team_Two_passiv_Material = (Material)Resources.Load("Material/Unit_Team_1_passiv_Material", typeof(Material));

        Tile = (GameObject)Resources.Load("Prefab/Tile", typeof(GameObject));
        Unit = (GameObject)Resources.Load("Prefab/Unit", typeof(GameObject));
        for (int z = 0; z < height; z++)
        {
            for (int x = 0; x < width; x++)
            {
                Tile = Instantiate((GameObject)Resources.Load("Prefab/Tile", typeof(GameObject)), new Vector3(x, 0, z), Quaternion.identity) as GameObject;
                Tile.GetComponent<HS_Tile>().Index = indexCounter;
                TileList.Add(Tile);
                if (z == 0)
                {
                    Unit = Instantiate((GameObject)Resources.Load("Prefab/Unit", typeof(GameObject)), new Vector3(x, 1, z), Quaternion.identity);

                    Unit.GetComponent<Renderer>().material = unit_Team_One_active_Material;

                    Unit.GetComponent<HS_Unit>().Index = unitIndex;
                    Unit.GetComponent<HS_Unit>().Is_Enemy = false;
                    Unit.GetComponent<HS_Unit>().CurrentTile = Tile;
                    Unit.GetComponent<HS_Unit>().MovePattern = Pattern.GetComponent<HS_Pattern>().AllMP[0].pattern;
                    Unit.GetComponent<HS_Unit>().AttackPattern = Pattern.GetComponent<HS_Pattern>().AllAP[0].pattern;
                    Unit.GetComponent<HS_Unit>().createBoardPosition(width);
                    Unit.GetComponentInChildren<Canvas>().worldCamera = cam;
                    Tile.GetComponent<HS_Tile>().CurrentUnit = Unit;
                    Unit.name = "T1_" + unitIndex;
                    UnitList.Add(Unit);
                    Team1List.Add(Unit);
                    ++unitIndex;
                }
                if (z == height - 1)
                {
                    Unit = Instantiate((GameObject)Resources.Load("Prefab/Unit", typeof(GameObject)), new Vector3(x, 1, z), Quaternion.identity);
                    Unit.GetComponent<Renderer>().material = unit_Team_Two_active_Material;

                    Unit.GetComponent<HS_Unit>().Index = unitIndex;
                    Unit.GetComponent<HS_Unit>().Is_Enemy = true;
                    Unit.GetComponent<HS_Unit>().CurrentTile = Tile;
                    Unit.GetComponent<HS_Unit>().MovePattern = Pattern.GetComponent<HS_Pattern>().AllMP[0].pattern;
                    Unit.GetComponent<HS_Unit>().AttackPattern = Pattern.GetComponent<HS_Pattern>().AllAP[0].pattern;
                    Unit.GetComponent<HS_Unit>().createBoardPosition(width);
                    Tile.GetComponent<HS_Tile>().CurrentUnit = Unit;
                    Unit.name = "T2_" + unitIndex;
                    UnitList.Add(Unit);
                    Team2List.Add(Unit);
                    ++unitIndex;
                }
                ++indexCounter;
                Tile.GetComponent<HS_Tile>().createBoardPosition(width);
            }
        }
        Tile = Instantiate((GameObject)Resources.Load("Prefab/Tile", typeof(GameObject))) as GameObject;
        Tile.GetComponent<Renderer>().enabled = false;
        updateTilePriority();
        // <--

        team1Index = Team1List.Count;
        team2Index = Team2List.Count;

    }

    private void Update()
    {
        if (Input.GetKeyDown("c"))
        {
            foreach (GameObject u in UnitList)
            {
                Destroy(u);
            }
            foreach (GameObject t in TileList)
            {
                Destroy(t);
            }
        }

        if (Input.GetKeyDown("r"))
        {
            Start();
        }

        // Board.teamActivityChecker()
        // -->
        if (team1Index <= 0)
        {
            team1Index = Team1List.Count;
            set_Team_2_activ();
            set_Team_1_passiv();
            timer.GetComponentInChildren<HS_Timer>().Which_Team = false;
            timer.GetComponentInChildren<HS_Timer>().resetTimer();
            timer.GetComponentInChildren<HS_Timer>().RoundCounter++;
            updateTilePriority();
            disintegrate();
            updateTilePriority();
        }

        if (team2Index <= 0)
        {
            team2Index = Team2List.Count;
            set_Team_1_activ();
            set_Team_2_passiv();
            timer.GetComponentInChildren<HS_Timer>().Which_Team = true;
            timer.GetComponentInChildren<HS_Timer>().resetTimer();
            timer.GetComponentInChildren<HS_Timer>().RoundCounter++;
            updateTilePriority();
            disintegrate();
            updateTilePriority();
        }

        if (Team1List.Count == 0)
        {
            winpanel.SetActive(true);
            wintext.text = "Team 2 hat gewonnen!";
        }

        if (Team2List.Count == 0)
        {
            winpanel.SetActive(true);
            wintext.text = "Team 1 hat gewonnen!";
        }
        // <--
    }

    public void updateTilePriority()
    {
        foreach (GameObject g in tileList)
        {
            if (g.GetComponent<HS_Tile>().Index != 0)
            {
                if (g.GetComponent<HS_Tile>().CurrentUnit)
                {
                    g.GetComponent<HS_Tile>().Priority = 1;
                }
                else
                {
                    g.GetComponent<HS_Tile>().Priority = 4;
                }
            }
        }
        foreach (GameObject g in tileList)
        {
            if (g.GetComponent<HS_Tile>().Priority != 1 && g.GetComponent<HS_Tile>().Index != 0)
            {
                int borderCheck = 0;
                for (int i = 0; i < g.GetComponent<HS_Tile>().BoardPosition.GetLength(0); i++)
                {
                    for (int j = 0; j < g.GetComponent<HS_Tile>().BoardPosition.GetLength(1); j++)
                    {
                        if (g.GetComponent<HS_Tile>().BoardPosition[i, j] != 0)
                        {
                            GameObject h = tileList[g.GetComponent<HS_Tile>().BoardPosition[i, j] - 1];
                            int prioH = h.GetComponent<HS_Tile>().Priority;
                            int prioG = g.GetComponent<HS_Tile>().Priority;
                            if (prioH < prioG)
                            {
                                switch (prioH)
                                {
                                    case 1:
                                        g.GetComponent<HS_Tile>().Priority = 2;
                                        break;
                                    case 2:
                                        g.GetComponent<HS_Tile>().Priority = 3;
                                        break;
                                    case 3:
                                        g.GetComponent<HS_Tile>().Priority = 4;
                                        break;
                                    case 4:
                                        g.GetComponent<HS_Tile>().Priority = 4;
                                        break;
                                }
                            }
                            if (borderCheck > 0 && g.GetComponent<HS_Tile>().Priority == 4)
                            {
                                g.GetComponent<HS_Tile>().Priority = 5;
                            }
                            if (borderCheck > 3 && g.GetComponent<HS_Tile>().Priority == 4)
                            {
                                g.GetComponent<HS_Tile>().Priority = 6;
                            }
                        }
                        if (g.GetComponent<HS_Tile>().BoardPosition[i, j] == 0 || tileList[g.GetComponent<HS_Tile>().BoardPosition[i, j] - 1].GetComponent<HS_Tile>().Index == 0)
                        {
                            borderCheck++;
                        }
                    }
                }
            }
        }
        for (int k = tileList.Count - 1; k >= 0; k--)
        {
            GameObject g = tileList[k];
            if (g.GetComponent<HS_Tile>().Priority != 1 && g.GetComponent<HS_Tile>().Index != 0)
            {
                int borderCheck = 0;
                for (int i = g.GetComponent<HS_Tile>().BoardPosition.GetLength(0) - 1; i >= 0; i--)
                {
                    for (int j = g.GetComponent<HS_Tile>().BoardPosition.GetLength(1) - 1; j >= 0; j--)
                    {
                        if (g.GetComponent<HS_Tile>().BoardPosition[i, j] != 0)
                        {
                            GameObject h = tileList[g.GetComponent<HS_Tile>().BoardPosition[i, j] - 1];
                            int prioH = h.GetComponent<HS_Tile>().Priority;
                            int prioG = g.GetComponent<HS_Tile>().Priority;
                            if (prioH < prioG)
                            {
                                switch (prioH)
                                {
                                    case 1:
                                        g.GetComponent<HS_Tile>().Priority = 2;
                                        break;
                                    case 2:
                                        g.GetComponent<HS_Tile>().Priority = 3;
                                        break;
                                    case 3:
                                        g.GetComponent<HS_Tile>().Priority = 4;
                                        break;
                                    case 4:
                                        g.GetComponent<HS_Tile>().Priority = 4;
                                        break;
                                }
                            }
                        }
                        if (g.GetComponent<HS_Tile>().BoardPosition[i, j] == 0 || tileList[g.GetComponent<HS_Tile>().BoardPosition[i, j] - 1].GetComponent<HS_Tile>().Index == 0)
                        {
                            borderCheck++;
                        }
                    }
                }
                if (borderCheck > 0 && g.GetComponent<HS_Tile>().Priority == 4)
                {
                    g.GetComponent<HS_Tile>().Priority = 5;
                }
                if (borderCheck > 3 && g.GetComponent<HS_Tile>().Priority >= 4)
                {
                    g.GetComponent<HS_Tile>().Priority = 6;
                }
            }
        }
    }

    public void disintegrate()
    {
        tile_destruction_source.Play();
        int maxToDestroy = 2;
        int destroyed = 0;
        List<GameObject> prio6Tiles = new List<GameObject>();
        List<GameObject> prio5Tiles = new List<GameObject>();
        List<GameObject> prio4Tiles = new List<GameObject>();
        List<GameObject> prio3Tiles = new List<GameObject>();
        List<GameObject> prio2Tiles = new List<GameObject>();

        foreach (GameObject g in tileList)
        {
            switch (g.GetComponent<HS_Tile>().Priority)
            {
                case 6:
                    prio6Tiles.Add(g);
                    break;
                case 5:
                    prio5Tiles.Add(g);
                    break;
                case 4:
                    prio4Tiles.Add(g);
                    break;
                case 3:
                    prio3Tiles.Add(g);
                    break;
                case 2:
                    prio2Tiles.Add(g);
                    break;
            }
        }

        int i = 0;
        while (prio6Tiles.Count > 0 && destroyed < maxToDestroy)
        {
            GameObject g = prio6Tiles[i];
            if (Random.value <= 0.20f)
            {
                tileList[tileList.IndexOf(g)] = Instantiate(Tile);
                prio6Tiles.Remove(g);
                Destroy(g);
                destroyed++;
            }
            if (prio6Tiles.Count > 0)
            {
                i = (i + 1) % prio6Tiles.Count;
            }
        }
        i = 0;
        while (prio5Tiles.Count > 0 && destroyed < maxToDestroy)
        {
            GameObject g = prio5Tiles[i];
            if (Random.value <= 0.20f)
            {
                tileList[tileList.IndexOf(g)] = Instantiate(Tile);
                prio5Tiles.Remove(g);
                Destroy(g);
                destroyed++;
            }
            if (prio5Tiles.Count > 0)
            {
                i = (i + 1) % prio5Tiles.Count;
            }
        }
        i = 0;
        while (prio4Tiles.Count > 0 && destroyed < maxToDestroy)
        {
            GameObject g = prio4Tiles[i];
            if (Random.value <= 0.20f)
            {
                tileList[tileList.IndexOf(g)] = Instantiate(Tile);
                prio4Tiles.Remove(g);
                Destroy(g);
                destroyed++;
            }
            if (prio4Tiles.Count > 0)
            {
                i = (i + 1) % prio4Tiles.Count;
            }
        }
        i = 0;
        while (prio3Tiles.Count > 0 && destroyed < maxToDestroy)
        {
            GameObject g = prio3Tiles[i];
            if (Random.value <= 0.20f)
            {
                tileList[tileList.IndexOf(g)] = Instantiate(Tile);
                prio3Tiles.Remove(g);
                Destroy(g);
                destroyed++;
            }
            if (prio3Tiles.Count > 0)
            {
                i = (i + 1) % prio3Tiles.Count;
            }
        }
        i = 0;
        while (prio2Tiles.Count > 0 && destroyed < maxToDestroy)
        {
            GameObject g = prio2Tiles[i];
            if (Random.value <= 0.20f)
            {
                tileList[tileList.IndexOf(g)] = Instantiate(Tile);
                prio2Tiles.Remove(g);
                Destroy(g);
                destroyed++;
            }
            if (prio2Tiles.Count > 0)
            {
                i = (i + 1) % prio2Tiles.Count;
            }
        }




        Debug.Log(destroyed);
        Debug.Log("----------Finished Method------------------");

    }

    public void set_Team_1_passiv()
    {
        if (Team1List.Count > 0)
        {
            if (controller.GetComponent<HS_Controller>().buttons.activeInHierarchy)
            {
                controller.GetComponent<HS_Controller>().choice_1_clicked();
            }

            if (controller.GetComponent<HS_Controller>().selected_Unit)
            {
                controller.GetComponent<HS_Controller>().deselect_Unit();
            }

            if (controller.GetComponent<HS_Controller>().selected_Tile)
            {
                controller.GetComponent<HS_Controller>().deselect_Tile();
            }
            
            foreach (GameObject u in Team1List)
            {
                u.GetComponent<Renderer>().material = unit_Team_One_passiv_Material;
                u.GetComponent<HS_Unit>().Is_activ = false;
            }

           
            team1Index = Team1List.Count;
            team2Index = Team2List.Count;
        }
    }

    public void set_Team_2_passiv()
    {
        if (Team1List.Count > 0)
        {
            if (controller.GetComponent<HS_Controller>().buttons.activeInHierarchy)
            {
                controller.GetComponent<HS_Controller>().choice_1_clicked();
            }
            
            if (controller.GetComponent<HS_Controller>().selected_Unit)
            {
                controller.GetComponent<HS_Controller>().deselect_Unit();
            }

            if (controller.GetComponent<HS_Controller>().selected_Tile)
            {
                controller.GetComponent<HS_Controller>().deselect_Tile();
            }

            foreach (GameObject u in Team2List)
            {
                u.GetComponent<Renderer>().material = unit_Team_Two_passiv_Material;
                u.GetComponent<HS_Unit>().Is_activ = false;
            }


            team1Index = Team1List.Count;
            team2Index = Team2List.Count;
        }
    }

    public void set_Team_1_activ()
    {
        if (Team1List.Count != 0)
        {
            foreach (GameObject u in Team1List)
            {
                u.GetComponent<Renderer>().material = unit_Team_One_active_Material;
                u.GetComponent<HS_Unit>().Is_activ = true;
            }

            team1Index = Team1List.Count;
            team2Index = Team2List.Count;
        }
    }

    public void set_Team_2_activ()
    {
        if (Team1List.Count > 0)
        {
            foreach (GameObject u in Team2List)
            {
                u.GetComponent<Renderer>().material = unit_Team_Two_active_Material;
                u.GetComponent<HS_Unit>().Is_activ = true;
            }

            team1Index = Team1List.Count;
            team2Index = Team2List.Count;
        }
    }

    public void decrementTeam1()
    {
        team1Index--;
    }

    public void decrementTeam2()
    {
        team2Index--;
    }

    public int Width
    {
        get
        {
            return width;
        }
    }

    public int Height
    {
        get
        {
            return height;
        }
    }

    public List<GameObject> TileList
    {
        get
        {
            return tileList;
        }
    }
}