﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HS_Tile : MonoBehaviour
{
    [SerializeField] private int index = 0;
    [SerializeField] private int priority = 0;
    [SerializeField] private GameObject currentUnit;
    private int[,] boardPosition = new int[3,3];

    public void createBoardPosition(int boardWidth)
    {
        int z = boardPosition.GetLength(0) / 2;
        int x = (boardPosition.GetLength(1) / 2) * -1;
        for (int i = 0; i < boardPosition.GetLength(0); i++)
        {
            for (int j = 0; j < boardPosition.GetLength(1); j++)
            {
                if (x > 0 && x > (((index - 1) / boardWidth + 1) * boardWidth) - index)
                {
                    boardPosition[i, j] = 0;
                }
                else if (x < 0 && -1 * x >= index - (((index - 1) / boardWidth) * boardWidth))
                {
                    boardPosition[i, j] = 0;
                }
                else if(index + (boardWidth * z) + x < 0 || index + (boardWidth * z) + x > 100)
                {
                    boardPosition[i, j] = 0;
                }
                else
                {
                    boardPosition[i, j] = index + (boardWidth * z) + x;
                }
                x++;
            }
            x = (boardPosition.GetLength(1) / 2) * -1;
            z--;
        }
    }

    public int[,] BoardPosition
    {
        set
        {
            boardPosition = value;
        }
        get
        {
            return boardPosition;
        }
    }

    public int Priority
    {
        set
        {
            priority = value;
        }
        get
        {
            return priority;
        }
    }
    public int Index
    {
        set
        {
            index = value;
        }
        get
        {
            return index;
        }
    }

    public GameObject CurrentUnit
    {
        get
        {
            return currentUnit;
        }

        set
        {
            currentUnit = value;
        }
    }
}
