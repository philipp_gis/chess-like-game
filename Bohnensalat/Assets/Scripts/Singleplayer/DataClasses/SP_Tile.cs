﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using static SP_Enum.tileReachability;

public class SP_Tile : MonoBehaviour
{
    // Model der Felder

    #region Header

    [Header("Attributes")]
    
    [SerializeField] private GameObject tileCurrentUnit;
    [SerializeField] private SP_Enum.tileReachability type;
    [SerializeField] private int priority;

    void Awake()
    {
        // Initialisierung des Typ der Einheit
        type = DEFAULT;
        priority = 0;
    }

    #endregion



    #region Getter Setter

    public GameObject CurrentUnit { get => tileCurrentUnit; set => tileCurrentUnit = value; }
    public SP_Enum.tileReachability Type { get => type; set => type = value; }
    public int Priority { get => priority; set => priority = value; }

    #endregion
}