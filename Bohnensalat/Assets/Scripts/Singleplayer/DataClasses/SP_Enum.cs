﻿using UnityEngine;

public class SP_Enum : MonoBehaviour
{
    // Verwaltung aller Enumerations

    #region Header
    // Spielzustände
    public enum  gameState { UNITY_STATE, SELECTION_STATE, ACTION_STATE };

    // Teamzugehörigkeit
    public enum team { TEAM_RED, TEAM_BLUE, NOT_SELECTED }

    // Feldabfrage für Muster
    public enum tileReachability { DEFAULT, ATTACKABLE, MOVEABLE, BOTHABLE, FUSIONABLE };

    // Aktionen der der Spieler ausführen kann
    public enum action { MOVE, POST_MOVE_WORK, ATTACK, POST_ATTACK_WORK, FUSION, POST_FUSION_WORK, NOT_IN_ACTION }

    // Objekte die der Spieler auswählen kann
    public enum selection { ENEMY, ALLY, NOTHING_SELECTED }

    // Objekte über der Spieler hovert
    public enum hover { EMPTY_TILE, TILE_WITH_UNIT, UNIT, NOTHING }

    // Objekte die der Spieler auswählen kann
    public enum isAnimation { IN_ANIMATION, OUT_OF_ANIMATION }
    #endregion
}