﻿using System.Collections.Generic;
using UnityEngine;


//using static SP_Enum.unitTypes;

public class SP_Unit : MonoBehaviour
{
    // Model der Einheiten

    #region Header

    [Header("Attributes")]

    [SerializeField] private SP_Enum.team team;             // Teamzugehörigkeit

    [SerializeField] private int currentLevel;              // Level der Einheit
    [SerializeField] private GameObject unitCurrentTile;    // Zur Einheit gehörige Feld

    [SerializeField] private bool isActiv;                  // Ist die Einheit aktiv oder passiv

    [SerializeField] private List<int[]> movePattern;       // Bewegungsmuster
    [SerializeField] private string movePatternName;        // Name des Bewegungsmusters

    [SerializeField] private List<int[]> attackPattern;     // Angriffsmuster
    [SerializeField] private string attackPatternName;      // Name des Angriffsmusters

    [SerializeField] private List<GameObject> actionList;   // Angriffsmuster

    private Animator unitAnimator;                          // Animator der Einheit

    void Awake()
    {
        // Initialisierung des Typ der Einheit
        currentLevel = 1;
        actionList = new List<GameObject> { };
    }

    #endregion



    #region Getter Setter

    public int CurrentLevel { get => currentLevel; set => currentLevel = value; }
    public bool IsActiv { get => isActiv; set => isActiv = value; }
    public GameObject CurrentTile { get => unitCurrentTile; set => unitCurrentTile = value; }
    public List<int[]> MovePattern { get => movePattern; set => movePattern = value; }
    public List<int[]> AttackPattern { get => attackPattern; set => attackPattern = value; }
    public Animator Animator { get => unitAnimator; set => unitAnimator = value; }
    public string MovePatternName { get => movePatternName; set => movePatternName = value; }
    public string AttackPatternName { get => attackPatternName; set => attackPatternName = value; }
    public SP_Enum.team Team { get => team; set => team = value; }
    public List<GameObject> ActionList { get => actionList; set => actionList = value; }

    #endregion
}