﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SP_Image : MonoBehaviour
{
    // Verwaltung von Bildern

    #region Header

    SP_Lists lists;

    Sprite apBasic;
    Sprite apBlock2;
    Sprite apBlockKreuz3;
    Sprite apBlockKreuz4;
    Sprite apBlockPlus3;
    Sprite apBlockPlus4;
    Sprite apKreuz1;
    Sprite apKreuz2;
    Sprite apKreuz3;
    Sprite apKreuz4;
    Sprite apPlus1;
    Sprite apPlus2;
    Sprite apPlus3;
    Sprite apPlus4;

    Sprite mpBasic;
    Sprite mpBlock2;
    Sprite mpBlockKreuz3;
    Sprite mpBlockKreuz4;
    Sprite mpBlockPlus3;
    Sprite mpBlockPlus4;
    Sprite mpKreuz1;
    Sprite mpKreuz2;
    Sprite mpKreuz3;
    Sprite mpKreuz4;
    Sprite mpPlus2;
    Sprite mpPlus3;
    Sprite mpPlus4;

    Sprite moveIcon;
    Sprite attackIcon;
    Sprite fusionIcon;

    private void Awake()
    {
        apBasic = Resources.Load<Sprite>("Images/Ap_Basic");
        apBlock2 = Resources.Load<Sprite>("Images/Ap_Block2");
        apBlockKreuz3 = Resources.Load<Sprite>("Images/Ap_BlockKreuz3");
        apBlockKreuz4 = Resources.Load<Sprite>("Images/Ap_BlockKreuz4");
        apBlockPlus3 = Resources.Load<Sprite>("Images/Ap_BlockPlus3");
        apBlockPlus4 = Resources.Load<Sprite>("Images/Ap_BlockPlus4");
        apKreuz1 = Resources.Load<Sprite>("Images/Ap_Kreuz1");
        apKreuz2 = Resources.Load<Sprite>("Images/Ap_Kreuz2");
        apKreuz3 = Resources.Load<Sprite>("Images/Ap_Kreuz3");
        apKreuz4 = Resources.Load<Sprite>("Images/Ap_Kreuz4");
        apPlus1 = Resources.Load<Sprite>("Images/Ap_Plus1");
        apPlus2 = Resources.Load<Sprite>("Images/Ap_Plus2");
        apPlus3 = Resources.Load<Sprite>("Images/Ap_Plus3");
        apPlus4 = Resources.Load<Sprite>("Images/Ap_Plus4");

        mpBasic = Resources.Load<Sprite>("Images/Mp_Basic");
        mpBlock2 = Resources.Load<Sprite>("Images/Mp_Block2");
        mpBlockKreuz3 = Resources.Load<Sprite>("Images/Mp_BlockKreuz3");
        mpBlockKreuz4 = Resources.Load<Sprite>("Images/Mp_BlockKreuz4");
        mpBlockPlus3 = Resources.Load<Sprite>("Images/Mp_BlockPlus3");
        mpBlockPlus4 = Resources.Load<Sprite>("Images/Mp_BlockPlus4");
        mpKreuz1 = Resources.Load<Sprite>("Images/Mp_Kreuz1");
        mpKreuz2 = Resources.Load<Sprite>("Images/Mp_Kreuz2");
        mpKreuz3 = Resources.Load<Sprite>("Images/Mp_Kreuz3");
        mpKreuz4 = Resources.Load<Sprite>("Images/Mp_Kreuz4");
        mpPlus2 = Resources.Load<Sprite>("Images/Mp_Plus2");
        mpPlus3 = Resources.Load<Sprite>("Images/Mp_Plus3");
        mpPlus4 = Resources.Load<Sprite>("Images/Mp_Plus4");

        moveIcon = Resources.Load<Sprite>("Images/MoveIcon");
        attackIcon = Resources.Load<Sprite>("Images/AttackIcon");
        fusionIcon = Resources.Load<Sprite>("Images/FusionIcon");

        lists = GetComponent<SP_Lists>();

        lists.MovepatternImageList = new List<(string, Sprite)> { };
        lists.AttackpatternImageList = new List<(string, Sprite)> { };

        lists.MovepatternImageList.Add(("Basic MP", mpBasic));
        lists.MovepatternImageList.Add(("Block 2", mpBlock2));
        lists.MovepatternImageList.Add(("Block-Kreuz 3", mpBlockKreuz3));
        lists.MovepatternImageList.Add(("Block-Kreuz 4", mpBlockKreuz4));
        lists.MovepatternImageList.Add(("Block-Plus 3", mpBlockPlus3));
        lists.MovepatternImageList.Add(("Block-Plus 4", mpBlockPlus4));
        lists.MovepatternImageList.Add(("Kreuz 1", mpKreuz1));
        lists.MovepatternImageList.Add(("Kreuz 2", mpKreuz2));
        lists.MovepatternImageList.Add(("Kreuz 3", mpKreuz3));
        lists.MovepatternImageList.Add(("Kreuz 4", mpKreuz4));
        lists.MovepatternImageList.Add(("Plus 2", mpPlus2));
        lists.MovepatternImageList.Add(("Plus 3", mpPlus3));
        lists.MovepatternImageList.Add(("Plus 4", mpPlus4));

        lists.AttackpatternImageList.Add(("Basic AP", apBasic));
        lists.AttackpatternImageList.Add(("Block 2", apBlock2));
        lists.AttackpatternImageList.Add(("Block-Kreuz 3", apBlockKreuz3));
        lists.AttackpatternImageList.Add(("Block-Kreuz 4", apBlockKreuz4));
        lists.AttackpatternImageList.Add(("Block-Plus 3", apBlockPlus3));
        lists.AttackpatternImageList.Add(("Block-Plus 4", apBlockPlus4));
        lists.AttackpatternImageList.Add(("Kreuz 1", apKreuz1));
        lists.AttackpatternImageList.Add(("Kreuz 2", apKreuz2));
        lists.AttackpatternImageList.Add(("Kreuz 3", apKreuz3));
        lists.AttackpatternImageList.Add(("Kreuz 4", apKreuz4));
        lists.AttackpatternImageList.Add(("Plus 1", apPlus1));
        lists.AttackpatternImageList.Add(("Plus 2", apPlus2));
        lists.AttackpatternImageList.Add(("Plus 3", apPlus3));
        lists.AttackpatternImageList.Add(("Plus 4", apPlus4));
    }

    #endregion



    #region Getter Setter

    public Sprite ApBasic { get => apBasic; set => apBasic = value; }
    public Sprite ApBlock2 { get => apBlock2; set => apBlock2 = value; }
    public Sprite ApBlockKreuz3 { get => apBlockKreuz3; set => apBlockKreuz3 = value; }
    public Sprite ApBlockKreuz4 { get => apBlockKreuz4; set => apBlockKreuz4 = value; }
    public Sprite ApBlockPlus3 { get => apBlockPlus3; set => apBlockPlus3 = value; }
    public Sprite ApBlockPlus4 { get => apBlockPlus4; set => apBlockPlus4 = value; }
    public Sprite ApKreuz1 { get => apKreuz1; set => apKreuz1 = value; }
    public Sprite ApKreuz2 { get => apKreuz2; set => apKreuz2 = value; }
    public Sprite ApKreuz3 { get => apKreuz3; set => apKreuz3 = value; }
    public Sprite ApKreuz4 { get => apKreuz4; set => apKreuz4 = value; }
    public Sprite ApPlus1 { get => apPlus1; set => apPlus1 = value; }
    public Sprite ApPlus2 { get => apPlus2; set => apPlus2 = value; }
    public Sprite ApPlus3 { get => apPlus3; set => apPlus3 = value; }
    public Sprite ApPlus4 { get => apPlus4; set => apPlus4 = value; }
    public Sprite MpBasic { get => mpBasic; set => mpBasic = value; }
    public Sprite MpBlock2 { get => mpBlock2; set => mpBlock2 = value; }
    public Sprite MpBlockKreuz3 { get => mpBlockKreuz3; set => mpBlockKreuz3 = value; }
    public Sprite MpBlockKreuz4 { get => mpBlockKreuz4; set => mpBlockKreuz4 = value; }
    public Sprite MpBlockPlus3 { get => mpBlockPlus3; set => mpBlockPlus3 = value; }
    public Sprite MpBlockPlus4 { get => mpBlockPlus4; set => mpBlockPlus4 = value; }
    public Sprite MpKreuz1 { get => mpKreuz1; set => mpKreuz1 = value; }
    public Sprite MpKreuz2 { get => mpKreuz2; set => mpKreuz2 = value; }
    public Sprite MpKreuz3 { get => mpKreuz3; set => mpKreuz3 = value; }
    public Sprite MpKreuz4 { get => mpKreuz4; set => mpKreuz4 = value; }
    public Sprite MpPlus2 { get => mpPlus2; set => mpPlus2 = value; }
    public Sprite MpPlus3 { get => mpPlus3; set => mpPlus3 = value; }
    public Sprite MpPlus4 { get => mpPlus4; set => mpPlus4 = value; }
    public Sprite MoveIcon { get => moveIcon; set => moveIcon = value; }
    public Sprite AttackIcon { get => attackIcon; set => attackIcon = value; }
    public Sprite FusionIcon { get => fusionIcon; set => fusionIcon = value; }

    #endregion
}
