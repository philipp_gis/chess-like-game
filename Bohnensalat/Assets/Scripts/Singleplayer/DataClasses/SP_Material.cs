﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SP_Material : MonoBehaviour
{
    // Verwaltung von Materialien
    
    #region Header

    Material teamFalseActiveMaterial;
    Material teamTrueActiveMaterial;
    Material teamFalsePassivMaterial;
    Material teamTruePassivMaterial;
    Material allySelectedMaterial;
    Material enemySelectedMaterial;
    Material tileDefaultMaterial;
    Material tileAttackMaterial;
    Material tileMoveMaterial;
    Material tileBothableMaterial;

    private void Awake()
    {
        teamTrueActiveMaterial = (Material)Resources.Load("Material/Singleplayer/teamTrueActiveMaterial", typeof(Material));
        teamTruePassivMaterial = (Material)Resources.Load("Material/Singleplayer/teamTruePassivMaterial", typeof(Material));

        teamFalseActiveMaterial = (Material)Resources.Load("Material/Singleplayer/teamFalseActiveMaterial", typeof(Material));
        teamFalsePassivMaterial = (Material)Resources.Load("Material/Singleplayer/teamFalsePassivMaterial", typeof(Material));

        allySelectedMaterial = (Material)Resources.Load("Material/Singleplayer/allySelectedMaterial", typeof(Material));
        enemySelectedMaterial = (Material)Resources.Load("Material/Singleplayer/enemySelectedMaterial", typeof(Material));

        tileDefaultMaterial = (Material)Resources.Load("Material/Singleplayer/tileDefaultMaterial", typeof(Material));
        tileAttackMaterial = (Material)Resources.Load("Material/Singleplayer/tileAttackMaterial", typeof(Material));
        tileMoveMaterial = (Material)Resources.Load("Material/Singleplayer/tileMoveMaterial", typeof(Material));
        tileBothableMaterial = (Material)Resources.Load("Material/Singleplayer/tileBothableMaterial", typeof(Material));
    }

    #endregion

    #region Methoden

    public void SetMaterial(GameObject objectToColor, Material material)
    {
        // Färbt Objekt mit gewünschten Material
        //
        // Erster Parameter: Gameobjekt das gefärbt werden soll
        // Zweiter Paramter: Material das dass Gameobjekt erhalten soll

        objectToColor.GetComponentInChildren<Renderer>().material = material;
    }
   
    #endregion

    #region Getter Setter

    public Material TeamRedActiveMaterial { get => teamFalseActiveMaterial; }
    public Material TeamBlueActiveMaterial { get => teamTrueActiveMaterial; }
    public Material TeamRedPassivMaterial { get => teamFalsePassivMaterial; }
    public Material TeamBluePassivMaterial { get => teamTruePassivMaterial; }
    public Material AllySelectedMaterial { get => allySelectedMaterial; }
    public Material EnemySelectedMaterial { get => enemySelectedMaterial; }
    public Material TileDefaultMaterial { get => tileDefaultMaterial; }
    public Material TileAttackMaterial { get => tileAttackMaterial; }
    public Material TileMoveMaterial { get => tileMoveMaterial; set => tileMoveMaterial = value; }
    public Material TileBothableMaterial { get => tileBothableMaterial; set => tileBothableMaterial = value; }

    #endregion
}