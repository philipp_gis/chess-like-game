﻿using UnityEngine;
using UnityEngine.UI;
using static SP_Enum.gameState;
using static SP_Enum.tileReachability;
using static SP_Enum.action;
using static SP_Enum.team;
using System.Collections.Generic;
using System;

public class SP_Hover : MonoBehaviour
{
    // Shortcuts
    private SP_Material material;
    private SP_Lists lists;
    private SP_Controller controller;
    private SP_Selection selection;
    private SP_Image image;

    // Raycasting
    public RaycastHit HitInfo;
    [SerializeField] private bool hit;

    // Old New Check Variablen
    public GameObject oldHover;
    public GameObject newHover;

    // Referenzen auf Hoverinfo
    public Text unitName;
    public Text unitLevel;
    public GameObject mpFrame;
    public GameObject mpImage;
    public GameObject apFrame;
    public GameObject apImage;

    public GameObject PlayerActionPanel;
    public GameObject ActionImage;

    List<GameObject> enemyAttackTiles = new List<GameObject> { };

    private void Awake()
    {
        material = GetComponent<SP_Material>();
        lists = GetComponent<SP_Lists>();
        controller = GetComponent<SP_Controller>();
        selection = GetComponent<SP_Selection>();
        image = GetComponent<SP_Image>();

        unitName.text = "";
        unitLevel.text = "";
        mpFrame.SetActive(false);
        mpImage.SetActive(false);
        apFrame.SetActive(false);
        apImage.SetActive(false);
    }

    public void HoverSelection()
    {
        // Verwaltet was passiert wenn man über Einheiten und Felder hovert

        // Raycast um festzustellen über was der Spieler hovert
        HitInfo = new RaycastHit();
        hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out HitInfo);

        if (controller.CurrentTeam == controller.PlayerTeam && controller.CurrentGameState == SELECTION_STATE)
        {
            PlayerActionPanel.SetActive(false);
            ActionImage.SetActive(false);

            // Färbt Objekte über die der Spieler hovert
            if (HitInfo.collider is BoxCollider)
            {
                if (!HitInfo.transform.GetComponent<SP_Tile>().CurrentUnit)
                {
                    // Feld ohne Einheit
                    material.SetMaterial(HitInfo.transform.gameObject, material.AllySelectedMaterial);
                }

                if (HitInfo.transform.GetComponent<SP_Tile>().CurrentUnit)
                {
                    // Feld mit Einheit
                    material.SetMaterial(HitInfo.transform.gameObject, material.AllySelectedMaterial);

                    unitName.text = HitInfo.transform.gameObject.GetComponent<SP_Tile>().CurrentUnit.GetComponent<SP_Unit>().transform.parent.name;
                    unitLevel.text = "" + HitInfo.transform.gameObject.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().CurrentLevel;

                    apImage.SetActive(true);
                    mpImage.SetActive(true);
                    mpFrame.SetActive(true);
                    apFrame.SetActive(true);

                    foreach ((string, Sprite) i in lists.MovepatternImageList)
                    {
                        if (HitInfo.transform.gameObject.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().MovePatternName == i.Item1)
                        {
                            mpImage.GetComponentInChildren<Image>().sprite = i.Item2;
                            break;
                        }
                    }

                    foreach ((string, Sprite) i in lists.AttackpatternImageList)
                    {
                        if (HitInfo.transform.gameObject.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().AttackPatternName == i.Item1)
                        {
                            apImage.GetComponentInChildren<Image>().sprite = i.Item2;
                            break;
                        }
                    }
                }
            }

            else if (HitInfo.collider is CapsuleCollider)
            {
                // Einheit
                material.SetMaterial(HitInfo.transform.gameObject.GetComponentInChildren<SP_Unit>().CurrentTile, material.AllySelectedMaterial);

                unitName.text = HitInfo.transform.parent.name;
                unitLevel.text = "" + HitInfo.transform.gameObject.GetComponentInChildren<SP_Unit>().CurrentLevel;

                apImage.SetActive(true);
                mpImage.SetActive(true);
                mpFrame.SetActive(true);
                apFrame.SetActive(true);

                foreach ((string, Sprite) i in lists.MovepatternImageList)
                {
                    if (HitInfo.transform.gameObject.GetComponentInChildren<SP_Unit>().MovePatternName == i.Item1)
                    {
                        mpImage.GetComponentInChildren<Image>().sprite = i.Item2;
                        break;
                    }
                }

                foreach ((string, Sprite) i in lists.AttackpatternImageList)
                {
                    if (HitInfo.transform.gameObject.GetComponentInChildren<SP_Unit>().AttackPatternName == i.Item1)
                    {
                        apImage.GetComponentInChildren<Image>().sprite = i.Item2;
                        break;
                    }
                }
            }

            if (HitInfo.transform)
            {
                newHover = HitInfo.transform.gameObject;
            }

            // Wenn über ein neues Objekt gehovert wird, wird das Material des alten Objektes zurück gesetzt
            if (oldHover != newHover || !hit)
            {
                unitName.text = "";
                unitLevel.text = "";
                mpImage.SetActive(false);
                apImage.SetActive(false);
                mpFrame.SetActive(false);
                apFrame.SetActive(false);

                if (oldHover)
                {
                    if (oldHover.GetComponent<Collider>() is BoxCollider)
                    {
                        if (oldHover.GetComponent<SP_Tile>())
                        {
                            // Feld ohne Einheit
                            material.SetMaterial(oldHover, material.TileDefaultMaterial);
                        }

                        if (oldHover.GetComponent<SP_Tile>().CurrentUnit)
                        {
                            // Feld mit Einheit
                            material.SetMaterial(oldHover, material.TileDefaultMaterial);
                        }
                    }
                    else if (oldHover.GetComponent<Collider>() is CapsuleCollider)
                    {
                        // Einheit
                        material.SetMaterial(oldHover.GetComponentInChildren<SP_Unit>().CurrentTile, material.TileDefaultMaterial);
                    }
                }

                oldHover = newHover;
            }
        }

        if (controller.CurrentTeam == controller.EnemyTeam)
        {
            if (selection.SelectedUnit)
            {
                unitName.text = selection.SelectedUnit.name;
                unitLevel.text = "" + selection.SelectedUnit.GetComponentInChildren<SP_Unit>().CurrentLevel;

                mpImage.SetActive(true);
                apImage.SetActive(true);
                mpFrame.SetActive(true);
                apFrame.SetActive(true);

                foreach ((string, Sprite) i in lists.MovepatternImageList)
                {
                    if (selection.SelectedUnit.GetComponentInChildren<SP_Unit>().MovePatternName == i.Item1)
                    {
                        mpImage.GetComponentInChildren<Image>().sprite = i.Item2;
                        break;
                    }
                }

                foreach ((string, Sprite) i in lists.AttackpatternImageList)
                {
                    if (selection.SelectedUnit.GetComponentInChildren<SP_Unit>().AttackPatternName == i.Item1)
                    {
                        apImage.GetComponentInChildren<Image>().sprite = i.Item2;
                        break;
                    }
                }
            }
            else
            {
                mpImage.SetActive(false);
                apImage.SetActive(false);
                mpFrame.SetActive(false);
                apFrame.SetActive(false);
            }
        }

        // Erzeugt anhand der Muster des Gegners und der Felderliste 
        // eine Liste mit Felder auf dennen Einheiten des Gegners angreifen können
        enemyAttackTiles = new List<GameObject> { };
        if (controller.EnemyTeam == TEAM_BLUE)
        {
            foreach (GameObject eu in lists.TeamBlueList)
            {
                List<int[]> attackPattern;          // Angriffsmuster der ausgewählten Einheit
                int localX;                         // X Koordinate des jeweiligen Musterfeldes
                int localZ;                         // Z Koordinate des jeweiligen Musterfeldes

                attackPattern = eu.GetComponentInChildren<SP_Unit>().AttackPattern;
                localX = (int)eu.transform.position.x;
                localZ = (int)eu.transform.position.z;

                foreach (int[] ap in attackPattern)
                {
                    // Lokale Koordinaten des Angriffmusters in Weltkoordinaten umwandeln
                    int worldX = ap[0] + localX;
                    int worldZ = ap[1] + localZ;

                    // Vergleich der Position der Felder der Muster und der Felder des Spielbrettes
                    foreach (GameObject t in lists.TileList)
                    {
                        if (worldX == t.transform.position.x && worldZ == t.transform.position.z)
                        {
                            bool isClone = false;

                            if (enemyAttackTiles.Count == 0)
                            {
                                enemyAttackTiles.Add(t);
                            }

                            // Überprüft ob das Angriffsfeld nicht schon in der Liste vorhanden ist
                            foreach (GameObject pat in enemyAttackTiles)
                            {
                                if (t == pat)
                                {
                                    isClone = true;
                                }
                            }

                            if (!isClone)
                            {
                                enemyAttackTiles.Add(t);
                            }
                        }
                    }
                }
            }
        }
        else
        {
            // Erzeugt anhand der Muster des Spielers und der Felderliste 
            // eine Liste mit Felder auf dennen Einheiten des Spielers angreifen können

            foreach (GameObject eu in lists.TeamRedList)
            {
                List<int[]> attackPattern;          // Angriffsmuster der ausgewählten Einheit
                int localX;                         // X Koordinate des jeweiligen Musterfeldes
                int localZ;                         // Z Koordinate des jeweiligen Musterfeldes

                attackPattern = eu.GetComponentInChildren<SP_Unit>().AttackPattern;
                localX = (int)eu.transform.position.x;
                localZ = (int)eu.transform.position.z;

                foreach (int[] ap in attackPattern)
                {
                    // Lokale Koordinaten des Angriffmusters in Weltkoordinaten umwandeln
                    int worldX = ap[0] + localX;
                    int worldZ = ap[1] + localZ;

                    // Vergleich der Position der Felder der Muster und der Felder des Spielbrettes
                    foreach (GameObject t in lists.TileList)
                    {
                        if (worldX == t.transform.position.x && worldZ == t.transform.position.z)
                        {
                            bool isClone = false;

                            if (enemyAttackTiles.Count == 0)
                            {
                                enemyAttackTiles.Add(t);
                            }

                            // Überprüft ob das Angriffsfeld nicht schon in der Liste vorhanden ist
                            foreach (GameObject pat in enemyAttackTiles)
                            {
                                if (t == pat)
                                {
                                    isClone = true;
                                }
                            }

                            if (!isClone)
                            {
                                enemyAttackTiles.Add(t);
                            }
                        }
                    }
                }
            }
        }

        // Hover Aktionen die passieren wenn der Spieler eine Einheit ausgewählt hat
        if (controller.CurrentGameState == ACTION_STATE && controller.CurrentTeam == controller.PlayerTeam && controller.CurrentAction == NOT_IN_ACTION)
        {
            Vector3 actionPanelPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y + 160, Input.mousePosition.z);

            if (hit)
            {
                if (HitInfo.collider is CapsuleCollider)
                {
                    // feindiche Einheit
                    if (HitInfo.transform.GetComponentInChildren<SP_Unit>().Team == controller.EnemyTeam)
                    {
                        if (HitInfo.transform.GetComponentInChildren<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().Type != DEFAULT && HitInfo.transform.GetComponentInChildren<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().Type != MOVEABLE || HitInfo.transform.GetComponentInChildren<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().Type == BOTHABLE)
                        {
                            PlayerActionPanel.SetActive(true);
                            ActionImage.SetActive(true);
                            ActionImage.GetComponent<Image>().sprite = image.AttackIcon;
                            PlayerActionPanel.transform.position = actionPanelPosition;
                            DangerCheck();
                        }
                        else
                        {
                            PlayerActionPanel.SetActive(false);
                            ActionImage.SetActive(false);
                            ActionImage.GetComponentInChildren<Image>().color = Color.white;
                        }
                    }

                    // freundliche Einheit
                    if (HitInfo.transform.GetComponentInChildren<SP_Unit>().Team == controller.PlayerTeam)
                    {
                        if (HitInfo.transform.GetComponentInChildren<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().Type != DEFAULT && HitInfo.transform.GetComponentInChildren<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().Type != ATTACKABLE)
                        {
                            PlayerActionPanel.SetActive(true);
                            ActionImage.SetActive(true);
                            ActionImage.GetComponent<Image>().sprite = image.FusionIcon;
                            PlayerActionPanel.transform.position = actionPanelPosition;
                            DangerCheck();
                        }
                        else
                        {
                            PlayerActionPanel.SetActive(false);
                            ActionImage.SetActive(false);
                            ActionImage.GetComponentInChildren<Image>().color = Color.white;
                        }
                    }
                }

                else if (HitInfo.collider is BoxCollider)
                {
                    // leeres Feld
                    if (!HitInfo.transform.GetComponent<SP_Tile>().CurrentUnit)
                    {
                        if (HitInfo.transform.GetComponent<SP_Tile>().Type != DEFAULT && HitInfo.transform.GetComponent<SP_Tile>().Type != ATTACKABLE)
                        {
                            PlayerActionPanel.SetActive(true);
                            ActionImage.SetActive(true);
                            PlayerActionPanel.transform.position = actionPanelPosition;
                            ActionImage.GetComponent<Image>().sprite = image.MoveIcon;
                            DangerCheck();
                        }
                        else
                        {
                            PlayerActionPanel.SetActive(false);
                            ActionImage.SetActive(false);
                            ActionImage.GetComponentInChildren<Image>().color = Color.white;
                        }
                    }

                    else if (HitInfo.transform.GetComponent<SP_Tile>().CurrentUnit)
                    {
                        // Feld mit feindlicher Einheit
                        if (HitInfo.transform.GetComponent<SP_Tile>().CurrentUnit.GetComponent<SP_Unit>().Team == controller.EnemyTeam)
                        {
                            if (HitInfo.transform.GetComponent<SP_Tile>().Type != DEFAULT && HitInfo.transform.GetComponent<SP_Tile>().Type != MOVEABLE)
                            {
                                PlayerActionPanel.SetActive(true);
                                ActionImage.SetActive(true);
                                PlayerActionPanel.transform.position = actionPanelPosition;
                                ActionImage.GetComponent<Image>().sprite = image.AttackIcon;
                                DangerCheck();
                            }
                            else
                            {
                                PlayerActionPanel.SetActive(false);
                                ActionImage.SetActive(false);
                                ActionImage.GetComponentInChildren<Image>().color = Color.white;

                            }
                        }

                        // Feld mit freundlicher Einheit
                        else if (HitInfo.transform.GetComponent<SP_Tile>().CurrentUnit.GetComponent<SP_Unit>().Team == controller.PlayerTeam)
                        {
                            if (HitInfo.transform.GetComponent<SP_Tile>().Type != DEFAULT && HitInfo.transform.GetComponent<SP_Tile>().Type != ATTACKABLE)
                            {
                                PlayerActionPanel.SetActive(true);
                                ActionImage.SetActive(true);
                                PlayerActionPanel.transform.position = actionPanelPosition;
                                ActionImage.GetComponent<Image>().sprite = image.FusionIcon;
                                DangerCheck();
                            }
                            else
                            {
                                PlayerActionPanel.SetActive(false);
                                ActionImage.SetActive(false);
                                ActionImage.GetComponentInChildren<Image>().color = Color.white;
                            }
                        }
                    }
                }
            }
            else
            {
                PlayerActionPanel.SetActive(false);
                ActionImage.SetActive(false);
                ActionImage.GetComponentInChildren<Image>().color = Color.white;
            }
        }
        else
        {
            PlayerActionPanel.SetActive(false);
            ActionImage.SetActive(false);
            ActionImage.GetComponentInChildren<Image>().color = Color.white;
        }
    }

    void DangerCheck()
    {
        foreach (GameObject at in enemyAttackTiles)
        {
            if (HitInfo.collider is BoxCollider)
            {
                if (at == HitInfo.transform.gameObject)
                {
                    ActionImage.GetComponentInChildren<Image>().color = Color.red;
                    break;
                }
                else
                {
                    ActionImage.GetComponentInChildren<Image>().color = Color.white;
                }
            }

            else if (HitInfo.collider is CapsuleCollider)
            {
                if (at == HitInfo.transform.GetComponentInChildren<SP_Unit>().CurrentTile)
                {
                    ActionImage.GetComponentInChildren<Image>().color = Color.red;
                    break;
                }
                else
                {
                    ActionImage.GetComponentInChildren<Image>().color = Color.white;
                }
            }
            
        }
    }
}
