﻿using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class SP_PulseColor : MonoBehaviour
{
    Color startColorRGB;                // Startwert für LERP
    Color endColorRGB;                  // Endwert für LERP

    public Color defaultColorRGB;      // Default Farbwert des Objektes
    public Color maxColorRGB;          // Maximale Helligkeitwert des Objektes
    public Color minColorRGB;          // Minimaler Helligkeitswert des Objektes

    private float startTime;            // Zeitpunkt wann LERP beginnt
    private float period;               // Dauer eines LERP Durchlauf

    public bool isLERP;                // Deaktiviert den LERP
    public bool startLERP;                // Deaktiviert den LERP

    void Start()
    {
        // Anfangszustand
        startLERP = false;
        isLERP = false;

        // Lege LERP Dauer fest
        startTime = Time.time;
        period = 0.125f;
    }

    private void FixedUpdate()
    {
        // [Ermittelt die Farbwerte die erreicht werden sollen}
        if (startLERP)
        {
                    Debug.Log(defaultColorRGB);
            // Default Farbwert des Objektes
            defaultColorRGB = GetComponent<Renderer>().material.color;

            // Cache für HSV Werte
            float h;
            float s;
            float v;

            // Default Farbe des Objektes in HSV Werte umrechnen
            Color.RGBToHSV(defaultColorRGB, out h, out s, out v);

            // Cache für minimalen und maximalen Helligkeitswerte des Objektes
            Vector3 maxColorHSV = new Vector3(h, s, 1);
            Vector3 minColorHSV = new Vector3(h, s, 0.5f);

            // Umrechnen des ermittelten Farbwerte in RGB
            maxColorRGB = Color.HSVToRGB(maxColorHSV.x, maxColorHSV.y, maxColorHSV.z);
            minColorRGB = Color.HSVToRGB(minColorHSV.x, minColorHSV.y, minColorHSV.z);

            // Inizialisiere LERP
            startColorRGB = defaultColorRGB;
            endColorRGB = maxColorRGB;

            startLERP = false; 
            Debug.Log(defaultColorRGB);
            Debug.Log(isLERP);
            isLERP = true;

            
        }

        // [Führt LERP aus]
        if (isLERP)
        {

            float currentTime;
            float progress;

            // Bestimmt den Moment der Zeitmessung und wandelt es in den prozentualen Anteil des gesamten LERP
            currentTime = Time.time - startTime;
            progress = currentTime / period;

            // Wechselt den Helligkeitswert des Objektes anhand des Fortschritts der Gesamtstrecke
            GetComponent<Renderer>().material.color = Color.Lerp(startColorRGB, endColorRGB, progress);

            // Wenn 100%  des LERP erreicht wurde
            if (progress >= period)
            {
                period = 0.5f;
                // Heligkeitswert des Objektes steigt und fällt endlos
                if (endColorRGB == maxColorRGB)
                {
                    Debug.Log("+");
                    startColorRGB = maxColorRGB;
                    endColorRGB = minColorRGB;
                    startTime = Time.time;
                }
                else if (endColorRGB == minColorRGB)
                {
                    Debug.Log("-");
                    startColorRGB = minColorRGB;
                    endColorRGB = maxColorRGB;
                    startTime = Time.time;
                }
            } 
        }
    }

    public bool StartLERP { get => startLERP; set => startLERP = value; }
    public bool IsLERP { get => isLERP; set => isLERP = value; }
}
