﻿using System.Collections.Generic;
using UnityEngine;
using static SP_Enum.team;
using static SP_Enum.tileReachability;
using static SP_Enum.gameState;
using static SP_Enum.selection;
using static SP_Enum.hover;

public class SP_Selection : MonoBehaviour
{
    // Verwaltung der Auswahl von Objekten

    #region Header

    private SP_Actions actions;
    private SP_Material material;
    private SP_Sound sound;
    private SP_Lists lists;
    private SP_Controller controller;

    // Merkt sich welche Einheit, Feld ausgewählt ist

    [Header("READ ONLY")]
    [SerializeField] private GameObject selectedUnit;
    [SerializeField] private GameObject selectedTile;

    public RaycastHit hitInfo;
    [SerializeField] private bool hit;

    private void Awake()
    {
        actions = GetComponent<SP_Actions>();
        material = GetComponent<SP_Material>();
        sound = GetComponent<SP_Sound>();
        lists = GetComponent<SP_Lists>();
        controller = GetComponent<SP_Controller>();
    }

    #endregion

    #region Verwaltungsmethoden

    public void ChoseUnit()
    {
        // Verhalten wenn etwas angeklickt wurde

        // Raycast um festzustellen was der Spieler anklickt
        hitInfo = new RaycastHit();
        hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);

        // kein Treffer
        if (!hit)
        {
            // Spielt ein Klicksound ab
            sound.playSound(sound.SoundSource, sound.ClickSound);
        }

        // Feld
        else if (hitInfo.collider is BoxCollider)
        {
            // Freies Feld
            if (!hitInfo.transform.GetComponent<SP_Tile>().CurrentUnit)
            {
                // Spielt ein Klicksound ab
                sound.playSound(sound.SoundSource, sound.ClickSound);
            }

            // Feld von Verbündeten
            else if (hitInfo.transform.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().Team == controller.CurrentTeam)
            {
                // Wählt Einheit aus
                SelectUnit();
            }

            // Feld von Gegner
            else if (hitInfo.transform.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().Team != controller.CurrentTeam)
            {
                // Wählt Einheit aus
                SelectUnit();
            }
        }

        // Einheit
        else if (hitInfo.collider is CapsuleCollider)
        {
            // Verbündete Einheit
            if (hitInfo.transform.GetComponent<SP_Unit>().Team == controller.GetComponent<SP_Controller>().CurrentTeam)
            {
                // Wählt Einheit aus
                SelectUnit();
            }

            // Feindliche Einheiten
            else if (hitInfo.transform.GetComponent<SP_Unit>().Team != controller.GetComponent<SP_Controller>().CurrentTeam)
            {
                // Wählt Einheit aus
                SelectUnit();
            }
        }
    }

    public void ChoseAction()
    {
        // Aktionen die vom Spieler ausgeführt werden können je nachdem was angeklickt wird

        hitInfo = new RaycastHit();
        hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);

        // Kein Treffer
        if (!hit) { }

        else if (hitInfo.collider is BoxCollider)
        {
            // Freies Feld
            if (!hitInfo.transform.GetComponent<SP_Tile>().CurrentUnit)
            {
                if (selectedUnit.GetComponentInChildren<SP_Unit>().IsActiv == true)
                {
                    actions.Move();
                }
            }

            // eigenes Feld
            else if (hitInfo.transform.GetComponent<SP_Tile>().CurrentUnit == selectedUnit) { }

            // Feld von Verbündeten
            else if (hitInfo.transform.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().Team == controller.PlayerTeam)
            {
                if (selectedUnit.GetComponentInChildren<SP_Unit>().IsActiv == true)
                {
                    actions.Fusion();
                }
            }

            // Feld von Gegner
            else if (hitInfo.transform.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().Team != controller.PlayerTeam)
            {
                if (selectedUnit.GetComponentInChildren<SP_Unit>().IsActiv == true)
                {
                    actions.Attack();
                }
            }
        }

        else if (hitInfo.collider is CapsuleCollider)
        {
            // eigene Einheit
            if (hitInfo.transform == selectedUnit.GetComponentInChildren<SP_Unit>().transform) { }

            // verbündete Einheit
            else if (hitInfo.transform.GetComponent<SP_Unit>().Team == controller.GetComponent<SP_Controller>().PlayerTeam)
            {
                if (selectedUnit.GetComponentInChildren<SP_Unit>().IsActiv == true)
                {
                    actions.Fusion();
                }
            }

            // feindliche Einheiten
            else if (hitInfo.transform.GetComponent<SP_Unit>().Team != controller.GetComponent<SP_Controller>().PlayerTeam)
            {
                if (selectedUnit.GetComponentInChildren<SP_Unit>().IsActiv == true)
                {
                    actions.Attack();
                }
            }
        }
    }

    #endregion

    #region Methoden

    public void SelectUnit()
    {
        // Getroffene Einheit wird ausgewählt und in den nächsten Zustand gewechselt

        // Wählt Einheit und das Feld auf dem diese steht aus 
        SelectObjects();

        // Färbe die Felder der Einheit in der Sie eine Aktion ausführen kann
        ColorPattern();

        // Verbündete Einheit wurde ausgewählt
        if (selectedUnit.GetComponentInChildren<SP_Unit>().Team == controller.PlayerTeam)
        {
            // Färbt Einheit und dessen Feld als Freund
            material.SetMaterial(selectedTile, material.AllySelectedMaterial);

            // Zustandswechsel
            controller.CurrentSelection = ALLY;
            controller.CurrentGameState = ACTION_STATE;
        }

        // Feindliche Einheit wurde ausgewählt
        else if (selectedUnit.GetComponentInChildren<SP_Unit>().Team != controller.PlayerTeam)
        {
            // Färbt Einheit und dessen Feld als Feind
            material.SetMaterial(selectedTile, material.EnemySelectedMaterial);

            // Zustandswechsel
            controller.CurrentSelection = ENEMY;
            controller.CurrentGameState = ACTION_STATE;
        }
    }

    public void DeselectSelectedUnit()
    {
        // Wählt ausgewählte Einheit und dessen Felder ab

        if (selectedUnit)
        {
            foreach (GameObject g in lists.TileList)
            {
                if (g.GetComponent<SP_Tile>().Type != DEFAULT)
                {
                    // Setzt Feldzustand auf Standart
                    g.GetComponent<SP_Tile>().Type = DEFAULT;
                    g.GetComponent<SP_PulseColor>().IsLERP = false;
                }
            }

            // Färbt Einheit und dessen Felder in den Ursprung zurück
            DecolorSelectedUnit();
            
            // Wähle Einheit und Feld ab
            selectedTile = null;
            selectedUnit = null;

            // Setze Zustände zurück
            controller.CurrentSelection = NOTHING_SELECTED;
            controller.CurrentGameState = SELECTION_STATE;
        }
    }

    #endregion

    #region Hilfsmethoden

    public void DecolorSelectedUnit()
    {
        // Färbt die ausgewählte Einheit und ihre Felder zurück in den Standard

        if (selectedUnit.GetComponent<SP_Unit>().IsActiv)
        {
            Debug.Log("setze aktiv");
            material.SetMaterial(selectedUnit, material.TeamBlueActiveMaterial);
        }

        else if (!selectedUnit.GetComponent<SP_Unit>().IsActiv)
        {
            Debug.Log("setze passiv");
            material.SetMaterial(selectedUnit, material.TeamBluePassivMaterial);
        }


        foreach (GameObject g in lists.TileList)
        {
            if (g.GetComponent<Renderer>().material != material.TileDefaultMaterial)
            {
                material.SetMaterial(g, material.TileDefaultMaterial);
            }
        }
    }

    private void SelectObjects()
    {
        // Wählt angeklickte Einheit aus

        // Einheit
        if (hitInfo.transform.GetComponent<SP_Unit>())
        {
            // Einheit und dessen Feld wird ausgewählt
            selectedUnit = hitInfo.transform.gameObject;
            selectedTile = hitInfo.transform.GetComponent<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().gameObject;
        }

        // Feld
        else if (hitInfo.transform.GetComponent<SP_Tile>())
        {
            // Feld und darauf stehende Einheit wird ausgewählt
            selectedTile = hitInfo.transform.gameObject;
            selectedUnit = hitInfo.transform.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().gameObject;
        }
    }

    public void ColorPattern()
    {
        // Färbt die Felder anhand der übergebenen Muster der Einheiten

        List<int[]> attackPattern;
        List<int[]> movePattern;
        int localX;
        int localZ;

        attackPattern = selectedUnit.GetComponent<SP_Unit>().AttackPattern;
        movePattern = selectedUnit.GetComponent<SP_Unit>().MovePattern;
        localX = (int)selectedUnit.transform.position.x;
        localZ = (int)selectedUnit.transform.position.z;

        foreach (int[] i in attackPattern)
        {
            // Lokale Koordinaten des Pattern in Weltkoordinaten umwandeln
            int worldX = i[0] + localX;
            int worldZ = i[1] + localZ;

            // Vergleiche Koordinaten von Muster und Felder, wenn diese übereinstimmen 
            foreach (GameObject g in lists.TileList)
            {
                if (worldX == g.transform.position.x && worldZ == g.transform.position.z)
                {
                    // Färbt Feld
                    material.SetMaterial(g, material.TileAttackMaterial);

                    // Setzt Feld auf Angreifbar
                    g.GetComponent<SP_Tile>().Type = ATTACKABLE;

                    // Falls nutzbar vom Spieler leuchtet das Feld
                    foreach (GameObject u in lists.UnitList)
                    {
                        if (u.GetComponentInChildren<SP_Unit>().Team == controller.EnemyTeam)
                        {
                            if (u.GetComponentInChildren<SP_Unit>().CurrentTile == g)
                            {
                                g.GetComponent<SP_PulseColor>().StartLERP = true;
                                break;
                            }
                        }
                    }

                    break;
                }
            }
        }

        foreach (int[] i in movePattern)
        {
            // Lokale Koordinaten des Pattern in Weltkoordinaten umwandeln
            int worldX = i[0] + localX;
            int worldZ = i[1] + localZ;

            // Vergleiche Koordinaten von Muster und Felder, wenn diese übereinstimmen 
            foreach (GameObject g in lists.TileList)
            {
                if (worldX == g.transform.position.x && worldZ == g.transform.position.z)
                {
                    if (g.GetComponent<SP_Tile>().Type == ATTACKABLE)
                    {
                        // Färbt Feld
                        material.SetMaterial(g, material.TileBothableMaterial);

                        // Setzt Feld auf beides
                        g.GetComponent<SP_Tile>().Type = BOTHABLE;
                    }
                    else
                    {
                        // Färbt Feld
                        material.SetMaterial(g, material.TileMoveMaterial);

                        // Setzt Feld auf beweglich
                        g.GetComponent<SP_Tile>().Type = MOVEABLE;
                    }

                    // Falls nutzbar vom Spieler leuchtet das Feld
                    foreach (GameObject u in lists.UnitList)
                    {
                        if (u.GetComponentInChildren<SP_Unit>().Team == controller.PlayerTeam)
                        {
                            if (u.GetComponentInChildren<SP_Unit>().CurrentTile == g)
                            {
                                g.GetComponent<SP_PulseColor>().StartLERP = true;
                                break;
                            }
                        }
                    }

                    if (!g.GetComponent<SP_Tile>().CurrentUnit)
                    {
                        g.GetComponent<SP_PulseColor>().StartLERP = true;
                    }

                    break;
                }
            }
        }
    }

    #endregion

    #region Getter Setter

    public GameObject SelectedUnit { get => selectedUnit; set => selectedUnit = value; }
    public GameObject SelectedTile { get => selectedTile; set => selectedTile = value; }
    public RaycastHit HitInfo { get => hitInfo; }
    public bool Hit { get => hit; }

    #endregion
}