﻿using UnityEngine;
using System.Collections.Generic;
using static SP_Enum.team;
using System.Security.Cryptography;

public class SP_Board : MonoBehaviour
{
    // Verwaltung des Spielfeldes

    #region Header

    // Klassenreferenzen
    private SP_Lists lists;
    private SP_Material material;
    private SP_Pattern pattern;
    private SP_Sound sound;

    [Header("Folder References")]
    [SerializeField] private GameObject unitFolder;     // Ordner in UnityHierarchy für Einheiten
    [SerializeField] private GameObject tileFolder;     // Ordner in UnityHierarchy für Felder

    [Header("Board Attributes")]
    [SerializeField] private int boardDepth = 10;       // Anzahl Spielfelder in der Tiefe
    [SerializeField] private int boardWidth = 10;       // Anzahl Spielfelder in der Breite

    // Eine Liste mit der Reihenfolge welche Felder nach welcher Runde zerstört werden sollen
    private List<string> destroyTileSequenz = new List<string> { "T00", "T99", "T01", "T98", "T02", "T97", "T03", "T96", "T04", "T95",
                                                                 "T05", "T94", "T06", "T93", "T07", "T92", "T08", "T91", "T09", "T90",
                                                                 "T10", "T89", "T11", "T88", "T12", "T87", "T13", "T86", "T14", "T85",
                                                                 "T15", "T84", "T16", "T83", "T17", "T82", "T18", "T81", "T19", "T80",
                                                                 "T20", "T79", "T21", "T78", "T22", "T77", "T23", "T76", "T24", "T75",
                                                                 "T25", "T74", "T26", "T73", "T27", "T72", "T28", "T71", "T29", "T70",
                                                                 "T30", "T69", "T31", "T68", "T32", "T67", "T33", "T66", "T34", "T65",
                                                                 "T35", "T64", "T36", "T63", "T37", "T62", "T38", "T61", "T39", "T60",
                                                                 "T40", "T59", "T41", "T58", "T42", "T57", "T43", "T56", "T44", "T55",
                                                                 "T45", "T54", "T46", "T53", "T47", "T52", "T48", "T51", "T49", "T50" };
    
    public int destroyIndex;                            // Zähler welches Feld nach einer Runde zerstört werden soll

    void Awake()
    {
        // Abkürzungen
        lists = GetComponent<SP_Lists>();
        material = GetComponent<SP_Material>();
        pattern = GetComponent<SP_Pattern>();
        sound = GetComponent<SP_Sound>();

        // Initialisierung
        destroyIndex = -3;
    }

    #endregion

    #region Methoden

    public void SetupBoard()
    {
        // Spielbrett mit Spielfiguren wird erzeugt
        
        for (int x = 0; x < boardWidth; x++)
        {
            for (int z = 0; z < boardDepth; z++)
            {
                // Felder werden erzeugt
                GameObject tile = Instantiate((GameObject)Resources.Load("Prefab/Singleplayer/Tile", typeof(GameObject)), new Vector3(x, 0, z), Quaternion.identity) as GameObject;

                // Felder erhalten einen Namen
                tile.name = "T" + x.ToString() + z.ToString();

                // Felder werden in ein Ordner in UnityHirarchy abgelegt
                tile.transform.parent = tileFolder.transform;

                // Felder werden in die Felderliste gepackt
                lists.TileList.Add(tile);

                // Einheiten werden nur in der ersten und letzen Spalte erzeugt, 
                if (x == 0 || x == boardWidth - 1)
                {
                    if (x == 0)
                    {
                        // Rote Einheiten werden erzeugt
                        GameObject unit = Instantiate((GameObject)Resources.Load("Prefab/Singleplayer/UnitF", typeof(GameObject)), new Vector3(x, 1, z), Quaternion.Euler(0, 0, 0)) as GameObject;

                        // Einheiten werden in den Einheitenordner in UnityHirarchy abgelegt
                        unit.transform.parent = unitFolder.transform;

                        // Einheiten werden in die Einheitenliste gepackt
                        lists.UnitList.Add(unit);

                        // Einheiten erhalten einen Namen für Team Rot
                        unit.name = "UF" + z.ToString();
                        unit.GetComponentInChildren<SP_Unit>().name = "UF_inner_" + z.ToString();

                        // Einheiten werden rot eingefärbt
                        material.SetMaterial(unit, material.TeamRedActiveMaterial);

                        // Einheit Team Rot zugewiesen
                        unit.GetComponentInChildren<SP_Unit>().Team = TEAM_RED;

                        // Einheit wird aktiv gesetzt
                        unit.GetComponentInChildren<SP_Unit>().IsActiv = true;

                        // Einheiten bekommen die Anfangsmuster zugewiesen
                        // Da das Angriffsmuster asymmetrisch ist muss es gespiegelt werden
                        unit.GetComponentInChildren<SP_Unit>().MovePattern = pattern.MpBasic;
                        unit.GetComponentInChildren<SP_Unit>().MovePatternName = lists.PatternBasic[0].Item2;
                        unit.GetComponentInChildren<SP_Unit>().AttackPattern = pattern.flipPattern(pattern.ApBasic, false);
                        unit.GetComponentInChildren<SP_Unit>().AttackPatternName = lists.PatternBasic[1].Item2;

                        // Einheiten werden in die Liste von Team Rot gepackt
                        lists.TeamRedList.Add(unit);

                        // Die Einheit und das Feld auf dem diese steht referenzieren sich gegenseitig
                        unit.GetComponentInChildren<SP_Unit>().CurrentTile = tile;
                        unit.GetComponentInChildren<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().CurrentUnit = unit.GetComponentInChildren<Transform>().GetChild(0).transform.gameObject;

                        // Animator für Bewegungen zugewiesen
                        unit.GetComponentInChildren<SP_Unit>().Animator = (Animator)Resources.Load("Animation/MoveAnimations/Unit");
                    }

                    else if (x == boardWidth - 1)
                    {
                        // Blaue Einheiten werden erzeugt
                        GameObject unit = Instantiate((GameObject)Resources.Load("Prefab/Singleplayer/UnitT", typeof(GameObject)), new Vector3(x, 1, z), Quaternion.Euler(0, 0, 0)) as GameObject;

                        // Einheiten werden in ein Ordner in UnityHirarchy abgelegt
                        unit.transform.parent = unitFolder.transform;

                        // Einheiten werden in die Einheitenliste gepackt
                        lists.UnitList.Add(unit);

                        // Einheiten erhalten ihrem Team entsprechend einen Namen
                        unit.name = "UT" + z.ToString();
                        unit.GetComponentInChildren<SP_Unit>().name = "UT_inner_" + z.ToString();

                        // Einheiten werden entsprechend ihrem Team eingefärbt
                        material.SetMaterial(unit, material.TeamBlueActiveMaterial);

                        // Einheit Team Rot zugewiesen
                        unit.GetComponentInChildren<SP_Unit>().Team = TEAM_BLUE;

                        // Einheit wird aktiv gesetzt
                        unit.GetComponentInChildren<SP_Unit>().IsActiv = true;

                        // Einheiten bekommen die Anfangsmuster zugewiesen
                        // Da das Angriffsmuster asymmetrisch ist muss es gespiegelt werden
                        unit.GetComponentInChildren<SP_Unit>().MovePattern = pattern.MpBasic;
                        unit.GetComponentInChildren<SP_Unit>().MovePatternName = lists.PatternBasic[0].Item2;
                        unit.GetComponentInChildren<SP_Unit>().AttackPattern = pattern.flipPattern(pattern.ApBasic, true);
                        unit.GetComponentInChildren<SP_Unit>().AttackPatternName = lists.PatternBasic[1].Item2;

                        // Einheiten werden in die Liste von Team Blau gepackt
                        lists.TeamBlueList.Add(unit);

                        // Die Einheit und das Feld auf dem diese steht referenzieren sich gegenseitig
                        unit.GetComponentInChildren<SP_Unit>().CurrentTile = tile;
                        unit.GetComponentInChildren<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().CurrentUnit = unit.GetComponentInChildren<Transform>().GetChild(0).transform.gameObject;

                        // Animator für Bewegungen zugewiesen
                        unit.GetComponentInChildren<SP_Unit>().Animator = (Animator)Resources.Load("Animation/MoveAnimations/Unit");
                    }
                }
            }
        }
    }

    public void DestroyTile()
    {
        if (0 <= destroyIndex && destroyIndex < destroyTileSequenz.Count)
        {
            GameObject tileToDestroy = null;
            GameObject unitToDestroy = null;


            foreach (GameObject g in lists.TileList)
            {
                if (0 <= destroyIndex && g.name == destroyTileSequenz[destroyIndex])
                {
                    tileToDestroy = g;
                }
            }

            if (tileToDestroy.GetComponent<SP_Tile>().CurrentUnit)
            {
                unitToDestroy = tileToDestroy.GetComponent<SP_Tile>().CurrentUnit;

                lists.UnitList.Remove(unitToDestroy.transform.parent.gameObject);

                if (tileToDestroy.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().Team == TEAM_RED)
                {
                    lists.TeamRedList.Remove(unitToDestroy.transform.parent.gameObject);
                }

                else if (tileToDestroy.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().Team == TEAM_BLUE)
                {
                    lists.TeamBlueList.Remove(unitToDestroy.transform.parent.gameObject);
                }

                Destroy(unitToDestroy.transform.parent.gameObject);
            }
            sound.playSound(sound.SoundSource, sound.TileDestructionSound);
            lists.TileList.Remove(tileToDestroy);
            Destroy(tileToDestroy);
        }
        destroyIndex++;
    }

    #endregion
}
