﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using static SP_Enum.team;
using static SP_Enum.isAnimation;
using static SP_Enum.action;

public class SP_EnemyBot : MonoBehaviour
{
    // Shortcuts
    private SP_Controller controller;
    private SP_Lists lists;
    private SP_Selection selection;
    private SP_Sound sound;
    private SP_UI ui;

    public List<GameObject> activeUnits;                              // Liste mit aktiven Einheiten des Bots
    public List<GameObject> botUnits;                                 // Liste mit Einheiten des Spielers
    private List<GameObject> playerUnits;                             // Liste mit Einheiten des Spielers
    private List<GameObject> playerAttackTiles;                       // Liste mit Felder auf die der Spieler angreifen kann
    private List<(GameObject, int)> botTileActions;                   // Liste mit Feldern auf die der Bot Aktionen ausführen kann
    private List<GameObject> priorityTileList;                        // Liste mit Feldern der niedrigsten Priorität
    private List<(List<int[]>, string, int, bool)> fusionList;        // Liste mit Mustern die in der Fusion zur Auswahl stehen

    private int lowestPriority;                                       // Stellt die niedrigste Priorität in der Aktionsliste dar
    public GameObject targetTile;                                     // Feld auf die der Bot eine Aktion ausführt
    private GameObject nearestEnemy;                                  // Von der ausgewählten Einheit aus, die nahe liegenste feindliche Einheit 

    private Vector3 startPosition;                                    // Startposition des LERP
    private Vector3 endPosition;                                      // Zielposition des LERP
    private float startTime;                                          // Startzeit des LERP
    private float endTime;                                            // Zielzeit des LERP

    int teamLv;                                                       // Obergrenze wie viel Fusionen vollzogen werden
    int selectedUnitMovepatternLv;                                    // Stufe des Bewegungsmusters der ausgewählten Einheit
    int selectedUnitAttackpatternLv;                                  // Stufe des Angriffsmusters der ausgewählten Einheit
    int targetUnitMovepatternLv;                                      // Stufe des Bewegungsmusters der Zieleinheit
    int targetUnitAttackpatternLv;                                    // Stufe des Angriffsmusters der Zieleinheit

    bool isNewRound;                                                  // Überprüft ob eine neue Runde begonnen hat
    bool isNewUnit;                                                   // Überprüft ob eine neue Einheit benutzt wird
    bool isLERP;                                                      // Ist die Animation am laufen
    bool isPostWork;                                                  // Ist die Nachbearbeitung am laufen

    private void Awake()
    {
        // Shortcuts
        controller = GetComponent<SP_Controller>();
        lists = GetComponent<SP_Lists>();
        selection = GetComponent<SP_Selection>();
        sound = GetComponent<SP_Sound>();
        ui = GetComponent<SP_UI>();

        isNewRound = true;
        isNewUnit = true;
        isLERP = false;
        isPostWork = false;
    }

    public void BotController()
    {
        // Verwaltet die Gegner KI
        // Wenn der Bot dran ist kann die Runde nicht übersprungen werden
        // Der Bot wählt aus den noch aktiven Einheiten aus seinem Team zufällig eine aus
        // Er betrachtet alle möglichen Aktionen und bewertet diese
        // Aus diesen Aktionen wählt er die beste aus und führt diese aus
        // Zum Schluss wird die benutze Einheit passiv gesetzt
        // Dies wiederholt der Bot sollange bis das Spiel beendet ist oder alle Einheiten bewegt wurden und beendet seinen Zug 

        if (isNewRound)
        {
            isNewRound = false;

            MakeActiveUnitList();
        }

        if (isNewUnit)
        {
            isNewUnit = false;

            MakeBotUnitList();
            SelectRandomUnit();
            MakePlayerUnitList();
            MakePlayerAttackTileList();
            MakeTeamLv();
            MakeActionList();
            SearchLowestPriority();

            if (lowestPriority != 4 && lowestPriority != 7)
            {
                MakePriorityList();
                PrepareActionHandler();
                PrepareLERP();
            }
            else
            {
                activeUnits.Remove(selection.SelectedUnit);
                DeselectSelectedUnit();

                controller.CurrentAction = NOT_IN_ACTION;

                isPostWork = false;
                isNewUnit = true;

                if (activeUnits.Count == 0)
                {
                    isNewRound = true;
                    ui.ClickedOnSkipButton();
                }
            }
        }

        if (isPostWork)
        {
            ActionHandler();
        }
    }

    void PrepareActionHandler()
    {
        // Je nach Priorität sucht der Bot aus Prioritätenliste das Zielfeld heraus
        // Ist die Priorität 1 oder 5, sucht der Bot die wertvollste Einheit des Spielers heraus um diese zu schlagen
        // Ist die Priorität 2 sucht der Bot zufällig eine verbündete Einheit aus um mit dieser zu fusionieren
        // Ist die Priorität 3 oder 7 sucht der Bot das leere Feld aus mit dem er schnellst möglich zum Spieler kommt

        if (lowestPriority == 1 || lowestPriority == 5)
        {
            ChoiseStrongestUnit();
        }

        if (lowestPriority == 2)
        {
            ChoiseRandomUnit();
        }

        if (lowestPriority == 3 || lowestPriority == 6)
        {
            SearchNearestUnit();
            SearchNearestTile();
        }
    }

    void ActionHandler()
    {
        // Wählt anhand der Priorität die passende Methode aus. 
        // 1: Der Bot greift an. 2: Der Bot fusioniert 3: Der Bot bewegt sich auf den Spieler zu. 4: Der Bot bleibt stehen. 
        // Bei 1 - 4 kann der Bot seine Einheit nicht verlieren. 
        // 5: Der Bot greift an. 6: Der Bot bewegt sich auf den Spieler zu. 7: Der Bot bleibt stehen. 
        // Bei 5 - 7 geht der Bot von Verlust seiner Einheit aus.

        if (!(lowestPriority == 4 || lowestPriority == 7))
        {
            if (lowestPriority == 2)
            {
                selection.SelectedUnit.GetComponentInChildren<SP_Unit>().CurrentLevel += targetTile.GetComponent<SP_Tile>().CurrentUnit.GetComponent<SP_Unit>().CurrentLevel;
                MakePatternLv();
                ChoiseExisitngPattern();
                MakeNewPattern();
                ChoiseNewPattern();
            }

            if (lowestPriority == 1 || lowestPriority == 2 || lowestPriority == 5)
            {
                DestroyTargetUnit();
            }

            if (lowestPriority == 1 || lowestPriority == 5)
            {
                ui.WinCheck();
            }

            SetTargetTileAsReference();
        }

        activeUnits.Remove(selection.SelectedUnit);
        DeselectSelectedUnit();

        controller.CurrentAction = NOT_IN_ACTION;

        isPostWork = false;
        isNewUnit = true;

        if (activeUnits.Count == 0)
        {
            isNewRound = true;
            ui.ClickedOnSkipButton();
        }
    }

    void MakeBotUnitList()
    {
        // Erzeugt anhand der Teamlisten eine Liste mit Einheiten des Bots

        botUnits = new List<GameObject> { };

        if (controller.EnemyTeam == TEAM_BLUE)
        {
            foreach (GameObject u in lists.TeamBlueList)
            {
                botUnits.Add(u);
            }
        }
        else
        {
            foreach (GameObject u in lists.TeamRedList)
            {
                botUnits.Add(u);
            }
        }

        Debug.Log("botUnits: " + botUnits.Count);
    }

    void MakeActiveUnitList()
    {
        // Erzeugt anhand der Teamliste des Bots eine Liste mit aktiven Einheiten

        activeUnits = new List<GameObject> { };

        if (controller.EnemyTeam == TEAM_BLUE)
        {
            foreach (GameObject u in lists.TeamBlueList)
            {
                activeUnits.Add(u);
            }
        }
        else
        {
            foreach (GameObject u in lists.TeamRedList)
            {
                activeUnits.Add(u);
            }
        }

        Debug.Log("activeUnits: " + activeUnits.Count);
    }

    void MakePlayerUnitList()
    {
        // Erzeugt anhand der Teamlisten eine Liste mit Einheiten des Spielers

        playerUnits = new List<GameObject> { };

        if (controller.PlayerTeam == TEAM_BLUE)
        {
            foreach (GameObject u in lists.TeamBlueList)
            {
                playerUnits.Add(u);
            }
        }
        else
        {
            foreach (GameObject u in lists.TeamRedList)
            {
                playerUnits.Add(u);
            }
        }

        Debug.Log("playerUnits: " + playerUnits.Count);
    }

    void MakePlayerAttackTileList()
    {
        // Erzeugt anhand der Muster des Spielers und der Felderliste 
        // eine Liste mit Felder auf dennen Einheiten des Spielers angreifen können

        playerAttackTiles = new List<GameObject> { };

        foreach (GameObject pu in playerUnits)
        {
            List<int[]> attackPattern;          // Angriffsmuster der ausgewählten Einheit
            int localX;                         // X Koordinate des jeweiligen Musterfeldes
            int localZ;                         // Z Koordinate des jeweiligen Musterfeldes

            attackPattern = pu.GetComponentInChildren<SP_Unit>().AttackPattern;
            localX = (int)pu.transform.position.x;
            localZ = (int)pu.transform.position.z;

            foreach (int[] ap in attackPattern)
            {
                // Lokale Koordinaten des Angriffmusters in Weltkoordinaten umwandeln
                int worldX = ap[0] + localX;
                int worldZ = ap[1] + localZ;

                // Vergleich der Position der Felder der Muster und der Felder des Spielbrettes
                foreach (GameObject t in lists.TileList)
                {
                    if (worldX == t.transform.position.x && worldZ == t.transform.position.z)
                    {
                        bool isClone = false;

                        if (playerAttackTiles.Count == 0)
                        {
                            playerAttackTiles.Add(t);
                        }

                        // Überprüft ob das Angriffsfeld nicht schon in der Liste vorhanden ist
                        foreach (GameObject pat in playerAttackTiles)
                        {
                            if (t == pat)
                            {
                                isClone = true;
                            }
                        }

                        if (!isClone)
                        {
                            playerAttackTiles.Add(t);
                        }
                    }
                }
            }
        }

        foreach (GameObject t in playerAttackTiles)
        {
            Debug.Log("playerAttackTiles: " + t.name);
        }
    }

    void SelectRandomUnit()
    {
        // Wählt eine zufällige Einheit aus der Liste der aktiven Einheiten

        // Erzeugt eine Zufallsposition
        int random = Random.Range(0, activeUnits.Count);

        // Wählt anhand der Zufalsposition in der Liste der aktiven Einheiten die Einheit und dessen Feld an
        selection.SelectedUnit = activeUnits[random];
        selection.SelectedTile = activeUnits[random].GetComponentInChildren<SP_Unit>().CurrentTile;

        Debug.Log("SelectedUnit: " + selection.SelectedUnit.name);
    }

    void MakeTeamLv()
    {
        // Erzeugt einen Wert an dem festgestellt wird ob der Bot weiter fusioniert

        teamLv = 5;

        foreach (GameObject u in botUnits)
        {
            if (u.GetComponentInChildren<SP_Unit>().CurrentLevel == 1)
            {
                teamLv++;
            }
            else
            {
                teamLv -= u.GetComponentInChildren<SP_Unit>().CurrentLevel;
            }
        }
    }

    void MakeActionList()
    {
        // Erstellt eine Liste mit Feldern mit der der Bot interagieren kann.
        // Diese Felder werden anhand der Bewegungsmuster und Angriffsmuster der ausgewählten Einheit,
        // Ob das Feld mit einer Einheit besetzt ist,
        // Ob diese Einheit freundlich oder feindlich ist,
        // und ob dieses Feld vom Spieler angegriffen werden kann

        List<int[]> movePattern;            // Bewegungsmuster der ausgewählten Einheit
        List<int[]> attackPattern;          // Angriffsmuster der ausgewählten Einheit
        int localX;                         // X Koordinate des jeweiligen Musterfeldes
        int localZ;                         // Z Koordinate des jeweiligen Musterfeldes

        attackPattern = selection.SelectedUnit.GetComponentInChildren<SP_Unit>().AttackPattern;
        movePattern = selection.SelectedUnit.GetComponentInChildren<SP_Unit>().MovePattern;
        localX = (int)selection.SelectedUnit.transform.position.x;
        localZ = (int)selection.SelectedUnit.transform.position.z;

        botTileActions = new List<(GameObject, int)> { };

        foreach (int[] i in attackPattern)
        {
            // Lokale Koordinaten des Angriffsmuster in Weltkoordinaten umwandeln
            int worldX = i[0] + localX;
            int worldZ = i[1] + localZ;

            // Überprüfe ob ein Feld des Angriffsmuster eine Spielereinheit besitzt und ob der Spieler diese angreifen kann
            // Wenn das Feld eine Spielereinheit besitzt, bekommt das Feld die Priorität 1: Angriff ohne Verlust
            // Wenn das Feld eine Spielereinheit besitzt und vom Spieler angegriffen werden kann, 
            // bekommt das Feld die Priorität 5: Angriff mit Verlust

            foreach (GameObject t in playerUnits)
            {
                if (worldX == t.transform.position.x && worldZ == t.transform.position.z)
                {
                    bool isHit = false;

                    foreach (GameObject pa in playerAttackTiles)
                    {
                        Debug.Log("ATTACK: t: " + t.name + " -> pat: " + pa.name);

                        if (worldX == pa.transform.position.x && worldZ == pa.transform.position.z)
                        {
                            botTileActions.Add((t.GetComponentInChildren<SP_Unit>().CurrentTile, 5));
                            isHit = true;

                            break;
                        }
                    }

                    if (!isHit)
                    {
                        botTileActions.Add((t.GetComponentInChildren<SP_Unit>().CurrentTile, 1));
                    }
                }
            }
        }

        foreach (int[] i in movePattern)
        {
            // Lokale Koordinaten des Bewegungsmusters in Weltkoordinaten umwandeln
            int worldX = i[0] + localX;
            int worldZ = i[1] + localZ;

            // Überprüfe ob ein Felde des Bewegungsmuster leer oder besetzt ist und ob diese von Spielerangriffen erreicht wird
            // Wenn das Feld mit einer Boteinheit besetzt ist, bekommt es die Priorität 2: Fusion ohne Verlust
            // Wenn das Feld leer ist und nicht vom Spieler angegriffen werden kann, bekommt es die Priorität 3: Auf Spieler ohne Verlust zubewegen
            // Wenn das Feld leer ist aber der Spieler es angreiffen kann, bekommt es die Priorität 6: Auf Spieler mit Verlust zubewegen

            if (0 < teamLv)
            {
                if (selection.SelectedUnit.GetComponentInChildren<SP_Unit>().CurrentLevel < 6)
                {
                    foreach (GameObject bu in botUnits)
                    {
                        if (worldX == bu.transform.position.x && worldZ == bu.transform.position.z)
                        {
                            bool isHit = false;

                            foreach (GameObject pa in playerAttackTiles)
                            {
                                if (worldX == pa.transform.position.x && worldZ == pa.transform.position.z)
                                {
                                    isHit = true;
                                    break;
                                }
                            }

                            if (!isHit)
                            {
                                botTileActions.Add((bu.GetComponentInChildren<SP_Unit>().CurrentTile, 2));
                            }
                        }
                    }
                }
            }

            foreach (GameObject t in lists.TileList)
            {
                if (worldX == t.transform.position.x && worldZ == t.transform.position.z)
                {
                    if (t.GetComponent<SP_Tile>().CurrentUnit == null)
                    {
                        bool isHit = false;

                        foreach (GameObject pat in playerAttackTiles)
                        {
                            Debug.Log("MOVE: t: " + t.name + " -> pat: " + pat.name);

                            if (worldX == pat.transform.position.x && worldZ == pat.transform.position.z)
                            {
                                botTileActions.Add((t, 6));
                                isHit = true;
                                break;
                            }
                        }

                        if (!isHit)
                        {
                            botTileActions.Add((t, 3));
                        }
                    }
                }
            }
        }

        // Überprüft ob das Feld der ausgewählten Einheit angreiffbar ist
        // Ist das Feld nicht vom Spieler angreifbar, bekommt es die Priorität 4: Stehen bleiben ohne Verlust
        // Ist das Feld vom Spieler angreifbar, bekommt es die Priorität 7: Stehen bleiben mit Verlust
        foreach (GameObject pat in playerAttackTiles)
        {
            if (selection.SelectedTile == pat)
            {
                botTileActions.Add((selection.SelectedTile, 7));
                break;
            }
            else
            {
                botTileActions.Add((selection.SelectedTile, 4));
                break;
            }
        }

        Debug.Log("botTileActions: " + botTileActions.Count);

        foreach ((GameObject, int) d in botTileActions)
        {
            Debug.Log("botTileActions: " + d.Item1.name + " Priorität: " + d.Item2);
        }
    }

    void SearchLowestPriority()
    {
        // Sucht die niedrigste Priorität in der Aktionsliste des Bots aus

        // Setzt Priorität zurück
        lowestPriority = 10;

        foreach ((GameObject, int) t in botTileActions)
        {
            if (t.Item2 < lowestPriority)
            {
                lowestPriority = t.Item2;
            }
        }

        Debug.Log("lowestPriority: " + lowestPriority);
    }

    void MakePriorityList()
    {
        // Erstellt eine Liste mit allen Felder der niedrigsten Priorität aus der Aktionsliste des Bots

        priorityTileList = new List<GameObject> { };

        foreach ((GameObject, int) t in botTileActions)
        {
            if (t.Item2 == lowestPriority)
            {
                priorityTileList.Add(t.Item1);
            }
        }

        Debug.Log("priorityTileList: " + priorityTileList.Count);
    }

    void ChoiseStrongestUnit()
    {
        // Wählt aus der Prioritätenliste die stärkste Einheit aus

        // Merkt sich lokal die stärkste Einheit aus der Prioritätsliste
        int highestUnitLv = 0;

        foreach (GameObject t in priorityTileList)
        {
            Debug.Log(t.name);

            if (highestUnitLv < t.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().CurrentLevel)
            {
                highestUnitLv = t.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().CurrentLevel;
                targetTile = t;
            }
        }

        Debug.Log("ChoiseStrongestUnit: " + targetTile.name);
    }

    void ChoiseRandomUnit()
    {
        // Wählt aus der Prioritätenliste eine zufällige Einheit aus

        // Erzeugt eine Zufallsposition in der Liste
        int rndPos = Random.Range(0, priorityTileList.Count);

        targetTile = priorityTileList[rndPos];

        Debug.Log("ChoiseRandomUnit: " + targetTile.name);
    }

    void SearchNearestUnit()
    {
        // Sucht die, von der ausgewählten Einheit, nahe liegenste feindliche Einheit aus

        float shortestDistance = 100;
        float currentDistance = 0;

        // Position A
        float myX = 0;
        float myZ = 0;
        Vector2 myPosition = new Vector2(0, 0);

        // Position B
        float otherX = 0;
        float otherZ = 0;
        Vector2 otherPosition = new Vector2(0, 0);

        nearestEnemy = null;

        // Position der ausgewählten Einheit
        myX = selection.SelectedUnit.transform.position.x;
        myZ = selection.SelectedUnit.transform.position.z;
        myPosition = new Vector2(myX, myZ);

        foreach (GameObject pu in playerUnits)
        {
            // Position der Spielereinheit
            otherX = pu.transform.position.x;
            otherZ = pu.transform.position.z;
            otherPosition = new Vector2(otherX, otherZ);

            // Berechne Distanz zwischen ausgewählter Einheit und der Spielereinheit
            currentDistance = Vector2.Distance(myPosition, otherPosition);

            // Sucht die Spielereinheit mit der kleinsten Distanz aus
            if (currentDistance < shortestDistance)
            {
                shortestDistance = currentDistance;
                nearestEnemy = pu;
            }
        }

        Debug.Log("nearestEnemy: " + nearestEnemy.name);
    }

    void SearchNearestTile()
    {
        // Sucht das, von der Spielereinheit aus, nahe liegenste Bewegungsfeld der ausgewählten Einheit aus

        float shortestDistance = 100;
        float currentDistance = 0;

        // Position A
        float myX = 0;
        float myZ = 0;
        Vector2 myPosition = new Vector2(0, 0);

        // Position B
        float otherX = 0;
        float otherZ = 0;
        Vector2 otherPosition = new Vector2(0, 0);

        // Position der ausgewählten Einheit
        myX = nearestEnemy.transform.position.x;
        myZ = nearestEnemy.transform.position.z;
        myPosition = new Vector2(myX, myZ);

        foreach (GameObject pt in priorityTileList)
        {
            // Position der Spielereinheit
            otherX = pt.transform.position.x;
            otherZ = pt.transform.position.z;
            otherPosition = new Vector2(otherX, otherZ);

            // Berechne Distanz zwischen ausgewählter Einheit und der Spielereinheit
            currentDistance = Vector2.Distance(myPosition, otherPosition);

            // Sucht die Spielereinheit mit der kleinsten Distanz aus
            if (currentDistance < shortestDistance)
            {
                shortestDistance = currentDistance;
                targetTile = pt;
            }
        }

        Debug.Log("SearchNearestTile: " + targetTile.name);
    }

    void PrepareLERP()
    {
        // Bereitet den LERP und die Animation von der ausgewählten Einheit zum Zielfeld vor

        // Ermittelt die Position der ausgewählten Einheit
        float playerHorizontalPosition = selection.SelectedUnit.transform.position.x;
        float playerVerticalPosition = selection.SelectedUnit.transform.position.z;
        Vector3 playerTransform = new Vector3(playerHorizontalPosition, 1, playerVerticalPosition);

        // Ermittelt die angeklickte Zielposition
        float targetHorizontalPosition = targetTile.transform.position.x;
        float targetVerticalPosition = targetTile.transform.position.z;
        Vector3 targetTransform = new Vector3(targetHorizontalPosition, 1, targetVerticalPosition);

        // Lege Start und Endpunkt für LERP fest
        startPosition = playerTransform;
        endPosition = targetTransform;

        // Lege LERP Dauer fest
        startTime = Time.time;
        endTime = 1f;

        selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", true);

        controller.Animation = IN_ANIMATION;

        isLERP = true;

        Debug.Log("PrepareLERP: " + startPosition + " -> " + endPosition + " | " + startTime);
    }

    private void FixedUpdate()
    {
        // führt den LERP und die Animation aus

        if (isLERP)
        {
            float currentTime;
            float progress;

            // Bestimmt den Moment der Zeitmessung und wandelt es in den prozentualen Anteil des gesamten LERP
            currentTime = Time.time - startTime;
            progress = currentTime / endTime;

            // Verschiebe ausgewählte Einheit in Zielposition des LERP anhand des Fortschritts der Gesamtstrecke
            selection.SelectedUnit.transform.position = Vector3.Lerp(startPosition, endPosition, progress);

            // Wenn 100%  des LERP erreicht wurde

            if (progress >= 1.0f)
            {
                controller.Animation = OUT_OF_ANIMATION;

                selection.SelectedUnit.GetComponentInChildren<Animator>().SetBool("Bool", false);

                isLERP = false;
                isPostWork = true;

                Debug.Log("LERP finished");

                if (lowestPriority == 1 || lowestPriority == 5)
                {
                    sound.playSound(sound.SoundSource, sound.AttackSound);
                }

                if (lowestPriority == 2)
                {
                    sound.playSound(sound.SoundSource, sound.FusionSound);
                }

                if (lowestPriority == 3 || lowestPriority == 4)
                {
                    sound.playSound(sound.SoundSource, sound.MoveSound);
                }
            }
        }
    }

    void DestroyTargetUnit()
    {
        // Einheit auf dem Zielfeld wird zerstört und aus sämtlichen Listen entfernt

        GameObject targetUnit = targetTile.GetComponent<SP_Tile>().CurrentUnit.transform.parent.gameObject;

        lists.UnitList.Remove(targetUnit);
        activeUnits.Remove(targetUnit);
        botUnits.Remove(targetUnit);

        if (targetTile.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().Team == TEAM_BLUE)
        {
            lists.TeamBlueList.Remove(targetUnit);
        }
        else
        {
            lists.TeamRedList.Remove(targetUnit);
        }

        Debug.Log("Destroyed: " + targetUnit.name);
        Destroy(targetUnit);
    }

    void MakePatternLv()
    {
        // Bewertet die Angriffsmuster und Bewegunsmuster der ausgewählten Einheit

        selectedUnitMovepatternLv = 0;
        selectedUnitAttackpatternLv = 0;
        targetUnitMovepatternLv = 0;
        targetUnitAttackpatternLv = 0;

        GameObject targetUnit = targetTile.GetComponent<SP_Tile>().CurrentUnit;

        for (int i = 0; i < lists.PatternList.Count; i++)
        {
            for (int j = 0; j < lists.PatternList[i].Count; j++)
            {
                if (lists.PatternList[i][j].Item2 == selection.SelectedUnit.GetComponentInChildren<SP_Unit>().MovePatternName)
                {
                    selectedUnitMovepatternLv = i;
                }

                if (lists.PatternList[i][j].Item2 == selection.SelectedUnit.GetComponentInChildren<SP_Unit>().AttackPatternName)
                {
                    selectedUnitAttackpatternLv = i;
                }

                if (lists.PatternList[i][j].Item2 == targetUnit.GetComponentInChildren<SP_Unit>().MovePatternName)
                {
                    targetUnitMovepatternLv = i;
                }

                if (lists.PatternList[i][j].Item2 == targetUnit.GetComponentInChildren<SP_Unit>().AttackPatternName)
                {
                    targetUnitAttackpatternLv = i;
                }
            }
        }
        Debug.Log("selectedUnitMovepattern: " + selectedUnitMovepatternLv);
        Debug.Log("selectedUnitAttackpattern: " + selectedUnitAttackpatternLv);
        Debug.Log("targetUnitMovepattern: " + targetUnitMovepatternLv);
        Debug.Log("targetUnitAttackpattern: " + targetUnitAttackpatternLv);
    }

    void ChoiseExisitngPattern()
    {
        // Wählt das höchststufige Muster aus das die fusionierenden Einheiten besitzen

        if (selectedUnitAttackpatternLv < targetUnitAttackpatternLv)
        {
            selection.SelectedUnit.GetComponentInChildren<SP_Unit>().AttackPattern = targetTile.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().AttackPattern;
        }

        if (selectedUnitMovepatternLv < targetUnitMovepatternLv)
        {
            selection.SelectedUnit.GetComponentInChildren<SP_Unit>().MovePattern = targetTile.GetComponent<SP_Tile>().CurrentUnit.GetComponentInChildren<SP_Unit>().MovePattern;
        }

        Debug.Log("oldSelectedUnitMovepattern: " + selection.SelectedUnit.GetComponentInChildren<SP_Unit>().AttackPatternName);
        Debug.Log("oldSelectedUnitAttackpattern: " + selection.SelectedUnit.GetComponentInChildren<SP_Unit>().MovePatternName);
    }

    void MakeNewPattern()
    {
        // Wählt anhand der Stufe der ausgewählten Einheit zufällig ein Pattern dieser Stufe aus

        fusionList = new List<(List<int[]>, string, int, bool)> { };

        int unitLv = selection.SelectedUnit.GetComponentInChildren<SP_Unit>().CurrentLevel;

        if (unitLv < 4)
        {
            for (int i = 0; i < 3; i++)
            {
                int rndPos = Random.Range(0, lists.PatternList[unitLv - 1].Count);
                bool rndBool = Random.value < 0.5f;

                List<int[]> fusionPattern = lists.PatternList[unitLv - 1][rndPos].Item1;
                string fusionPatternName = lists.PatternList[unitLv - 1][rndPos].Item2;

                fusionList.Add((fusionPattern, fusionPatternName, unitLv, rndBool));

                Debug.Log("PatternListCount: " + lists.PatternList[unitLv - 1].Count + " | rndPos: " + rndPos + " | rndBool: " + rndBool + " | fusionPatternName: " + fusionPatternName);
            }
        }
        else
        {
            for (int i = 0; i < 3; i++)
            {
                int rndPos = Random.Range(0, lists.PatternList[4].Count);
                bool rndBool = Random.value < 0.5f;

                List<int[]> fusionPattern = lists.PatternList[4][rndPos].Item1;
                string fusionPatternName = lists.PatternList[4][rndPos].Item2;

                fusionList.Add((fusionPattern, fusionPatternName, unitLv, rndBool));

                Debug.Log("PatternListCount: " + lists.PatternList[4].Count + " | rndPos: " + rndPos + " | rndBool: " + rndBool + " | fusionPatternName: " + fusionPatternName);
            }
        }

        foreach ((List<int[]>, string, int, bool) fl in fusionList)
        {
            Debug.Log("fusionList: " + fl.Item2 + " | Level: " + fl.Item3 + " | Art: " + fl.Item4);
        }
    }

    void ChoiseNewPattern()
    {
        // Sucht das schwächte Muster der Einheit aus und ersetzt es durch das stärkste Fusionsmuster

        (List<int[]>, string, int) tmpMP = (new List<int[]> { }, "", 0);
        (List<int[]>, string, int) tmpAP = (new List<int[]> { }, "", 0);

        foreach ((List<int[]>, string, int, bool) fp in fusionList)
        {
            if (tmpMP.Item3 == fp.Item3 && fp.Item4 == true)
            {
                int rndPos = Random.Range(0, fusionList.Count);

                tmpMP = (fusionList[rndPos].Item1, fusionList[rndPos].Item2, fusionList[rndPos].Item3);
                break;
            }

            if (tmpMP.Item3 < fp.Item3 && fp.Item4 == true)
            {
                tmpMP = (fp.Item1, fp.Item2, fp.Item3);
                break;
            }

            if (tmpAP.Item3 == fp.Item3 && fp.Item4 == false)
            {
                int rndPos = Random.Range(0, fusionList.Count);

                tmpAP = (fusionList[rndPos].Item1, fusionList[rndPos].Item2, fusionList[rndPos].Item3);
                break;
            }

            if (tmpAP.Item3 < fp.Item3 && fp.Item4 == false)
            {
                tmpAP = (fp.Item1, fp.Item2, fp.Item3);
                break;
            }
        }

        if (tmpMP.Item3 < tmpAP.Item3)
        {
            selection.SelectedUnit.GetComponentInChildren<SP_Unit>().AttackPattern = tmpAP.Item1;
            selection.SelectedUnit.GetComponentInChildren<SP_Unit>().AttackPatternName = tmpAP.Item2;
        }
        else
        {
            selection.SelectedUnit.GetComponentInChildren<SP_Unit>().MovePattern = tmpMP.Item1;
            selection.SelectedUnit.GetComponentInChildren<SP_Unit>().MovePatternName = tmpMP.Item2;
        }

        Debug.Log("NewSelectedUnitMovepattern: " + selection.SelectedUnit.GetComponentInChildren<SP_Unit>().AttackPatternName);
        Debug.Log("NewSelectedUnitAttackpattern: " + selection.SelectedUnit.GetComponentInChildren<SP_Unit>().MovePatternName);
    }

    void SetTargetTileAsReference()
    {
        // Feld der ausgewählten Einheit verliert Ihre Referenz auf diese
        // Zielfeld und ausgewählte Einheit referenzieren sich gegenseitig

        // Feld Der ausgewählten Einheit verliert ihre Referenz
        selection.SelectedUnit.GetComponentInChildren<SP_Unit>().CurrentTile.GetComponent<SP_Tile>().CurrentUnit = null;

        // Ausgewählte Einheit überschreibt sein Feld mit dem Zielfeld
        selection.SelectedUnit.GetComponentInChildren<SP_Unit>().CurrentTile = targetTile;

        // Zielfeld überschreibt seine Einheit mit der ausgewählten Einheit
        targetTile.GetComponent<SP_Tile>().CurrentUnit = selection.SelectedUnit.transform.GetChild(0).gameObject;

        // Ausgewähltes Feld wird mit Zielfeld überschrieben
        selection.SelectedTile = targetTile;

        Debug.Log("NewSelectedUnit: " + selection.SelectedUnit.name);
        Debug.Log("NewSelectedTile: " + selection.SelectedTile.name);
    }

    void DeselectSelectedUnit()
    {
        // Wählt ausgewählte Einheit und dessen Feld ab

        activeUnits.Remove(selection.SelectedUnit);

        selection.SelectedUnit = null;
        selection.SelectedTile = null;

        targetTile = null;

        Debug.Log("");
    }
}