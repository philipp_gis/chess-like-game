﻿using UnityEngine;
using static SP_Enum.gameState;
using static SP_Enum.action;
using static SP_Enum.isAnimation;
using static SP_Enum.selection;
using static SP_Enum.team;
using static SP_Enum.hover;


public class SP_Controller : MonoBehaviour
{
    // Verwaltet den Spielablauf

    #region Header

    [Header("Class References")]
    private SP_Board board;
    private SP_Lists lists;
    private SP_UI ui;
    private SP_Selection selection;
    private SP_Actions actions;
    private SP_EnemyBot enemy;
    private SP_Hover hover;


    [Header("Global States")]
    [SerializeField] private SP_Enum.gameState currentGameState;            // Aktueller Spielzustand
    [SerializeField] private SP_Enum.team playerTeam;                       // Team des Spielers
    [SerializeField] private SP_Enum.team enemyTeam;                        // Team des Gegners
    [SerializeField] private SP_Enum.team currentTeam;                      // Team das aktuell dran ist
    [SerializeField] private SP_Enum.selection currentSelection;            // Auswahl die aktuell ausgeführt wird
    [SerializeField] private SP_Enum.action currentAction;                  // Aktion die aktuell ausgeführt wird
    [SerializeField] private SP_Enum.isAnimation isAnimation;               // Ist eine Animation am laufen 
    [SerializeField] private SP_Enum.hover hoverState;                      // Worüber schwebt der Spieler mit seiner Maus

    private void Awake()
    {
        // Abkürzungen
        board = GetComponent<SP_Board>();
        ui = GetComponent<SP_UI>();
        selection = GetComponent<SP_Selection>();
        actions = GetComponent<SP_Actions>();
        enemy = GetComponent<SP_EnemyBot>();
        lists = GetComponent<SP_Lists>();
        hover = GetComponent<SP_Hover>();

        // Default Zustände
        currentGameState = UNITY_STATE;
        playerTeam = NOT_SELECTED;
        enemyTeam = NOT_SELECTED;
        currentTeam = NOT_SELECTED;
        currentSelection = NOTHING_SELECTED;
        currentAction = NOT_IN_ACTION;
        isAnimation = OUT_OF_ANIMATION;
        hoverState = NOTHING;
    }

    #endregion

    #region Verwaltungsmethoden

    private void Start()
    {
        
        // Zum Spielstart wird das Spielfeld mit Feldern und Einheiten in Teams erstellt 
        board.SetupBoard();

        // nicht notwendige UI Elemente werden ausgeblendet
        ui.StartUI();
    }

    private void Update()
    {
        // Zustandsverwaltung
        switch (currentGameState)
        {
            // Default Zustand des Spiels
            case UNITY_STATE:

                if (currentTeam == enemyTeam && currentTeam != NOT_SELECTED && lists.TeamRedList.Count != 0 && lists.TeamBlueList.Count != 0)
                {
                    // Wenn der Gegner dran ist übernimmt die KI
                    enemy.BotController();
                    hover.HoverSelection();
                }

                break;

            case SELECTION_STATE:

                if (currentTeam == playerTeam)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        // Mit Linksklick wählt der Spieler eine Einheit aus
                        selection.ChoseUnit();
                    }

                    hover.HoverSelection();
                }
                else
                {
                    // Wenn der Gegner dran ist übernimmt die KI
                    enemy.BotController();
                    hover.HoverSelection();
                }

                break;

            // Je nach dem ob der Spieler eine Einheit des eigenen oder des feindlichen Team ausgewählt hatt, kann er Aktionen ausführen
            case ACTION_STATE:

                if (currentSelection == ENEMY)
                {
                    if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
                    {
                        // Einheit wird abgewählt
                        selection.DeselectSelectedUnit();
                    }
                }

                else if (currentSelection == ALLY)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        // Einheit kann sich Bewegen, Angreifen, Fusionieren, Abwählen
                        selection.ChoseAction();
                    }

                    if (Input.GetMouseButtonDown(1))
                    {
                        // Einheit wird abgewählt
                        selection.DeselectSelectedUnit();
                    }

                    hover.HoverSelection();
                }

                break;
        }

        // Aktionsverwaltung
        switch (currentAction)
        {
            case POST_MOVE_WORK:

                actions.PostMoveWork();

                break;

            case POST_ATTACK_WORK:

                actions.PostAttackWork();

                break;

            case POST_FUSION_WORK:

                actions.PostFusionWork();

                break;
        }

        if (currentGameState == SELECTION_STATE && currentTeam == playerTeam)
        {
            ui.SkipRoundButton.interactable = true;
        }
        else
        {
            ui.SkipRoundButton.interactable = false;
        }

    }

    #endregion

    #region Getter Setter

    public SP_Enum.gameState CurrentGameState { get => currentGameState; set => currentGameState = value; }
    public SP_Enum.action CurrentAction { get => currentAction; set => currentAction = value; }
    public SP_Enum.selection CurrentSelection { get => currentSelection; set => currentSelection = value; }
    public SP_Enum.isAnimation Animation { get => isAnimation; set => isAnimation = value; }
    public SP_Enum.team PlayerTeam { get => playerTeam; set => playerTeam = value; }
    public SP_Enum.team EnemyTeam { get => enemyTeam; set => enemyTeam = value; }
    public SP_Enum.team CurrentTeam { get => currentTeam; set => currentTeam = value; }
    public SP_Enum.hover HoverState { get => hoverState; set => hoverState = value; }

    #endregion
}