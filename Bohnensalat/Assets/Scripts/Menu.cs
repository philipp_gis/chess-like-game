﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    [SerializeField] AudioSource button_audio;

    private void Awake()
    {
        Application.targetFrameRate = 60;
    }

    public void startSingleplayer()
    {
        button_audio.Play();
        UnityEngine.SceneManagement.SceneManager.LoadScene("Singleplayer");        
    }

    public void startHotseat()
    {
        button_audio.Play();
        UnityEngine.SceneManagement.SceneManager.LoadScene("Hotseat");
    }

    public void startOneOnOne()
    {
        button_audio.Play();
        UnityEngine.SceneManagement.SceneManager.LoadScene("OneOnOne");
    }

    public void startMultiplayer()
    {
        button_audio.Play();
        UnityEngine.SceneManagement.SceneManager.LoadScene("Multiplayer");
    }

    public void quitGame()
    {
        button_audio.Play();
        Application.Quit();
    }
}
