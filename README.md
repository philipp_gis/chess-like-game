# Chess like Game
![Animation](/images/animation.gif)

## Description
Democracy Fusion Fight is a chess-like game where the player can combine his chess pieces to get better movement patterns.

## Goals
Lern the basics of creating Games and unsing Unity.

## Getting started
Doubleclick on `Bohnensalat.exe`to start the Game. Please select `Singleplayer` for the full experience. The other modes are in an old state or not finished. If you are interested in the code, have a look at `Bohnensalat/Assets/Scripts`.

## Future Work
- [ ] General naming 
- [ ] Delete not function modes
- [ ] Change music
- [ ] Change visuals
- [ ] Change gameplay
